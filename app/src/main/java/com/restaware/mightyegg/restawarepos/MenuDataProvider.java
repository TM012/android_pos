package com.restaware.mightyegg.restawarepos;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;
import static android.database.sqlite.SQLiteDatabase.openOrCreateDatabase;


/**
 * Created by mightyegg on 1/1/17.
 */

public class MenuDataProvider {

    public static HashMap<String, List<String>> getInfo() {
        HashMap<String, List<String>> MenuDetails = new HashMap<String, List<String>>();

        SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", null, null);

        Cursor cursor;
        String[] menu_categories;
        int[] menu_id;
        int i;

        cursor = db.rawQuery("SELECT * FROM MenuCategories_v1", null);
        menu_categories = new String[cursor.getCount()];
        menu_id = new int[cursor.getCount()];
        i = 0;

        if (cursor.moveToFirst()) {
            menu_categories[i] = cursor.getString(cursor.getColumnIndex("category_name"));
            menu_id[i] = cursor.getInt(cursor.getColumnIndex("Id"));
            i++;

            while (cursor.moveToNext()) {
                menu_categories[i] = cursor.getString(cursor.getColumnIndex("category_name"));
                menu_id[i] = cursor.getInt(cursor.getColumnIndex("Id"));
                i++;
            }
        }

        int total_categories = cursor.getCount();
        List<String> menu_items;

        for (i = 0; i < total_categories; i++) {
            cursor = db.rawQuery("SELECT * FROM MenuItems_v1 WHERE item_category='" + menu_categories[i] + "'", null);
            menu_items = new ArrayList<String>();

            if (cursor.moveToFirst()) {
                menu_items.add(cursor.getString(cursor.getColumnIndex("item_name")));

                while (cursor.moveToNext()) {
                    menu_items.add(cursor.getString(cursor.getColumnIndex("item_name")));
                }
            }

            MenuDetails.put(menu_categories[i], menu_items);
        }

        db.close();

        return MenuDetails;
    }
}
