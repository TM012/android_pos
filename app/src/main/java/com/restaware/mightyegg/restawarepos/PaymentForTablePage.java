package com.restaware.mightyegg.restawarepos;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

public class PaymentForTablePage extends AppCompatActivity {

    String table_name, time_stamp, selected_customer, customer, combination;

    //////////Variables to pass bill details///////////
    String print_type;
    String[] customer_number, category, item, unit_price, price, quantity, item_discount, item_discount_type, modifier, modifier_price;
    String[] discount, discount_type;
    String[] combination_array;
    int total_orders;
    ///////////////////////////////////////////////////

    //////////Variables to Populate Bill List///////////
    ListView lv_order_list;
    billListAdapter billAdapter;
    float subtotal, discount_amount, tax, total, tips, balance;
    String[] list_item, list_customer, list_quantity, list_each, list_total;
    ////////////////////////////////////////////////////

    /////////Variables for Payment Calculation////////////
    String paid_amount, change_amount;
    //////////////////////////////////////////////////////

    /////////Variables for Number Pad//////////////
    String payment_type;
    ///////////////////////////////////////////////

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_for_table_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_order_per_table);
        setSupportActionBar(toolbar);

        context = this;
        //TODO fetch payment type for tab payment
        payment_type = "";

        FetchInheritedValues();

        ///////////Initializing all variables///////////////
        FetchCombinations();

        FetchAllOrders();
        ////////////////////////////////////////////////////

        /////////////Toolbar Activities//////////////////
        TextView tv_table_name = (TextView) toolbar.findViewById(R.id.tv_table_name);
        tv_table_name.setText(table_name);

        TextView tv_customer_number = (TextView) toolbar.findViewById(R.id.tv_customer_number);
        tv_customer_number.setText(combination_array[Integer.parseInt(combination)]);

        TextView tv_customer_button = (TextView) findViewById(R.id.tv_customer_button);
        tv_customer_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //////////////////Dialog to Customer//////////////////////
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Select Customer")
                        .setItems(combination_array, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(PaymentForTablePage.this, PaymentForTablePage.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                intent.putExtra("table_name", table_name);
                                intent.putExtra("customer", customer);
                                intent.putExtra("time_stamp", time_stamp);
                                intent.putExtra("combination", String.valueOf(which));
                                startActivity(intent);
                                PaymentForTablePage.this.overridePendingTransition(0, 0);
                                PaymentForTablePage.this.finish();
                            }
                        });
                builder.create();
                builder.show();
                //////////////////////////////////////////////////////////////
            }
        });
        /////////////////////////////////////////////////

        //////////////Linking variables with objects in layout/////////////
        lv_order_list = (ListView) findViewById(R.id.lv_order_list);
        ///////////////////////////////////////////////////////////////////

        //////////////Refresh Bill List//////////////
        FetchTips();
        refreshBillSegment();
        FetchPaymentHistory();
        FetchPaymentTypeForTab();
        /////////////////////////////////////////////

        InitiateButtonListeners();
    }

    private void FetchPaymentTypeForTab() {
        SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
        Cursor c = db.rawQuery("SELECT * FROM TabDetails_v1 WHERE order_number='" + time_stamp + "' AND customer_name='" + table_name + "'", null);

        if (c.moveToFirst()) {
            if (!c.getString(c.getColumnIndex("card_number")).isEmpty()) {
                DecimalFormat df = new DecimalFormat("0.00");
                paid_amount = df.format(balance);
                payment_type = "Card";
                PopulatePaymentFields();
            }
        }

        c.close();
        db.close();
    }

    private void InitiateButtonListeners() {
        ImageButton ib_ppt_button_tip = (ImageButton) findViewById(R.id.ib_ppt_button_tip);
        ib_ppt_button_tip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // custom dialog
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.tip_dialog_layout);

                final EditText et_tip = (EditText) dialog.findViewById(R.id.et_tip);
                et_tip.setText(String.valueOf(tips));

                Button confirmButton = (Button) dialog.findViewById(R.id.button_confirm_tip);

                confirmButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tips = Float.parseFloat(et_tip.getText().toString());

                        refreshBillSegment();

                        PopulatePaymentFields();

                        dialog.cancel();
                    }
                });

                dialog.show();
            }
        });

        ImageButton ib_ppt_button_cancel = (ImageButton) findViewById(R.id.ib_ppt_button_cancel);
        ib_ppt_button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PaymentForTablePage.this, OrderForTablePage.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.putExtra("table_name", table_name);
                intent.putExtra("customer", customer);
                intent.putExtra("time_stamp", time_stamp);
                startActivity(intent);
                PaymentForTablePage.this.finish();
                PaymentForTablePage.this.overridePendingTransition(0, 0);
            }
        });

        ImageButton ib_ppt_button_clear = (ImageButton) findViewById(R.id.ib_ppt_button_clear);
        ib_ppt_button_clear.setOnClickListener(new ManipulatePaidAmount());
        ImageButton ib_ppt_button_0 = (ImageButton) findViewById(R.id.ib_ppt_button_0);
        ib_ppt_button_0.setOnClickListener(new ManipulatePaidAmount());
        ImageButton ib_ppt_button_1 = (ImageButton) findViewById(R.id.ib_ppt_button_1);
        ib_ppt_button_1.setOnClickListener(new ManipulatePaidAmount());
        ImageButton ib_ppt_button_2 = (ImageButton) findViewById(R.id.ib_ppt_button_2);
        ib_ppt_button_2.setOnClickListener(new ManipulatePaidAmount());
        ImageButton ib_ppt_button_3 = (ImageButton) findViewById(R.id.ib_ppt_button_3);
        ib_ppt_button_3.setOnClickListener(new ManipulatePaidAmount());
        ImageButton ib_ppt_button_4 = (ImageButton) findViewById(R.id.ib_ppt_button_4);
        ib_ppt_button_4.setOnClickListener(new ManipulatePaidAmount());
        ImageButton ib_ppt_button_5 = (ImageButton) findViewById(R.id.ib_ppt_button_5);
        ib_ppt_button_5.setOnClickListener(new ManipulatePaidAmount());
        ImageButton ib_ppt_button_6 = (ImageButton) findViewById(R.id.ib_ppt_button_6);
        ib_ppt_button_6.setOnClickListener(new ManipulatePaidAmount());
        ImageButton ib_ppt_button_7 = (ImageButton) findViewById(R.id.ib_ppt_button_7);
        ib_ppt_button_7.setOnClickListener(new ManipulatePaidAmount());
        ImageButton ib_ppt_button_8 = (ImageButton) findViewById(R.id.ib_ppt_button_8);
        ib_ppt_button_8.setOnClickListener(new ManipulatePaidAmount());
        ImageButton ib_ppt_button_9 = (ImageButton) findViewById(R.id.ib_ppt_button_9);
        ib_ppt_button_9.setOnClickListener(new ManipulatePaidAmount());
        ImageButton ib_ppt_button_dot = (ImageButton) findViewById(R.id.ib_ppt_button_dot);
        ib_ppt_button_dot.setOnClickListener(new ManipulatePaidAmount());
        ImageButton ib_ppt_button_10 = (ImageButton) findViewById(R.id.ib_ppt_button_10);
        ib_ppt_button_10.setOnClickListener(new ManipulatePaidAmount());
        ImageButton ib_ppt_button_20 = (ImageButton) findViewById(R.id.ib_ppt_button_20);
        ib_ppt_button_20.setOnClickListener(new ManipulatePaidAmount());
        ImageButton ib_ppt_button_50 = (ImageButton) findViewById(R.id.ib_ppt_button_50);
        ib_ppt_button_50.setOnClickListener(new ManipulatePaidAmount());
        ImageButton ib_ppt_button_arrow = (ImageButton) findViewById(R.id.ib_ppt_button_arrow);
        ib_ppt_button_arrow.setOnClickListener(new ManipulatePaidAmount());

        ImageButton ib_ppt_card = (ImageButton) findViewById(R.id.ib_ppt_card);
        ib_ppt_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DecimalFormat df = new DecimalFormat("0.00");
                paid_amount = df.format(balance);
                payment_type = "Card";
                PopulatePaymentFields();

                Log.d("AMOUNT CHECK", balance + " " + paid_amount);
            }
        });

        ImageButton ib_ppt_cash = (ImageButton) findViewById(R.id.ib_ppt_cash);
        ib_ppt_cash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payment_type = "Cash";
            }
        });

        ImageButton ib_ppt_pay = (ImageButton) findViewById(R.id.ib_ppt_pay);
        ib_ppt_pay.setOnClickListener(new PaymentClear());
    }

    private class ManipulatePaidAmount implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.ib_ppt_button_clear) {
                paid_amount = "0";
            } else if (v.getId() == R.id.ib_ppt_button_0) {
                paid_amount += "0";
            } else if (v.getId() == R.id.ib_ppt_button_1) {
                paid_amount += "1";
            } else if (v.getId() == R.id.ib_ppt_button_2) {
                paid_amount += "2";
            } else if (v.getId() == R.id.ib_ppt_button_3) {
                paid_amount += "3";
            } else if (v.getId() == R.id.ib_ppt_button_4) {
                paid_amount += "4";
            } else if (v.getId() == R.id.ib_ppt_button_5) {
                paid_amount += "5";
            } else if (v.getId() == R.id.ib_ppt_button_6) {
                paid_amount += "6";
            } else if (v.getId() == R.id.ib_ppt_button_7) {
                paid_amount += "7";
            } else if (v.getId() == R.id.ib_ppt_button_8) {
                paid_amount += "8";
            } else if (v.getId() == R.id.ib_ppt_button_9) {
                paid_amount += "9";
            } else if (v.getId() == R.id.ib_ppt_button_dot) {
                if (!paid_amount.contains(".")) {
                    paid_amount += ".";
                }
            } else if (v.getId() == R.id.ib_ppt_button_10) {
                paid_amount = "10";
            } else if (v.getId() == R.id.ib_ppt_button_20) {
                paid_amount = "20";
            } else if (v.getId() == R.id.ib_ppt_button_50) {
                paid_amount = "50";
            } else if (v.getId() == R.id.ib_ppt_button_arrow) {
                if (paid_amount.length() > 1) {
                    paid_amount = paid_amount.substring(0, paid_amount.length() - 1);
                } else {
                    paid_amount = "0";
                }
            }

            PopulatePaymentFields();
        }
    }

    private class PaymentClear implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.ib_ppt_pay) {
                if (payment_type.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Select Payment Type First", Toast.LENGTH_SHORT).show();
                } else {
                    PopulatePaymentFields();

                    if (Double.parseDouble(change_amount) < 0) {
                        Toast.makeText(getApplicationContext(), "Payment Not Cleared", Toast.LENGTH_SHORT).show();
                    } else {
                        SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);

                        db.execSQL("UPDATE PrintOptions_v1 SET payment_status='paid' WHERE order_number='" + time_stamp + "' AND " +
                                "table_number ='" + table_name + "' AND combination='" + combination_array[Integer.parseInt(combination)] + "'");

                        Cursor c = db.rawQuery("SELECT * FROM OrderLog_v1 WHERE order_number='" + time_stamp + "' AND table_number='" + table_name + "' " +
                                "AND customer_numbers='" + combination_array[Integer.parseInt(combination)] + "'", null);

                        Long tsLong = System.currentTimeMillis()/1000;
                        String ts = tsLong.toString();

                        if (c.moveToFirst()) {
                            db.execSQL("UPDATE OrderLog_v1 SET subtotal='" + subtotal + "', discount='" + discount_amount + "', tax='" + tax + "', " +
                                    "total='" + total + "', balance='" + balance + "', paid='" + paid_amount + "', change='" + change_amount + "', " +
                                    "payment_type='" + payment_type + "', tips='" + tips + "', payment_time='" + ts + "' WHERE Id=" + c.getInt(c.getColumnIndex("Id")));
                        } else {
                            db.execSQL("INSERT INTO OrderLog_v1 (order_number, table_number, total_customers, subtotal, discount, tax, total, balance, " +
                                    "paid, change, customer_numbers, payment_type, tips, payment_time) VALUES (" +
                                    "'" + time_stamp + "', '" + table_name + "', '" + customer + "', '" + subtotal + "', '" + discount_amount + "', " +
                                    "'" + tax + "', '" + total + "', '" + balance + "', '" + paid_amount + "', '" + change_amount + "', " +
                                    "'" + combination_array[Integer.parseInt(combination)] + "', '" + payment_type + "', '" + tips + "', " +
                                    "'" + ts + "');");
                        }

                        c = db.rawQuery("SELECT * FROM PrintOptions_v1 WHERE order_number='" + time_stamp + "' AND table_number='" + table_name +"'", null);
                        boolean paid = true;

                        if (c.moveToNext()) {
                            if (c.getString(c.getColumnIndex("payment_status")).equals("unpaid")) {
                                paid = false;
                            }
                            while (c.moveToNext()) {
                                if (c.getString(c.getColumnIndex("payment_status")).equals("unpaid")) {
                                    paid = false;
                                }
                            }
                        }

                        if (paid) {
                            //TODO check if payment is for tab or table
                            c = db.rawQuery("SELECT * FROM TabDetails_v1 WHERE order_number='" + time_stamp + "' AND customer_name='" + table_name + "'", null);

                            if (c.moveToFirst()) {
                                db.execSQL("UPDATE TabDetails_v1 SET status='closed' WHERE order_number='" + time_stamp + "' " +
                                        "AND customer_name='" + table_name + "'");
                                Toast.makeText(getApplicationContext(), "Payment for " + table_name + " is clear", Toast.LENGTH_SHORT).show();
                            } else {
                                db.execSQL("UPDATE TableArrangement_v1 SET status='vacant' WHERE number='" + table_name + "' AND order_number='" + time_stamp + "'");
                                Toast.makeText(getApplicationContext(), "Table " + table_name + " is vacant now", Toast.LENGTH_SHORT).show();
                            }

                            if (getIntent().getStringExtra("redirect_from").equals("Order")) {
                                Intent intent = new Intent(PaymentForTablePage.this, ViewTables.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(intent);
                            }
                            PaymentForTablePage.this.finish();
                        } else {
                            Toast.makeText(getApplicationContext(), "Payment for Customer(s) " + combination_array[Integer.parseInt(combination)] + " is clear", Toast.LENGTH_SHORT).show();
                        }

                        c.close();
                        db.close();
                    }
                }
            }
        }
    }

    protected void FetchTips() {
        SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
        Cursor c = db.rawQuery("SELECT * FROM OrderLog_v1 WHERE order_number='" + time_stamp + "' AND table_number='" + table_name +  "' " +
                "AND customer_numbers='" + combination_array[Integer.parseInt(combination)] + "'", null);

        if (c.moveToFirst()) {
            tips = Float.parseFloat(c.getString(c.getColumnIndex("tips")));
        } else {
            tips = 0;
        }

        c.close();
        db.close();
    }

    private void FetchPaymentHistory() {
        SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
        Cursor c = db.rawQuery("SELECT * FROM OrderLog_v1 WHERE order_number='" + time_stamp + "' AND customer_numbers='" + combination_array[Integer.parseInt(combination)] + "' " +
                "AND table_number='" + table_name + "'", null);

        if (c.moveToFirst()) {
            paid_amount = c.getString(c.getColumnIndex("paid"));
        } else {
            paid_amount = "0";
        }

        PopulatePaymentFields();

        c.close();
        db.close();
    }

    private void PopulatePaymentFields() {
        double paid, change;
        DecimalFormat df = new DecimalFormat("0.00");
        DecimalFormat df1 = new DecimalFormat("#.##");
        TextView tv_ppt_change = (TextView) findViewById(R.id.tv_ppt_change);
        TextView tv_ppt_paid = (TextView) findViewById(R.id.tv_ppt_paid);

        paid = Double.parseDouble(paid_amount);
        change = paid - Double.parseDouble(df.format(balance));
        change_amount = String.valueOf(change);

        //Log.d("CHANGE", change + " " + paid + "-" + balance);

        if (paid_amount.contains(".")) {
            tv_ppt_paid.setText("$ " + df.format(paid));
        } else {
            tv_ppt_paid.setText("$ " + df1.format(paid));
        }
        tv_ppt_change.setText("$ " + df.format(change));
    }

    private void FetchInheritedValues() {
        SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
        Cursor c;
        print_type = "";

        //////////////Fetch inherited values///////////////
        table_name = getIntent().getStringExtra("table_name");
        time_stamp = getIntent().getStringExtra("time_stamp");
        //TODO fetch selected bill type
        c = db.rawQuery("SELECT * FROM PrintOptions_v1 WHERE order_number='" + time_stamp + "' AND table_number='" + table_name + "'", null);
        if (c.moveToFirst()) {
            print_type = c.getString(c.getColumnIndex("print_options"));
        }
        customer = getIntent().getStringExtra("customer");
        combination = getIntent().getStringExtra("combination");
        ///////////////////////////////////////////////////

        c.close();
        db.close();
    }

    private void FetchAllOrders() {
        SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
        Cursor c;
        int i;

        //Fetching orders
        c = db.rawQuery("SELECT * FROM OrderPerCustomer_v1 WHERE order_number='" + time_stamp + "'", null);

        customer_number = new String[c.getCount()];
        category = new String[c.getCount()];
        item = new String[c.getCount()];
        unit_price = new String[c.getCount()];
        price = new String[c.getCount()];
        quantity = new String[c.getCount()];
        item_discount = new String[c.getCount()];
        item_discount_type = new String[c.getCount()];
        modifier = new String[c.getCount()];
        modifier_price = new String[c.getCount()];

        i = 0;

        if (c.moveToFirst()) {
            customer_number[i] = c.getString(c.getColumnIndex("customer_number"));
            category[i] = c.getString(c.getColumnIndex("category"));
            item[i] = c.getString(c.getColumnIndex("item"));
            unit_price[i] = c.getString(c.getColumnIndex("unit_price"));
            price[i] = c.getString(c.getColumnIndex("price"));
            quantity[i] = c.getString(c.getColumnIndex("quantity"));
            item_discount[i] = c.getString(c.getColumnIndex("item_discount"));
            item_discount_type[i] = c.getString(c.getColumnIndex("item_discount_type"));
            modifier[i] = c.getString(c.getColumnIndex("modifier"));
            modifier_price[i] = c.getString(c.getColumnIndex("modifier_price"));
            i++;

            while (c.moveToNext()) {
                customer_number[i] = c.getString(c.getColumnIndex("customer_number"));
                category[i] = c.getString(c.getColumnIndex("category"));
                item[i] = c.getString(c.getColumnIndex("item"));
                unit_price[i] = c.getString(c.getColumnIndex("unit_price"));
                price[i] = c.getString(c.getColumnIndex("price"));
                quantity[i] = c.getString(c.getColumnIndex("quantity"));
                item_discount[i] = c.getString(c.getColumnIndex("item_discount"));
                item_discount_type[i] = c.getString(c.getColumnIndex("item_discount_type"));
                modifier[i] = c.getString(c.getColumnIndex("modifier"));
                modifier_price[i] = c.getString(c.getColumnIndex("modifier_price"));
                i++;
            }

            total_orders = i;
        }

        c = db.rawQuery("SELECT * FROM BillSupplementary_v1 WHERE order_number='" + time_stamp + "'", null);
        i = 0;

        if (c.moveToFirst()) {
            discount = new String[c.getCount()];
            discount_type = new String[c.getCount()];

            discount[i] = c.getString(c.getColumnIndex("discount"));
            discount_type[i] = c.getString(c.getColumnIndex("discount_type"));
            i++;

            while (c.moveToNext()) {
                discount[i] = c.getString(c.getColumnIndex("discount"));
                discount_type[i] = c.getString(c.getColumnIndex("discount_type"));
                i++;
            }
        }

        c.close();
        db.close();
    }

    private void FetchCombinations() {
        SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
        Cursor c;
        int i;
        //Fetching Combinations
        c = db.rawQuery("SELECT * FROM PrintOptions_v1 WHERE order_number='" + time_stamp + "' AND table_number='" + table_name + "'", null);
        i = 0;

        if (c.moveToFirst()) {
            combination_array = new String[c.getCount()];
            combination_array[i] = c.getString(c.getColumnIndex("combination"));
            i++;
            while (c.moveToNext()) {
                combination_array[i] = c.getString(c.getColumnIndex("combination"));
                i++;
            }
        }
        c.close();
//        if (print_type.equals("Combination") || print_type.equals("All Separate")) {
//
//        }
//        else if (print_type.equals("All Separate")) {
//            combination_array = new String[Integer.parseInt(customer)];
//
//            for (i = 0; i < Integer.parseInt(customer); i++) {
//                combination_array[i] = String.valueOf(i + 1);
//            }
//        }
//        else if (print_type.equals("Print Together")) {
//            combination_array = new String[1];
//            combination_array[0] = "";
//            boolean first = true;
//
//            for (i = 0; i < Integer.parseInt(customer); i++) {
//                if (first) {
//                    first = false;
//                } else {
//                    combination_array[0] += ",";
//                }
//                combination_array[0] += String.valueOf(i + 1);
//            }
//        }

        db.close();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        String redirected_from = getIntent().getStringExtra("redirect_from");
        Intent intent = new Intent();
        if (redirected_from.equals("Order")) {
            intent = new Intent(PaymentForTablePage.this, OrderForTablePage.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        } else if (redirected_from.equals("History")) {
            intent = new Intent(PaymentForTablePage.this, PaymentHistoryPage.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        }
        intent.putExtra("table_name", table_name);
        intent.putExtra("customer", customer);
        intent.putExtra("time_stamp", time_stamp);
        startActivity(intent);
        PaymentForTablePage.this.finish();
        PaymentForTablePage.this.overridePendingTransition(0, 0);
    }

    private void refreshBillSegment() {
        //billAdapter.notifyDataSetChanged();
        String[] customers_in_selection = combination_array[Integer.parseInt(combination)].split(",");
        int list_total_items = 0;
        int i, j;

        for (i = 0; i < customers_in_selection.length; i++) {
            for (j = 0; j < customer_number.length; j++) {
                if (customers_in_selection[i].equals(customer_number[j])) {
                    list_total_items++;
                }
            }
        }

        list_item = new String[list_total_items];
        list_customer = new String[list_total_items];
        list_quantity = new String[list_total_items];
        list_each = new String[list_total_items];
        list_total = new String[list_total_items];

        int iteration = 0;
        for (i = 0; i < customers_in_selection.length; i++) {
            for (j = 0; j < customer_number.length; j++) {
                if (customers_in_selection[i].equals(customer_number[j])) {
                    list_item[iteration] = item[j];
                    list_customer[iteration] = customer_number[j];
                    list_quantity[iteration] = quantity[j];
                    list_each[iteration] = unit_price[j];
                    list_total[iteration] = price[j];
                    iteration++;
                }
            }
        }

        Log.d("TOTAL ITEMS", String.valueOf(list_total_items));

        billAdapter = new billListAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, R.id.tv_bill_list_name, list_customer);
        lv_order_list.setAdapter(billAdapter);

        subtotal = 0;
        discount_amount = 0;
        tax = 0;
        total = 0;
        balance = 0;

        for (i = 0; i < list_total_items; i++) {
            subtotal += Float.parseFloat(list_total[i]);
        }

        SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
        Cursor c = db.rawQuery("SELECT * FROM BillSupplementary_v1 WHERE order_number='" + time_stamp + "'", null);

        if (c.moveToFirst()) {
            if (c.getString(c.getColumnIndex("discount_type")).equals("Percentage")) {
                discount_amount = (subtotal * Float.parseFloat(c.getString(c.getColumnIndex("discount"))) / 100 );
            } else if (c.getString(c.getColumnIndex("discount_type")).equals("Amount")) {
                discount_amount = Float.parseFloat(c.getString(c.getColumnIndex("discount")));
                float individual_discount = discount_amount / Integer.parseInt(customer);
                discount_amount = individual_discount * customers_in_selection.length;
            }
        }

        c.close();
        db.close();

        tax = (subtotal - discount_amount) * (float)0.15;

        total = subtotal - discount_amount + tax;

        balance = total + tips;

        ///////////////////Value Display//////////////////////
        DecimalFormat df = new DecimalFormat("0.00");
        //df.setRoundingMode(RoundingMode.CEILING);
        TextView tv_order_per_table_subtotal = (TextView) findViewById(R.id.tv_order_per_table_subtotal);
        tv_order_per_table_subtotal.setText("Subtotal $ " + df.format(subtotal));

        TextView tv_order_per_table_discount = (TextView) findViewById(R.id.tv_order_per_table_discount);
        tv_order_per_table_discount.setText("Discount $ " + df.format(discount_amount));

        TextView tv_order_per_table_tax = (TextView) findViewById(R.id.tv_order_per_table_tax);
        tv_order_per_table_tax.setText("Tax $ " + df.format(tax));

        TextView tv_order_per_table_total = (TextView) findViewById(R.id.tv_order_per_table_total);
        tv_order_per_table_total.setText("Total $ " + df.format(total));

        TextView tv_order_per_table_balance_due = (TextView) findViewById(R.id.tv_order_per_table_balance_due);
        tv_order_per_table_balance_due.setText("Balance Due $ " + df.format(balance));
        //////////////////////////////////////////////////////

        //////////////////Value Display in Number Pad//////////////////
        TextView tv_ppt_balance = (TextView) findViewById(R.id.tv_ppt_balance);
        tv_ppt_balance.setText("$ " + df.format(balance));
        ///////////////////////////////////////////////////////////////
    }

    private class billListAdapter extends ArrayAdapter<String> {

        public billListAdapter(Context context, int resource, int textViewResourceId, String[] objects) {
            super(context, resource, textViewResourceId, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.order_per_table_bill_list_item, parent, false);

            TextView tv_bill_list_name = (TextView) row.findViewById(R.id.tv_bill_list_name);
            TextView tv_bill_list_customer = (TextView) row.findViewById(R.id.tv_bill_list_customer);
            TextView tv_bill_list_quantity = (TextView) row.findViewById(R.id.tv_bill_list_quantity);
            TextView tv_bill_list_unit_price = (TextView) row.findViewById(R.id.tv_bill_list_unit_price);
            TextView tv_bill_list_price = (TextView) row.findViewById(R.id.tv_bill_list_price);

            tv_bill_list_name.setText(list_item[position]);
            tv_bill_list_customer.setText(list_customer[position]);
            tv_bill_list_quantity.setText(list_quantity[position]);
            tv_bill_list_unit_price.setText(list_each[position]);
            tv_bill_list_price.setText(list_total[position]);

            return row;
        }
    }
}
