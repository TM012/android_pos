package com.restaware.mightyegg.restawarepos;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ViewTables extends AppCompatActivity {

    Context context;

    int[] XCoordinates, YCoordinates, identifier;
    String[] assignedTableTypes, number, status;
    int tables;

    String[] floors;

    TextView tv_guest_number;
    String guest_number, selected_button;

    Intent viewTableIntent;

    RelativeLayout.LayoutParams layoutParams;
    ViewGroup viewTableLayout;

    String redirectTo;
    String PIN;

    ImageButton ibButton1, ibButton2, ibButton3, ibButton4, ibButton5, ibButton6, ibButton7, ibButton8, ibButton9, ibButton0, ibClear, ibLogin;
    ImageView ivPIN1, ivPIN2, ivPIN3, ivPIN4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_tables);


        ////////////////Initialize Toolbar///////////////////
        InitializeToolbar();
        /////////////////////////////////////////////////////

        /////////////////////Initializing Layout Parameters///////////////////
        layoutParams = new RelativeLayout.LayoutParams(200, 200);

        context = this;
        viewTableIntent = getIntent();

        viewTableLayout = (ViewGroup) findViewById(R.id.layoutViewTableLayout);
        //////////////////////////////////////////////////////////////////////

        if (!TableLayout.selectedFloor.isEmpty()) {
            ////////Fetch Existing Table from Database//////////
            FetchExistingTablesFromDatabase();
            ////////////////////////////////////////////////////

            /////////////Populate View With Existing Tables///////////
            PopulateViewWithExistingTables();
            //////////////////////////////////////////////////////////
        }
    }

    private void InitializeToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_table_layout);
        setSupportActionBar(toolbar);

        TextView tv_floor_name = (TextView) toolbar.findViewById(R.id.tv_floor_name);
        if (TableLayout.selectedFloor.isEmpty()) {
            tv_floor_name.setText("None");
        } else {
            tv_floor_name.setText(TableLayout.selectedFloor);
        }

        ImageButton ib_order_history = (ImageButton) findViewById(R.id.ib_order_history);
        ib_order_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewTables.this, OrderHistoryPage.class);
                startActivity(intent);
            }
        });

        //copy for clock in/out
        ImageButton ib_clock_in_out = (ImageButton) findViewById(R.id.ib_clock_in_out);
        ib_clock_in_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PIN = "";
                redirectTo = "";

                //default dialog for clock in/out
                final String[] clock_options = {"Clock In", "Clock Out"};
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Select Clock Options")
                        .setItems(clock_options, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                redirectTo = clock_options[which];

                                // custom dialog
                                final Dialog clockDialog = new Dialog(context);
                                clockDialog.setContentView(R.layout.clock_in_out_dialog_layout);
                                clockDialog.setTitle("Provide Details");

                                ibButton1 = (ImageButton) clockDialog.findViewById(R.id.ibButton1);
                                ibButton2 = (ImageButton) clockDialog.findViewById(R.id.ibButton2);
                                ibButton3 = (ImageButton) clockDialog.findViewById(R.id.ibButton3);
                                ibButton4 = (ImageButton) clockDialog.findViewById(R.id.ibButton4);
                                ibButton5 = (ImageButton) clockDialog.findViewById(R.id.ibButton5);
                                ibButton6 = (ImageButton) clockDialog.findViewById(R.id.ibButton6);
                                ibButton7 = (ImageButton) clockDialog.findViewById(R.id.ibButton7);
                                ibButton8 = (ImageButton) clockDialog.findViewById(R.id.ibButton8);
                                ibButton9 = (ImageButton) clockDialog.findViewById(R.id.ibButton9);
                                ibButton0 = (ImageButton) clockDialog.findViewById(R.id.ibButton0);

                                ibButton1.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton2.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton3.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton4.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton5.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton6.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton7.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton8.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton9.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton0.setOnTouchListener(new EmployeePINOnTouchListener());

                                ibClear = (ImageButton) clockDialog.findViewById(R.id.ibClear);
                                ibClear.setOnTouchListener(new EmployeePINOnTouchListener());

                                ibLogin = (ImageButton) clockDialog.findViewById(R.id.ibLogin);
                                ibLogin.setOnTouchListener(new EmployeePINOnTouchListener());

                                ivPIN1 = (ImageView) clockDialog.findViewById(R.id.ivPIN1);
                                ivPIN2 = (ImageView) clockDialog.findViewById(R.id.ivPIN2);
                                ivPIN3 = (ImageView) clockDialog.findViewById(R.id.ivPIN3);
                                ivPIN4 = (ImageView) clockDialog.findViewById(R.id.ivPIN4);

                                clockDialog.show();
                            }
                        });
                builder.create();
                builder.show();
            }
        });

        ImageButton ib_floor = (ImageButton) toolbar.findViewById(R.id.ib_floor);
        ib_floor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //////////////////Fetch Tables from Database////////////////
                SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
                String querySetUp = "SELECT * FROM FloorTable_v1";
                Cursor c = db.rawQuery(querySetUp, null);
                floors = new String[c.getCount()];
                int i = 0;
                if (c.moveToFirst()) {
                    floors[i] = c.getString(c.getColumnIndex("name"));
                    i++;
                    while (c.moveToNext()) {
                        floors[i] = c.getString(c.getColumnIndex("name"));
                        i++;
                    }
                }
                c.close();
                db.close();

                Log.d("NUMBER OF FLOORS", String.valueOf(c.getCount()));
                ////////////////////////////////////////////////////////////

                //////////////////Dialog to Select Table//////////////////////
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Select Floor")
                        .setItems(floors, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String selection = floors[which];
                                TableLayout.selectedFloor = selection;
                                Intent intent = new Intent(ViewTables.this, ViewTables.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(intent);
                                ViewTables.this.overridePendingTransition(0, 0);
                                ViewTables.this.finish();
                            }
                        });
                builder.create();
                builder.show();
                //////////////////////////////////////////////////////////////
            }
        });

        ImageButton ib_tab = (ImageButton) findViewById(R.id.ib_tab);
        ib_tab.setOnClickListener(new TabOptions());

        ////////////////////Navigation Drawer//////////////////
        NavigationView navView = (NavigationView) findViewById(R.id.nav_menu);
        navView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                int id = item.getItemId();

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                Intent intent;

                switch (id) {
                    case R.id.navigation_floor:
                        //TODO check for employee permission
                        intent = new Intent(ViewTables.this, TableLayout.class);
                        startActivity(intent);
                        ViewTables.this.finish();
                        ViewTables.this.overridePendingTransition(0, 0);
                        break;
                    case R.id.navigation_menu:
                        intent = new Intent(ViewTables.this, MenuListPage.class);
                        startActivity(intent);
                        ViewTables.this.finish();
                        ViewTables.this.overridePendingTransition(0, 0);
                        break;
                    case R.id.navigation_clock:
                        intent = new Intent(ViewTables.this, ClockHistoryPage.class);
                        startActivity(intent);
                        ViewTables.this.finish();
                        ViewTables.this.overridePendingTransition(0, 0);
                        break;
                    case R.id.navigation_payment:
                        intent = new Intent(ViewTables.this, PaymentHistoryPage.class);
                        startActivity(intent);
                        ViewTables.this.finish();
                        ViewTables.this.overridePendingTransition(0, 0);
                        break;
                    case R.id.navigation_tab:
                        intent = new Intent(ViewTables.this, TabHistoryPage.class);
                        startActivity(intent);
                        ViewTables.this.finish();
                        ViewTables.this.overridePendingTransition(0, 0);
                        break;
                }
                return false;
            }
        });
        ///////////////////////////////////////////////////////
    }

    private class EmployeePINOnTouchListener implements View.OnTouchListener {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            Long tsLong = System.currentTimeMillis()/1000;
            String ts = tsLong.toString();

            if(v.getId() == R.id.ibButton1) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton1.setImageResource(R.drawable.employee_login_1_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "1";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton1.setImageResource(R.drawable.employee_login_button_1);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton2) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton2.setImageResource(R.drawable.employee_login_2_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "2";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton2.setImageResource(R.drawable.employee_login_button_2);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton3) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton3.setImageResource(R.drawable.employee_login_3_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "3";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton3.setImageResource(R.drawable.employee_login_button_3);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton4) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton4.setImageResource(R.drawable.employee_login_4_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "4";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton4.setImageResource(R.drawable.employee_login_button_4);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton5) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton5.setImageResource(R.drawable.employee_login_5_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "5";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton5.setImageResource(R.drawable.employee_login_button_5);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton6) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton6.setImageResource(R.drawable.employee_login_6_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "6";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton6.setImageResource(R.drawable.employee_login_button_6);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton7) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton7.setImageResource(R.drawable.employee_login_7_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "7";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton7.setImageResource(R.drawable.employee_login_button_7);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton8) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton8.setImageResource(R.drawable.employee_login_8_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "8";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton8.setImageResource(R.drawable.employee_login_button_8);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton9) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton9.setImageResource(R.drawable.employee_login_9_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "9";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton9.setImageResource(R.drawable.employee_login_button_9);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton0) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton0.setImageResource(R.drawable.employee_login_0_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "0";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton0.setImageResource(R.drawable.employee_login_button_0);
                        break;
                }
            }
            else if(v.getId() == R.id.ibClear) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibClear.setImageResource(R.drawable.employee_login_clear_clicked);
                        if(PIN.length() > 0) {
                            PIN = PIN.substring(0, PIN.length() - 1);
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibClear.setImageResource(R.drawable.employee_login_clear_button);
                        break;
                }
                Log.d("PIN", PIN);
            }
            else if(v.getId() == R.id.ibLogin) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        boolean return_type;
                        ibLogin.setImageResource(R.drawable.employee_login_login_clicked);
                        if(PIN.length() < 4) {
                            Toast.makeText(getApplicationContext(), "Incomplete PIN", Toast.LENGTH_SHORT).show();
                        }
                        else if(redirectTo.isEmpty()) {
                            Toast.makeText(getApplicationContext(), "Please Select an Option", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
                            Cursor c = db.rawQuery("SELECT * FROM user_tables WHERE employee_pin='" + PIN + "'", null);

                            if (c.moveToFirst()) {
                                if (redirectTo.equals("Clock In")) {
                                    return_type = EmployeeAttendance(PIN, ts, "In");

                                    if (return_type)
                                        Toast.makeText(getApplicationContext(), "Successfully Clocked In", Toast.LENGTH_SHORT).show();
                                    else
                                        Toast.makeText(getApplicationContext(), "Failed to Clock In", Toast.LENGTH_SHORT).show();
                                } else if (redirectTo.equals("Clock Out")) {
                                    return_type = EmployeeAttendance(PIN, ts, "Out");

                                    if (return_type)
                                        Toast.makeText(getApplicationContext(), "Successfully Clocked Out", Toast.LENGTH_SHORT).show();
                                    else
                                        Toast.makeText(getApplicationContext(), "Failed to Clock Out", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), "Invalid PIN", Toast.LENGTH_SHORT).show();
                            }

                            c.close();
                            db.close();
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibLogin.setImageResource(R.drawable.employee_login_login_button);
                        break;
                }
                Log.d("PIN", PIN);
            }

            int length = PIN.length();

            switch (length) {
                case 0:
                    ivPIN1.setImageResource(R.drawable.employee_login_white_dot);
                    ivPIN2.setImageResource(R.drawable.employee_login_white_dot);
                    ivPIN3.setImageResource(R.drawable.employee_login_white_dot);
                    ivPIN4.setImageResource(R.drawable.employee_login_white_dot);
                    break;
                case 1:
                    ivPIN1.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN2.setImageResource(R.drawable.employee_login_white_dot);
                    ivPIN3.setImageResource(R.drawable.employee_login_white_dot);
                    ivPIN4.setImageResource(R.drawable.employee_login_white_dot);
                    break;
                case 2:
                    ivPIN1.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN2.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN3.setImageResource(R.drawable.employee_login_white_dot);
                    ivPIN4.setImageResource(R.drawable.employee_login_white_dot);
                    break;
                case 3:
                    ivPIN1.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN2.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN3.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN4.setImageResource(R.drawable.employee_login_white_dot);
                    break;
                case 4:
                    ivPIN1.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN2.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN3.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN4.setImageResource(R.drawable.employee_login_blue_dot);
                    break;
            }

            return true;
        }
    }

    private boolean EmployeeAttendance(String employee_pin, String clock_time, String clock_mode) {
        SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
        Cursor c = db.rawQuery("SELECT * FROM EmployeeAttendance_v1 WHERE employee_pin='" + employee_pin + "'", null);
        String out_time = "";
        int id = -1;
        boolean first_time = false;
        if (c.moveToFirst()) {
            out_time = c.getString(c.getColumnIndex("out_time"));
            id = c.getInt(c.getColumnIndex("Id"));

            while (c.moveToNext()) {
                out_time = c.getString(c.getColumnIndex("out_time"));
                id = c.getInt(c.getColumnIndex("Id"));
            }
        } else {
            first_time = true;
        }
        c.close();

        if (clock_mode.equals("In")) {
            if (first_time || !out_time.equals("")) {
                db.execSQL("INSERT INTO EmployeeAttendance_v1 (employee_pin, in_time, out_time) VALUES ('" + employee_pin + "', " +
                        "'" + clock_time + "', '');");
                return true;
            }
        } else if (clock_mode.equals("Out")) {
            if (!first_time && out_time.equals("")) {
                db.execSQL("UPDATE EmployeeAttendance_v1 SET out_time='" + clock_time + "' WHERE Id=" + id);
                return true;
            }
        }

        db.close();
        return false;
    }

    private void FetchExistingTablesFromDatabase() {
        SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);

        String querySetUp = "SELECT * FROM TableArrangement_v1 WHERE floor='" + TableLayout.selectedFloor + "'";
        Cursor c = db.rawQuery(querySetUp, null);

        tables = c.getCount();
        XCoordinates = new int[tables];
        YCoordinates = new int[tables];
        assignedTableTypes = new String[tables];
        number = new String[tables];
        identifier = new int[tables];
        status = new String[tables];

        int i = 0;
        if (c.moveToFirst()) {
            XCoordinates[i] = Integer.parseInt(c.getString(c.getColumnIndex("xCoordinate")));
            YCoordinates[i] = Integer.parseInt(c.getString(c.getColumnIndex("yCoordinate")));
            assignedTableTypes[i] = c.getString(c.getColumnIndex("type"));
            number[i] = c.getString(c.getColumnIndex("number"));
            identifier[i] = c.getInt(c.getColumnIndex("Id"));
            status[i] = c.getString(c.getColumnIndex("status"));
            i++;

            while (c.moveToNext()) {
                XCoordinates[i] = Integer.parseInt(c.getString(c.getColumnIndex("xCoordinate")));
                YCoordinates[i] = Integer.parseInt(c.getString(c.getColumnIndex("yCoordinate")));
                assignedTableTypes[i] = c.getString(c.getColumnIndex("type"));
                number[i] = c.getString(c.getColumnIndex("number"));
                identifier[i] = c.getInt(c.getColumnIndex("Id"));
                status[i] = c.getString(c.getColumnIndex("status"));
                i++;
            }
        }

        c.close();
        db.close();
    }

    private void PopulateViewWithExistingTables() {
        ImageButton ib;
        TextView tv;
        int i;

        for (i = 0; i < tables; i++) {
            ib = new ImageButton(context);
            tv = new TextView(context);

            ib.setBackground(null);
            tv.setText(number[i]);
            tv.setTextColor(Color.WHITE);
            tv.setTextSize(20);

            if (assignedTableTypes[i].equals("Rectangle")) {
                //layoutParams = new RelativeLayout.LayoutParams(180, 94);
                if (status[i].equals("vacant")) {
                    ib.setImageResource(R.drawable.table_layout_rectangle_blue);
                } else {
                    ib.setImageResource(R.drawable.table_layout_rectangle_yellow);
                }
            }
            else if (assignedTableTypes[i].equals("Round")) {
                //layoutParams = new RelativeLayout.LayoutParams(180, 171);
                if (status[i].equals("vacant")) {
                    ib.setImageResource(R.drawable.table_layout_ellipse_blue);
                } else {
                    ib.setImageResource(R.drawable.table_layout_ellipse_yellow);
                }
            }
            ib.setLayoutParams(layoutParams);
            ib.setScaleType(ImageView.ScaleType.FIT_XY);

            viewTableLayout.addView(ib);
            viewTableLayout.addView(tv);
            ib.setX((float) XCoordinates[i]);
            ib.setY((float) YCoordinates[i]);
            tv.setX((float) XCoordinates[i] + 100);
            tv.setY((float) YCoordinates[i] + 100);

            //ib.setId(Integer.parseInt(String.valueOf(XCoordinates[i]) + String.valueOf(YCoordinates[i])));
            ib.setId(identifier[i]);
            ib.setOnClickListener(new TableButton());
        }
    }

    private class TableButton implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Log.d("BUTTON ID", String.valueOf(v.getId()));
            selected_button = String.valueOf(v.getId());

            SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);

            Cursor c = db.rawQuery("SELECT * FROM TableArrangement_v1 WHERE Id=" + v.getId(), null);

            if (c.moveToFirst()) {
                String status = c.getString(c.getColumnIndex("status"));

                if (status.equals("vacant")) {
                    // custom dialog
                    final Dialog dialog = new Dialog(context);
                    dialog.setContentView(R.layout.guest_number_dialog);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

                    tv_guest_number = (TextView) dialog.findViewById(R.id.tv_guest_number);
                    ImageButton dialog_button_1 = (ImageButton) dialog.findViewById(R.id.dialog_button_1);
                    ImageButton dialog_button_2 = (ImageButton) dialog.findViewById(R.id.dialog_button_2);
                    ImageButton dialog_button_3 = (ImageButton) dialog.findViewById(R.id.dialog_button_3);
                    ImageButton dialog_button_4 = (ImageButton) dialog.findViewById(R.id.dialog_button_4);
                    ImageButton dialog_button_5 = (ImageButton) dialog.findViewById(R.id.dialog_button_5);
                    ImageButton dialog_button_6 = (ImageButton) dialog.findViewById(R.id.dialog_button_6);
                    ImageButton dialog_button_7 = (ImageButton) dialog.findViewById(R.id.dialog_button_7);
                    ImageButton dialog_button_8 = (ImageButton) dialog.findViewById(R.id.dialog_button_8);
                    ImageButton dialog_button_9 = (ImageButton) dialog.findViewById(R.id.dialog_button_9);
                    ImageButton dialog_button_0 = (ImageButton) dialog.findViewById(R.id.dialog_button_0);

                    ImageButton dialog_button_cross = (ImageButton) dialog.findViewById(R.id.dialog_button_cross);
                    ImageButton dialog_button_tick = (ImageButton) dialog.findViewById(R.id.dialog_button_tick);

                    guest_number = "";

                    dialog_button_1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (guest_number.length() < 3) {
                                guest_number = guest_number + "1";
                            }
                            tv_guest_number.setText(guest_number);
                        }
                    });
                    dialog_button_2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (guest_number.length() < 3) {
                                guest_number = guest_number + "2";
                            }
                            tv_guest_number.setText(guest_number);
                        }
                    });
                    dialog_button_3.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (guest_number.length() < 3) {
                                guest_number = guest_number + "3";
                            }
                            tv_guest_number.setText(guest_number);
                        }
                    });
                    dialog_button_4.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (guest_number.length() < 3) {
                                guest_number = guest_number + "4";
                            }
                            tv_guest_number.setText(guest_number);
                        }
                    });
                    dialog_button_5.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (guest_number.length() < 3) {
                                guest_number = guest_number + "5";
                            }
                            tv_guest_number.setText(guest_number);
                        }
                    });
                    dialog_button_6.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (guest_number.length() < 3) {
                                guest_number = guest_number + "6";
                            }
                            tv_guest_number.setText(guest_number);
                        }
                    });
                    dialog_button_7.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (guest_number.length() < 3) {
                                guest_number = guest_number + "7";
                            }
                            tv_guest_number.setText(guest_number);
                        }
                    });
                    dialog_button_8.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (guest_number.length() < 3) {
                                guest_number = guest_number + "8";
                            }
                            tv_guest_number.setText(guest_number);
                        }
                    });
                    dialog_button_9.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (guest_number.length() < 3) {
                                guest_number = guest_number + "9";
                            }
                            tv_guest_number.setText(guest_number);
                        }
                    });
                    dialog_button_0.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (guest_number.length() < 3) {
                                guest_number = guest_number + "0";
                            }
                            tv_guest_number.setText(guest_number);
                        }
                    });

                    dialog_button_cross.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (guest_number.length() > 0) {
                                guest_number = guest_number.substring(0, guest_number.length() - 1);
                                tv_guest_number.setText(guest_number);
                            } else {
                                dialog.cancel();
                            }
                        }
                    });

                    dialog_button_tick.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (guest_number.length() > 0 && Integer.parseInt(guest_number) > 0) {
                                Log.d("GUEST NUMBER", guest_number);

                                /////////////////Insert Values into Table//////////////////////
                                SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);

                                Cursor c = db.rawQuery("SELECT * FROM TableArrangement_v1 WHERE Id=" + selected_button, null);
                                String table_name = "";
                                Long tsLong = System.currentTimeMillis()/1000;
                                String ts = tsLong.toString();

                                if (c.moveToFirst()) {
                                    table_name = c.getString(c.getColumnIndex("number"));
                                }

                                Log.d("TABLE NAME", table_name);

                                db.execSQL("INSERT INTO CustomerPerTable_v1 " +
                                        "(table_name, customer, time_stamp, floor_name) VALUES ('"
                                        + table_name + "', '" + guest_number + "', '" + ts + "', '" + TableLayout.selectedFloor + "');");

                                db.execSQL("UPDATE TableArrangement_v1 SET status='occupied', order_number='" + ts + "' WHERE Id=" + selected_button);

                                db.execSQL("INSERT INTO BillSupplementary_v1 (order_number, discount, discount_type) VALUES ('" + ts + "', '0', '');");

                                //////////////////////////Debugging to check values passed////////////////////////////
//                                c = db.rawQuery("SELECT * FROM CustomerPerTable_v1 WHERE time_stamp='" + ts + "'", null);
//
//                                if (c.moveToFirst()) {
//                                    Log.d("TABLE RESERVATION", c.getString(c.getColumnIndex("table_name")) + " " + c.getString(c.getColumnIndex("customer")) + " " + c.getString(c.getColumnIndex("time_stamp")));
//                                }
//
//                                c = db.rawQuery("SELECT * FROM TableArrangement_v1 WHERE Id=" + selected_button, null);
//
//                                if (c.moveToFirst()) {
//                                    Log.d("TABLE STATUS", c.getString(c.getColumnIndex("status")) + " " + c.getString(c.getColumnIndex("number")));
//                                }
                                //////////////////////////////////////////////////////////////////////////////////////
                                c.close();
                                db.close();
                                //////////////////////////////////////////////////////////////////////////
                                dialog.cancel();

                                Intent intent = new Intent(ViewTables.this, ViewTables.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(intent);
                                ViewTables.this.overridePendingTransition(0, 0);
                                ViewTables.this.finish();
                            } else {
                                Toast.makeText(getApplicationContext(), "Enter Valid Number", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                    dialog.show();
                } else if (status.equals("occupied")) {
                    //////////////////////////Select Details of Table Ordered From///////////////////////////
                    String table_name = "", customer = "", time_stamp = "";
                    int order_id = 0;
                    c = db.rawQuery("SELECT * FROM TableArrangement_v1 WHERE Id=" + selected_button, null);
                    if (c.moveToFirst()) {
                        table_name = c.getString(c.getColumnIndex("number"));
                    }

                    c.close();

                    c = db.rawQuery("SELECT * FROM CustomerPerTable_v1 WHERE table_name=" + table_name + " AND floor_name='" + TableLayout.selectedFloor + "'", null);

                    if (c.moveToFirst()) {
                        customer = c.getString(c.getColumnIndex("customer"));
                        time_stamp = c.getString(c.getColumnIndex("time_stamp"));
                        order_id = c.getInt(c.getColumnIndex("Id"));
                        while (c.moveToNext()) {
                            customer = c.getString(c.getColumnIndex("customer"));
                            time_stamp = c.getString(c.getColumnIndex("time_stamp"));
                            order_id = c.getInt(c.getColumnIndex("Id"));
                        }
                    }
                    /////////////////////////////////////////////////////////////////////////////////////////
                    //Redirect to Order Page
                    Intent intent = new Intent(ViewTables.this, OrderForTablePage.class);
                    OrderForTablePage.selected_customer = "1";
                    intent.putExtra("table_name", table_name);
                    intent.putExtra("customer", customer);
                    intent.putExtra("time_stamp", time_stamp);
                    intent.putExtra("order_id", String.valueOf(order_id));
                    startActivity(intent);
                    ViewTables.this.overridePendingTransition(0, 0);
                    ViewTables.this.finish();
                }
            }

            db.close();
        }
    }

    private class TabOptions implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            final String[] tab_options = {"Create Tab", "Tab List"};
            //////////////////Default Dialog to Select Tab Options//////////////////////
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Select Tab Options")
                    .setItems(tab_options, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Log.d("SELECTED TAB OPTIONS", tab_options[which]);

                            if (tab_options[which].equals("Create Tab")) {
                                //TODO open custom dialog
                                final Dialog create_tab_dialog = new Dialog(context);
                                create_tab_dialog.setContentView(R.layout.create_tab_dialog_layout);

                                final EditText et_tab_customer_name = (EditText) create_tab_dialog.findViewById(R.id.et_tab_customer_name);
                                final EditText et_tab_card_number = (EditText) create_tab_dialog.findViewById(R.id.et_tab_card_number);

                                Button button_tab_confirm = (Button) create_tab_dialog.findViewById(R.id.button_tab_confirm);
                                button_tab_confirm.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        String customer_name = et_tab_customer_name.getText().toString();
                                        String card_number = et_tab_card_number.getText().toString();

                                        if (!customer_name.isEmpty()) {
                                            SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
                                            Long tsLong = System.currentTimeMillis()/1000;
                                            String ts = tsLong.toString();

                                            //Insert Customer Details for CustomerPerTable
                                            db.execSQL("INSERT INTO CustomerPerTable_v1 (table_name, customer, time_stamp, floor_name) VALUES (" +
                                                    "'" + customer_name + "', '1', '" + ts + "', 'Tab');");

                                            //Insert Discount Details for BillSupplementary
                                            db.execSQL("INSERT INTO BillSupplementary_v1 (order_number, discount, discount_type) VALUES (" +
                                                    "'" + ts + "', '0', '');");

                                            //Insert Print Details for PrintOptions
                                            db.execSQL("INSERT INTO PrintOptions_v1 (order_number, print_options, table_number, combination, " +
                                                    "payment_status) VALUES (" +
                                                    "'" + ts + "', 'Print Together', '" + customer_name + "', '1', 'unpaid');");

                                            if (!card_number.isEmpty()) {
                                                //Insert Card Details for TabDetails
                                                db.execSQL("INSERT INTO TabDetails_v1 (order_number, customer_name, card_number, status) VALUES (" +
                                                        "'" + ts + "', '" + customer_name + "', '" + card_number + "', 'open');");
                                            } else {
                                                //Insert Card Details for TabDetails
                                                db.execSQL("INSERT INTO TabDetails_v1 (order_number, customer_name, card_number, status) VALUES (" +
                                                        "'" + ts + "', '" + customer_name + "', '', 'open');");
                                            }

                                            db.close();

                                            Toast.makeText(getApplicationContext(), "Tab created for " + customer_name, Toast.LENGTH_SHORT).show();

                                            create_tab_dialog.cancel();
                                        } else {
                                            Toast.makeText(getApplicationContext(), "Please insert a valid name", Toast.LENGTH_SHORT).show();
                                        }

                                    }
                                });

                                create_tab_dialog.show();
                            } else if (tab_options[which].equals("Tab List")) {
                                //TODO open custom dialog
                                final SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
                                Cursor c = db.rawQuery("SELECT * FROM TabDetails_v1 WHERE status='open'", null);

                                if (c.moveToFirst()) {
                                    final String[] open_tabs = new String[c.getCount()];
                                    final String[] open_orders = new String[c.getCount()];
                                    int i = 0;
                                    open_tabs[i] = c.getString(c.getColumnIndex("customer_name"));
                                    open_orders[i] = c.getString(c.getColumnIndex("order_number"));
                                    i++;

                                    while (c.moveToNext()) {
                                        open_tabs[i] = c.getString(c.getColumnIndex("customer_name"));
                                        open_orders[i] = c.getString(c.getColumnIndex("order_number"));
                                        i++;
                                    }

                                    AlertDialog.Builder tab_list_builder = new AlertDialog.Builder(context);
                                    tab_list_builder.setTitle("Open Tabs")
                                            .setItems(open_tabs, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    //TODO open order page
                                                    //////////////////////////Select Details of Table Ordered From///////////////////////////
                                                    SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);

                                                    String table_name, customer = "", time_stamp = "";
                                                    int order_id = 0;
                                                    Cursor c;

                                                    table_name = open_tabs[which];

                                                    c = db.rawQuery("SELECT * FROM CustomerPerTable_v1 WHERE table_name='" + table_name + "' " +
                                                            "AND time_stamp='" + open_orders[which] + "'", null);

                                                    if (c.moveToFirst()) {
                                                        customer = c.getString(c.getColumnIndex("customer"));
                                                        time_stamp = c.getString(c.getColumnIndex("time_stamp"));
                                                        order_id = c.getInt(c.getColumnIndex("Id"));
                                                        while (c.moveToNext()) {
                                                            customer = c.getString(c.getColumnIndex("customer"));
                                                            time_stamp = c.getString(c.getColumnIndex("time_stamp"));
                                                            order_id = c.getInt(c.getColumnIndex("Id"));
                                                        }
                                                    }
                                                    /////////////////////////////////////////////////////////////////////////////////////////
                                                    //Redirect to Order Page
                                                    Intent intent = new Intent(ViewTables.this, OrderForTablePage.class);
                                                    OrderForTablePage.selected_customer = "1";
                                                    intent.putExtra("table_name", table_name);
                                                    intent.putExtra("customer", customer);
                                                    intent.putExtra("time_stamp", time_stamp);
                                                    intent.putExtra("order_id", String.valueOf(order_id));
                                                    startActivity(intent);
                                                    ViewTables.this.overridePendingTransition(0, 0);
                                                    ViewTables.this.finish();

                                                    c.close();
                                                    db.close();
                                                }
                                            });
                                    tab_list_builder.create();
                                    tab_list_builder.show();
                                } else {
                                    Toast.makeText(getApplicationContext(), "No Open Tabs", Toast.LENGTH_SHORT).show();
                                }

                                c.close();
                                db.close();
                            }

                            dialog.cancel();
                        }
                    });
            builder.create();
            builder.show();
            /////////////////////////////////////////////////////////////////////////
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent intent = new Intent(ViewTables.this, TableLayout.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//        startActivity(intent);
//        ViewTables.this.finish();
    }
}
