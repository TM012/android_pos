package com.restaware.mightyegg.restawarepos;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OrderHistoryPage extends AppCompatActivity {

    Context context;
    String[] floors;

    SQLiteDatabase db;
    Cursor c;
    int i,j;

    String[] unique_order_numbers, unique_table_numbers;
    boolean[] unique_payment_status;
    String[] log_order_numbers, log_items, log_quantities, log_unit_prices, log_prices, log_table_numbers, log_customer_numbers;
    String[] paid_order_numbers, paid_table_numbers, paid_total;
    String[] unpaid_order_numbers, unpaid_items, unpaid_quantities, unpaid_unit_prices, unpaid_prices, unpaid_table_numbers, unpaid_customer_numbers;
    String[] unpaid_unique_orders, unpaid_unique_tables, unpaid_unique_total, unpaid_unique_due;
    int unique_orders, log_orders, paid_orders, unpaid_orders;
    String[] list_item, list_customer, list_quantity, list_each, list_total;

    RecyclerView rv_order_list, rv_all_order_list;

    String redirectTo;
    String PIN;

    ImageButton ibButton1, ibButton2, ibButton3, ibButton4, ibButton5, ibButton6, ibButton7, ibButton8, ibButton9, ibButton0, ibClear, ibLogin;
    ImageView ivPIN1, ivPIN2, ivPIN3, ivPIN4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history_page);

        context = this;
        db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);

        InitializeToolbar();
        FetchUniqueOrders();
        CheckIfPaid();
        InflateRecyclerView();
        InflateAllOrdersRecyclerView();

        db.close();
    }

    private void InflateAllOrdersRecyclerView() {
        rv_all_order_list = (RecyclerView) findViewById(R.id.rv_all_order_list);
        rv_all_order_list.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        if (unique_order_numbers != null && unique_order_numbers.length > 0 && rv_all_order_list != null) {
            rv_all_order_list.setAdapter(new AllOrderListAdapter());
        }

        rv_all_order_list.setLayoutManager(linearLayoutManager);
    }

    public class AllOrderListAdapter extends RecyclerView.Adapter<AllOrderListViewHolder> {
        private final View.OnClickListener allOrderListItemClickListener = new RecyclerViewItemClickListenerForAllOrders();

        @Override
        public AllOrderListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_history_all_order_list_item, parent, false);
            view.setOnClickListener(allOrderListItemClickListener);
            AllOrderListViewHolder holder = new AllOrderListViewHolder(view);
            return holder;
        }

        @Override
        public void onBindViewHolder(AllOrderListViewHolder holder, int position) {
            holder.tv_order_history_table_name.setText(unique_table_numbers[position]);

            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
            Date date = new Date(Long.parseLong(unique_order_numbers[position]) * 1000);
            holder.tv_order_history_order_date.setText(sdf.format(date));

            int matched_order_number = -1;
            double paid_amount = 0;

            if (unpaid_unique_orders != null) {
                for (i = 0; i < unpaid_unique_orders.length; i++) {
                    if (unique_order_numbers[position].equals(unpaid_unique_orders[i])) {
                        matched_order_number = i;
                        break;
                    }
                }
            }

            if (unique_payment_status[position]) {
                if (matched_order_number == -1) {
                    holder.tv_order_history_amount_paid.setText("Paid");

                    //TODO fetch and display total amount
                    db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);

                    c = db.rawQuery("SELECT * FROM OrderLog_v1 WHERE order_number='" + unique_order_numbers[position] + "' AND " +
                            "table_number='" + unique_table_numbers[position] + "'", null);

                    if (c.moveToFirst()) {
                        paid_amount += Double.parseDouble(c.getString(c.getColumnIndex("total")));
                        paid_amount += Double.parseDouble(c.getString(c.getColumnIndex("tips")));

                        while (c.moveToNext()) {
                            paid_amount += Double.parseDouble(c.getString(c.getColumnIndex("total")));
                            paid_amount += Double.parseDouble(c.getString(c.getColumnIndex("tips")));
                        }
                    }

                    //TODO consider tips

                    db.close();

                    DecimalFormat df = new DecimalFormat("0.00");
                    holder.tv_order_history_amount_due.setText("$ " + df.format(paid_amount));
                } else {
                    holder.tv_order_history_amount_paid.setText("$ " + unpaid_unique_due[matched_order_number]);
                    holder.tv_order_history_amount_due.setText("$ " + unpaid_unique_total[matched_order_number]);
                }
            } else {
                //TODO fetch amount unpaid
                if (matched_order_number != -1) {
                    holder.tv_order_history_amount_paid.setText("$ " + unpaid_unique_due[matched_order_number]);
                    holder.tv_order_history_amount_due.setText("$ " + unpaid_unique_total[matched_order_number]);
                }
            }
        }

        @Override
        public int getItemCount() {
            return unique_order_numbers.length;
        }
    }

    public class AllOrderListViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_order_history_table_name, tv_order_history_order_date, tv_order_history_amount_due, tv_order_history_amount_paid;

        public AllOrderListViewHolder(View itemView) {
            super(itemView);

            tv_order_history_table_name = (TextView) itemView.findViewById(R.id.tv_order_history_table_name);
            tv_order_history_order_date = (TextView) itemView.findViewById(R.id.tv_order_history_order_date);
            tv_order_history_amount_due = (TextView) itemView.findViewById(R.id.tv_order_history_amount_due);
            tv_order_history_amount_paid = (TextView) itemView.findViewById(R.id.tv_order_history_amount_paid);
        }
    }

    private class RecyclerViewItemClickListenerForAllOrders implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            int itemPosition = rv_all_order_list.getChildLayoutPosition(v);
            Log.d("CLICK POSITION", String.valueOf(itemPosition));

            // custom dialog
            final Dialog dialog = new Dialog(context);
            dialog.setContentView(R.layout.order_history_order_list_item);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            TextView tv_order_number = (TextView) dialog.findViewById(R.id.tv_order_number);
            TextView tv_order_per_table_subtotal = (TextView) dialog.findViewById(R.id.tv_order_per_table_subtotal);
            TextView tv_order_per_table_discount = (TextView) dialog.findViewById(R.id.tv_order_per_table_discount);
            TextView tv_order_per_table_tax = (TextView) dialog.findViewById(R.id.tv_order_per_table_tax);
            TextView tv_order_per_table_total = (TextView) dialog.findViewById(R.id.tv_order_per_table_total);
            TextView tv_order_per_table_balance_due = (TextView) dialog.findViewById(R.id.tv_order_per_table_balance_due);
            ListView lv_order_list = (ListView) dialog.findViewById(R.id.lv_order_list);

            //display values on dialog
            tv_order_number.setText("Order Number: " + unique_order_numbers[itemPosition]);

            int itemCount = 0;
            double subtotal = 0;
            double discount_amount = 0;
            double tax = 0;
            double bill_total = 0;
            double balance_due = 0;
            double paid_amount = 0;

            if (log_orders > 0) {
                for (i = 0; i < log_order_numbers.length; i++) {
                    if (unique_order_numbers[itemPosition].equals(log_order_numbers[i])) {
                        itemCount++;
                    }
                }

                Log.d("ITEM COUNT", String.valueOf(itemCount));

                if (itemCount > 0) {
                    list_item = new String[itemCount];
                    list_customer = new String[itemCount];
                    list_quantity = new String[itemCount];
                    list_each = new String[itemCount];
                    list_total = new String[itemCount];

                    itemCount = 0;

                    for (i = 0; i < log_order_numbers.length; i++) {
                        if (unique_order_numbers[itemPosition].equals(log_order_numbers[i])) {
                            list_item[itemCount] = log_items[i];
                            list_customer[itemCount] = log_customer_numbers[i];
                            list_quantity[itemCount] = log_quantities[i];
                            list_each[itemCount] = log_unit_prices[i];
                            list_total[itemCount] = log_prices[i];

                            subtotal = subtotal + Double.parseDouble(list_total[itemCount]);
                            itemCount++;
                        }
                    }

                    billListAdapter billAdapter = new billListAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, R.id.tv_bill_list_name, list_customer);
                    lv_order_list.setAdapter(billAdapter);
                }

                DecimalFormat df = new DecimalFormat("0.00");
                tv_order_per_table_subtotal.setText("Subtotal $ " + df.format(subtotal));

                db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);

                c = db.rawQuery("SELECT * FROM BillSupplementary_v1 WHERE order_number='" + unique_order_numbers[itemPosition] + "'", null);

                if (c.moveToFirst()) {
                    if (c.getString(c.getColumnIndex("discount_type")).equals("Percentage")) {
                        discount_amount = (subtotal * Float.parseFloat(c.getString(c.getColumnIndex("discount"))) / 100);
                    } else if (c.getString(c.getColumnIndex("discount_type")).equals("Amount")) {
                        discount_amount = Float.parseFloat(c.getString(c.getColumnIndex("discount")));
                    }
                }

                c = db.rawQuery("SELECT * FROM OrderLog_v1 WHERE order_number='" + unique_order_numbers[itemPosition] + "' AND " +
                        "table_number='" + unique_table_numbers[itemPosition] + "'", null);

                if (c.moveToFirst()) {
                    paid_amount += Double.parseDouble(c.getString(c.getColumnIndex("total")));

                    while (c.moveToNext()) {
                        paid_amount += Double.parseDouble(c.getString(c.getColumnIndex("total")));
                    }
                }

                db.close();

                tv_order_per_table_discount.setText("Discount $ " + df.format(discount_amount));

                tax = (subtotal - discount_amount) * 0.15;

                tv_order_per_table_tax.setText("Tax $ " + df.format(tax));

                bill_total = subtotal - discount_amount + tax;

                tv_order_per_table_total.setText("Total $ " + df.format(bill_total));

                balance_due = Double.parseDouble(df.format(bill_total)) - Double.parseDouble(df.format(paid_amount));

                tv_order_per_table_balance_due.setText("Balance Due $ " + df.format(balance_due));
            }

            dialog.show();
        }
    }

    //Step 3 for RecyclerView
    private void InflateRecyclerView() {
        rv_order_list = (RecyclerView) findViewById(R.id.rv_order_list);
        rv_order_list.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        if (unpaid_unique_orders != null && unpaid_unique_orders.length > 0 && rv_order_list != null) {
            rv_order_list.setAdapter(new OrderListAdapter());

            //rv_order_list.setOnClickListener(new RecyclerViewItemClickListener());
        }

        rv_order_list.setLayoutManager(linearLayoutManager);
    }

    //Step 2 for RecyclerView
    public class OrderListAdapter extends RecyclerView.Adapter<OrderListViewHolder> {
        private final View.OnClickListener orderListItemClickListener = new RecyclerViewItemClickListener();

        @Override
        public OrderListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_history_order_list_item, parent, false);
            view.setOnClickListener(orderListItemClickListener);
            OrderListViewHolder holder = new OrderListViewHolder(view);
            return holder;
        }

        @Override
        public void onBindViewHolder(OrderListViewHolder holder, int position) {
            holder.tv_order_number.setText("Order Number: " + unpaid_unique_orders[position]);

            int itemCount = 0;
            double subtotal = 0;
            double discount_amount = 0;
            double tax = 0;
            double bill_total = 0;
            double balance_due = 0;
            double paid_amount = 0;

            if (log_orders > 0) {
                for (i = 0; i < log_order_numbers.length; i++) {
                    if (unpaid_unique_orders[position].equals(log_order_numbers[i])) {
                        itemCount++;
                    }
                }

                Log.d("ITEM COUNT", String.valueOf(itemCount));

                if (itemCount > 0) {
                    list_item = new String[itemCount];
                    list_customer = new String[itemCount];
                    list_quantity = new String[itemCount];
                    list_each = new String[itemCount];
                    list_total = new String[itemCount];

                    itemCount = 0;

                    for (i = 0; i < log_order_numbers.length; i++) {
                        if (unpaid_unique_orders[position].equals(log_order_numbers[i])) {
                            list_item[itemCount] = log_items[i];
                            list_customer[itemCount] = log_customer_numbers[i];
                            list_quantity[itemCount] = log_quantities[i];
                            list_each[itemCount] = log_unit_prices[i];
                            list_total[itemCount] = log_prices[i];

                            subtotal = subtotal + Double.parseDouble(list_total[itemCount]);
                            itemCount++;
                        }
                    }

                    billListAdapter billAdapter = new billListAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, R.id.tv_bill_list_name, list_customer);
                    holder.lv_order_list.setAdapter(billAdapter);
                }

                DecimalFormat df = new DecimalFormat("0.00");
                holder.tv_order_per_table_subtotal.setText("Subtotal $ " + df.format(subtotal));

                db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);

                c = db.rawQuery("SELECT * FROM BillSupplementary_v1 WHERE order_number='" + unpaid_unique_orders[position] + "'", null);

                if (c.moveToFirst()) {
                    if (c.getString(c.getColumnIndex("discount_type")).equals("Percentage")) {
                        discount_amount = (subtotal * Float.parseFloat(c.getString(c.getColumnIndex("discount"))) / 100 );
                    } else if (c.getString(c.getColumnIndex("discount_type")).equals("Amount")) {
                        discount_amount = Float.parseFloat(c.getString(c.getColumnIndex("discount")));
                    }
                }

                c = db.rawQuery("SELECT * FROM OrderLog_v1 WHERE order_number='" + unpaid_unique_orders[position] + "' AND " +
                        "table_number='" + unpaid_unique_tables[position] + "'", null);

                if (c.moveToFirst()) {
                    paid_amount += Double.parseDouble(c.getString(c.getColumnIndex("total")));

                    while (c.moveToNext()) {
                        paid_amount += Double.parseDouble(c.getString(c.getColumnIndex("total")));
                    }
                }

                db.close();

                holder.tv_order_per_table_discount.setText("Discount $ " + df.format(discount_amount));

                tax = (subtotal - discount_amount) * 0.15;

                holder.tv_order_per_table_tax.setText("Tax $ " + df.format(tax));

                bill_total = subtotal - discount_amount + tax;
                unpaid_unique_total[position] = df.format(bill_total);

                holder.tv_order_per_table_total.setText("Total $ " + df.format(bill_total));

                balance_due = Double.parseDouble(df.format(bill_total)) - Double.parseDouble(df.format(paid_amount));
                unpaid_unique_due[position] = df.format(balance_due);

                holder.tv_order_per_table_balance_due.setText("Balance Due $ " + df.format(balance_due));
            }
        }

        @Override
        public int getItemCount() {
            return unpaid_unique_orders.length;
        }
    }

    //Step 1 for RecyclerView
    public class OrderListViewHolder extends RecyclerView.ViewHolder{

        public TextView tv_order_number, tv_order_per_table_subtotal, tv_order_per_table_discount, tv_order_per_table_tax, tv_order_per_table_total, tv_order_per_table_balance_due;
        public ListView lv_order_list;

        public OrderListViewHolder(View itemView) {
            super(itemView);

            tv_order_number = (TextView) itemView.findViewById(R.id.tv_order_number);
            tv_order_per_table_subtotal = (TextView) itemView.findViewById(R.id.tv_order_per_table_subtotal);
            tv_order_per_table_discount = (TextView) itemView.findViewById(R.id.tv_order_per_table_discount);
            tv_order_per_table_tax = (TextView) itemView.findViewById(R.id.tv_order_per_table_tax);
            tv_order_per_table_total = (TextView) itemView.findViewById(R.id.tv_order_per_table_total);
            tv_order_per_table_balance_due = (TextView) itemView.findViewById(R.id.tv_order_per_table_balance_due);
            lv_order_list = (ListView) itemView.findViewById(R.id.lv_order_list);

        }
    }

    private class RecyclerViewItemClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            int itemPosition = rv_order_list.getChildLayoutPosition(v);
            Log.d("CLICK POSITION", String.valueOf(itemPosition));

            // custom dialog
            final Dialog dialog = new Dialog(context);
            dialog.setContentView(R.layout.order_history_order_list_item);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            TextView tv_order_number = (TextView) dialog.findViewById(R.id.tv_order_number);
            TextView tv_order_per_table_subtotal = (TextView) dialog.findViewById(R.id.tv_order_per_table_subtotal);
            TextView tv_order_per_table_discount = (TextView) dialog.findViewById(R.id.tv_order_per_table_discount);
            TextView tv_order_per_table_tax = (TextView) dialog.findViewById(R.id.tv_order_per_table_tax);
            TextView tv_order_per_table_total = (TextView) dialog.findViewById(R.id.tv_order_per_table_total);
            TextView tv_order_per_table_balance_due = (TextView) dialog.findViewById(R.id.tv_order_per_table_balance_due);
            ListView lv_order_list = (ListView) dialog.findViewById(R.id.lv_order_list);

            //display values on dialog
            tv_order_number.setText("Order Number: " + unpaid_unique_orders[itemPosition]);

            int itemCount = 0;
            double subtotal = 0;
            double discount_amount = 0;
            double tax = 0;
            double bill_total = 0;
            double balance_due = 0;
            double paid_amount = 0;

            if (log_orders > 0) {
                for (i = 0; i < log_order_numbers.length; i++) {
                    if (unpaid_unique_orders[itemPosition].equals(log_order_numbers[i])) {
                        itemCount++;
                    }
                }

                Log.d("ITEM COUNT", String.valueOf(itemCount));

                if (itemCount > 0) {
                    list_item = new String[itemCount];
                    list_customer = new String[itemCount];
                    list_quantity = new String[itemCount];
                    list_each = new String[itemCount];
                    list_total = new String[itemCount];

                    itemCount = 0;

                    for (i = 0; i < log_order_numbers.length; i++) {
                        if (unpaid_unique_orders[itemPosition].equals(log_order_numbers[i])) {
                            list_item[itemCount] = log_items[i];
                            list_customer[itemCount] = log_customer_numbers[i];
                            list_quantity[itemCount] = log_quantities[i];
                            list_each[itemCount] = log_unit_prices[i];
                            list_total[itemCount] = log_prices[i];

                            subtotal = subtotal + Double.parseDouble(list_total[itemCount]);
                            itemCount++;
                        }
                    }

                    billListAdapter billAdapter = new billListAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, R.id.tv_bill_list_name, list_customer);
                    lv_order_list.setAdapter(billAdapter);
                }

                DecimalFormat df = new DecimalFormat("0.00");
                tv_order_per_table_subtotal.setText("Subtotal $ " + df.format(subtotal));

                db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);

                c = db.rawQuery("SELECT * FROM BillSupplementary_v1 WHERE order_number='" + unpaid_unique_orders[itemPosition] + "'", null);

                if (c.moveToFirst()) {
                    if (c.getString(c.getColumnIndex("discount_type")).equals("Percentage")) {
                        discount_amount = (subtotal * Float.parseFloat(c.getString(c.getColumnIndex("discount"))) / 100);
                    } else if (c.getString(c.getColumnIndex("discount_type")).equals("Amount")) {
                        discount_amount = Float.parseFloat(c.getString(c.getColumnIndex("discount")));
                    }
                }

                c = db.rawQuery("SELECT * FROM OrderLog_v1 WHERE order_number='" + unpaid_unique_orders[itemPosition] + "' AND " +
                        "table_number='" + unpaid_unique_tables[itemPosition] + "'", null);

                if (c.moveToFirst()) {
                    paid_amount += Double.parseDouble(c.getString(c.getColumnIndex("total")));

                    while (c.moveToNext()) {
                        paid_amount += Double.parseDouble(c.getString(c.getColumnIndex("total")));
                    }
                }

                db.close();

                tv_order_per_table_discount.setText("Discount $ " + df.format(discount_amount));

                tax = (subtotal - discount_amount) * 0.15;

                tv_order_per_table_tax.setText("Tax $ " + df.format(tax));

                bill_total = subtotal - discount_amount + tax;

                tv_order_per_table_total.setText("Total $ " + df.format(bill_total));

                balance_due = Double.parseDouble(df.format(bill_total)) - Double.parseDouble(df.format(paid_amount));

                tv_order_per_table_balance_due.setText("Balance Due $ " + df.format(balance_due));
            }

            dialog.show();
        }
    }

    private class billListAdapter extends ArrayAdapter<String> {

        public billListAdapter(Context context, int resource, int textViewResourceId, String[] objects) {
            super(context, resource, textViewResourceId, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.order_dashboard_list_item, parent, false);

            TextView tv_bill_list_name = (TextView) row.findViewById(R.id.tv_bill_list_name);
            TextView tv_bill_list_customer = (TextView) row.findViewById(R.id.tv_bill_list_customer);
            TextView tv_bill_list_quantity = (TextView) row.findViewById(R.id.tv_bill_list_quantity);
            TextView tv_bill_list_unit_price = (TextView) row.findViewById(R.id.tv_bill_list_unit_price);
            TextView tv_bill_list_price = (TextView) row.findViewById(R.id.tv_bill_list_price);

            tv_bill_list_name.setText(list_item[position]);
            tv_bill_list_customer.setText(list_customer[position]);
            tv_bill_list_quantity.setText(list_quantity[position]);
            tv_bill_list_unit_price.setText(list_each[position]);
            tv_bill_list_price.setText(list_total[position]);

            return row;
        }
    }

    private void FetchUniqueOrders() {
        //all unique order numbers
        c = db.rawQuery("SELECT * FROM CustomerPerTable_v1 WHERE floor_name='" + TableLayout.selectedFloor + "'", null);

        unique_orders = 0;

        if (c.moveToFirst()) {
            unique_order_numbers = new String[c.getCount()];
            unique_table_numbers = new String[c.getCount()];
            unique_payment_status = new boolean[c.getCount()];
            i = 0;

            unique_order_numbers[i] = c.getString(c.getColumnIndex("time_stamp"));
            unique_table_numbers[i] = c.getString(c.getColumnIndex("table_name"));
            unique_payment_status[i] = false;
            i++;

            while (c.moveToNext()) {
                unique_order_numbers[i] = c.getString(c.getColumnIndex("time_stamp"));
                unique_table_numbers[i] = c.getString(c.getColumnIndex("table_name"));
                unique_payment_status[i] = false;
                i++;
            }
            unique_orders = c.getCount();
            Log.d("UNIQUE ORDERS", String.valueOf(unique_orders));
        }

        //all orders
        c = db.rawQuery("SELECT * FROM OrderPerCustomer_v1", null);

        log_orders = 0;

        if (c.moveToFirst()) {
            log_order_numbers = new String[c.getCount()];
            log_items = new String[c.getCount()];
            log_quantities = new String[c.getCount()];
            log_unit_prices = new String[c.getCount()];
            log_prices = new String[c.getCount()];
            log_table_numbers = new String[c.getCount()];
            log_customer_numbers = new String[c.getCount()];
            i = 0;

            log_order_numbers[i] = c.getString(c.getColumnIndex("order_number"));
            log_items[i] = c.getString(c.getColumnIndex("item"));
            log_quantities[i] = c.getString(c.getColumnIndex("quantity"));
            log_unit_prices[i] = c.getString(c.getColumnIndex("unit_price"));
            log_prices[i] = c.getString(c.getColumnIndex("price"));
            log_table_numbers[i] = c.getString(c.getColumnIndex("table_number"));
            log_customer_numbers[i] = c.getString(c.getColumnIndex("customer_number"));
            i++;

            while (c.moveToNext()) {
                log_order_numbers[i] = c.getString(c.getColumnIndex("order_number"));
                log_items[i] = c.getString(c.getColumnIndex("item"));
                log_quantities[i] = c.getString(c.getColumnIndex("quantity"));
                log_unit_prices[i] = c.getString(c.getColumnIndex("unit_price"));
                log_prices[i] = c.getString(c.getColumnIndex("price"));
                log_table_numbers[i] = c.getString(c.getColumnIndex("table_number"));
                log_customer_numbers[i] = c.getString(c.getColumnIndex("customer_number"));
                i++;
            }
            log_orders = c.getCount();
            Log.d("INDIVIDUAL ORDERS", String.valueOf(log_orders));
        }

        //all paid orders
        c = db.rawQuery("SELECT * FROM OrderLog_v1", null);

        paid_orders = 0;

        if (c.moveToFirst()) {
            paid_order_numbers = new String[c.getCount()];
            paid_table_numbers = new String[c.getCount()];
            paid_total = new String[c.getCount()];
            i = 0;

            paid_order_numbers[i] = c.getString(c.getColumnIndex("order_number"));
            paid_table_numbers[i] = c.getString(c.getColumnIndex("table_number"));
            paid_total[i] = c.getString(c.getColumnIndex("total"));
            i++;

            while (c.moveToNext()) {
                paid_order_numbers[i] = c.getString(c.getColumnIndex("order_number"));
                paid_table_numbers[i] = c.getString(c.getColumnIndex("table_number"));
                paid_total[i] = c.getString(c.getColumnIndex("total"));
                i++;
            }
            paid_orders = c.getCount();
            Log.d("PAID ORDERS", String.valueOf(paid_orders));
        }

        //check payment status
        if (unique_orders > 0) {
            //PopulateUnpaidOrderList();
            UnpaidOrderListFromDatabase();
        }

//        //debug
//        for (i = 0; i < order_numbers.length; i++) {
//            Log.d("ORDER NUMBER", order_numbers[i]);
//        }
//        if (unique_table_numbers != null) {
//            Log.d("TABLE NUMBERS", "table numbers: " + unique_table_numbers.length);
//        }
    }

    private void CheckIfPaid() {
        if (unique_order_numbers != null) {
            if (paid_order_numbers != null) {
                for (i = 0; i < unique_order_numbers.length; i++) {
                    for (j = 0; j < paid_order_numbers.length; j++) {
                        if (unique_order_numbers[i].equals(paid_order_numbers[j])) {
                            unique_payment_status[i] = true;
                            break;
                        }
                    }
                }
            }
        }
    }

    private void UnpaidOrderListFromDatabase() {
        unpaid_orders = 0;

        c = db.rawQuery("SELECT * FROM TableArrangement_v1 WHERE status='occupied' AND floor='" + TableLayout.selectedFloor + "'", null);

        if (c.moveToFirst()) {
            unpaid_unique_orders = new String[c.getCount()];
            unpaid_unique_tables = new String[c.getCount()];
            unpaid_unique_total = new String[c.getCount()];
            unpaid_unique_due = new String[c.getCount()];
            //paid_total = new String[c.getCount()];
            i = 0;

            unpaid_unique_orders[i] = c.getString(c.getColumnIndex("order_number"));
            unpaid_unique_tables[i] = c.getString(c.getColumnIndex("number"));
            unpaid_unique_total[i] = "0";
            unpaid_unique_due[i] = "0";
            //paid_total[i] = c.getString(c.getColumnIndex("total"));
            i++;

            while (c.moveToNext()) {
                unpaid_unique_orders[i] = c.getString(c.getColumnIndex("order_number"));
                unpaid_unique_tables[i] = c.getString(c.getColumnIndex("number"));
                unpaid_unique_total[i] = "0";
                unpaid_unique_due[i] = "0";
                //paid_total[i] = c.getString(c.getColumnIndex("total"));
                i++;
            }

            unpaid_orders = c.getCount();
            Log.d("PAID ORDERS", String.valueOf(unpaid_orders));
        }
    }

    private void PopulateUnpaidOrderList() {
        if (paid_orders > 0) {
            for (i = 0; i < unique_order_numbers.length; i++) {
                for (j = 0; j < paid_order_numbers.length; j++) {
                    if (unique_order_numbers[i].equals(paid_order_numbers[j])) {
                        unique_payment_status[i] = true;
                        break;
                    }
                }
            }
        }

        int count = 0;

        //count unpaid orders
        for (i = 0; i < unique_payment_status.length; i++) {
            if (!unique_payment_status[i]) {
                count++;
            }
        }
        Log.d("UNPAID ORDERS", String.valueOf(count));

//        unpaid_order_numbers = new String[count];
//        unpaid_items = new String[count];
//        unpaid_quantities = new String[count];
//        unpaid_unit_prices = new String[count];
//        unpaid_prices = new String[count];
//        unpaid_table_numbers = new String[count];
//        unpaid_customer_numbers = new String[count];

        unpaid_unique_orders = new String[count];
        unpaid_unique_tables = new String[count];
        int iterate = 0;

        //populate unpaid order number array
        if (count > 0) {
            for (i = 0; i < unique_order_numbers.length; i++) {
                if (!unique_payment_status[i]) {
                    unpaid_unique_orders[iterate] = unique_order_numbers[i];
                    unpaid_unique_tables[iterate] = unique_table_numbers[i];
                    iterate++;
                }
            }
        }
    }

    private void InitializeToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_table_layout);
        setSupportActionBar(toolbar);

        TextView tv_floor_name = (TextView) toolbar.findViewById(R.id.tv_floor_name);
        if (TableLayout.selectedFloor.isEmpty()) {
            tv_floor_name.setText("None");
        } else {
            tv_floor_name.setText(TableLayout.selectedFloor);
        }

        ImageButton ib_floor = (ImageButton) toolbar.findViewById(R.id.ib_floor);
        ib_floor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //////////////////Fetch Tables from Database////////////////
                SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
                String querySetUp = "SELECT * FROM FloorTable_v1";
                Cursor c = db.rawQuery(querySetUp, null);
                floors = new String[c.getCount()];
                int i = 0;
                if (c.moveToFirst()) {
                    floors[i] = c.getString(c.getColumnIndex("name"));
                    i++;
                    while (c.moveToNext()) {
                        floors[i] = c.getString(c.getColumnIndex("name"));
                        i++;
                    }
                }
                c.close();
                db.close();

                Log.d("NUMBER OF FLOORS", String.valueOf(c.getCount()));
                ////////////////////////////////////////////////////////////

                //////////////////Dialog to Select Table//////////////////////
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Select Floor")
                        .setItems(floors, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                TableLayout.selectedFloor = floors[which];
                                Intent intent = new Intent(OrderHistoryPage.this, OrderHistoryPage.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(intent);
                                OrderHistoryPage.this.overridePendingTransition(0, 0);
                                OrderHistoryPage.this.finish();
                            }
                        });
                builder.create();
                builder.show();
                //////////////////////////////////////////////////////////////
            }
        });

        //copy for clock in/out
        ImageButton ib_clock_in_out = (ImageButton) findViewById(R.id.ib_clock_in_out);
        ib_clock_in_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PIN = "";
                redirectTo = "";

                //default dialog for clock in/out
                final String[] clock_options = {"Clock In", "Clock Out"};
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Select Clock Options")
                        .setItems(clock_options, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                redirectTo = clock_options[which];

                                // custom dialog
                                final Dialog clockDialog = new Dialog(context);
                                clockDialog.setContentView(R.layout.clock_in_out_dialog_layout);
                                clockDialog.setTitle("Provide Details");

                                ibButton1 = (ImageButton) clockDialog.findViewById(R.id.ibButton1);
                                ibButton2 = (ImageButton) clockDialog.findViewById(R.id.ibButton2);
                                ibButton3 = (ImageButton) clockDialog.findViewById(R.id.ibButton3);
                                ibButton4 = (ImageButton) clockDialog.findViewById(R.id.ibButton4);
                                ibButton5 = (ImageButton) clockDialog.findViewById(R.id.ibButton5);
                                ibButton6 = (ImageButton) clockDialog.findViewById(R.id.ibButton6);
                                ibButton7 = (ImageButton) clockDialog.findViewById(R.id.ibButton7);
                                ibButton8 = (ImageButton) clockDialog.findViewById(R.id.ibButton8);
                                ibButton9 = (ImageButton) clockDialog.findViewById(R.id.ibButton9);
                                ibButton0 = (ImageButton) clockDialog.findViewById(R.id.ibButton0);

                                ibButton1.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton2.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton3.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton4.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton5.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton6.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton7.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton8.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton9.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton0.setOnTouchListener(new EmployeePINOnTouchListener());

                                ibClear = (ImageButton) clockDialog.findViewById(R.id.ibClear);
                                ibClear.setOnTouchListener(new EmployeePINOnTouchListener());

                                ibLogin = (ImageButton) clockDialog.findViewById(R.id.ibLogin);
                                ibLogin.setOnTouchListener(new EmployeePINOnTouchListener());

                                ivPIN1 = (ImageView) clockDialog.findViewById(R.id.ivPIN1);
                                ivPIN2 = (ImageView) clockDialog.findViewById(R.id.ivPIN2);
                                ivPIN3 = (ImageView) clockDialog.findViewById(R.id.ivPIN3);
                                ivPIN4 = (ImageView) clockDialog.findViewById(R.id.ivPIN4);

                                clockDialog.show();
                            }
                        });
                builder.create();
                builder.show();
            }
        });

        ImageButton ib_tab = (ImageButton) findViewById(R.id.ib_tab);
        ib_tab.setOnClickListener(new TabOptions());
    }

    private class EmployeePINOnTouchListener implements View.OnTouchListener {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            Long tsLong = System.currentTimeMillis()/1000;
            String ts = tsLong.toString();

            if(v.getId() == R.id.ibButton1) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton1.setImageResource(R.drawable.employee_login_1_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "1";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton1.setImageResource(R.drawable.employee_login_button_1);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton2) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton2.setImageResource(R.drawable.employee_login_2_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "2";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton2.setImageResource(R.drawable.employee_login_button_2);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton3) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton3.setImageResource(R.drawable.employee_login_3_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "3";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton3.setImageResource(R.drawable.employee_login_button_3);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton4) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton4.setImageResource(R.drawable.employee_login_4_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "4";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton4.setImageResource(R.drawable.employee_login_button_4);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton5) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton5.setImageResource(R.drawable.employee_login_5_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "5";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton5.setImageResource(R.drawable.employee_login_button_5);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton6) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton6.setImageResource(R.drawable.employee_login_6_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "6";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton6.setImageResource(R.drawable.employee_login_button_6);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton7) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton7.setImageResource(R.drawable.employee_login_7_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "7";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton7.setImageResource(R.drawable.employee_login_button_7);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton8) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton8.setImageResource(R.drawable.employee_login_8_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "8";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton8.setImageResource(R.drawable.employee_login_button_8);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton9) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton9.setImageResource(R.drawable.employee_login_9_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "9";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton9.setImageResource(R.drawable.employee_login_button_9);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton0) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton0.setImageResource(R.drawable.employee_login_0_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "0";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton0.setImageResource(R.drawable.employee_login_button_0);
                        break;
                }
            }
            else if(v.getId() == R.id.ibClear) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibClear.setImageResource(R.drawable.employee_login_clear_clicked);
                        if(PIN.length() > 0) {
                            PIN = PIN.substring(0, PIN.length() - 1);
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibClear.setImageResource(R.drawable.employee_login_clear_button);
                        break;
                }
                Log.d("PIN", PIN);
            }
            else if(v.getId() == R.id.ibLogin) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        boolean return_type;
                        ibLogin.setImageResource(R.drawable.employee_login_login_clicked);
                        if(PIN.length() < 4) {
                            Toast.makeText(getApplicationContext(), "Incomplete PIN", Toast.LENGTH_SHORT).show();
                        }
                        else if(redirectTo.isEmpty()) {
                            Toast.makeText(getApplicationContext(), "Please Select an Option", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
                            Cursor c = db.rawQuery("SELECT * FROM user_tables WHERE employee_pin='" + PIN + "'", null);

                            if (c.moveToFirst()) {
                                if (redirectTo.equals("Clock In")) {
                                    return_type = EmployeeAttendance(PIN, ts, "In");

                                    if (return_type)
                                        Toast.makeText(getApplicationContext(), "Successfully Clocked In", Toast.LENGTH_SHORT).show();
                                    else
                                        Toast.makeText(getApplicationContext(), "Failed to Clock In", Toast.LENGTH_SHORT).show();
                                } else if (redirectTo.equals("Clock Out")) {
                                    return_type = EmployeeAttendance(PIN, ts, "Out");

                                    if (return_type)
                                        Toast.makeText(getApplicationContext(), "Successfully Clocked Out", Toast.LENGTH_SHORT).show();
                                    else
                                        Toast.makeText(getApplicationContext(), "Failed to Clock Out", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), "Invalid PIN", Toast.LENGTH_SHORT).show();
                            }

                            c.close();
                            db.close();
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibLogin.setImageResource(R.drawable.employee_login_login_button);
                        break;
                }
                Log.d("PIN", PIN);
            }

            int length = PIN.length();

            switch (length) {
                case 0:
                    ivPIN1.setImageResource(R.drawable.employee_login_white_dot);
                    ivPIN2.setImageResource(R.drawable.employee_login_white_dot);
                    ivPIN3.setImageResource(R.drawable.employee_login_white_dot);
                    ivPIN4.setImageResource(R.drawable.employee_login_white_dot);
                    break;
                case 1:
                    ivPIN1.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN2.setImageResource(R.drawable.employee_login_white_dot);
                    ivPIN3.setImageResource(R.drawable.employee_login_white_dot);
                    ivPIN4.setImageResource(R.drawable.employee_login_white_dot);
                    break;
                case 2:
                    ivPIN1.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN2.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN3.setImageResource(R.drawable.employee_login_white_dot);
                    ivPIN4.setImageResource(R.drawable.employee_login_white_dot);
                    break;
                case 3:
                    ivPIN1.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN2.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN3.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN4.setImageResource(R.drawable.employee_login_white_dot);
                    break;
                case 4:
                    ivPIN1.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN2.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN3.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN4.setImageResource(R.drawable.employee_login_blue_dot);
                    break;
            }

            return true;
        }
    }

    private boolean EmployeeAttendance(String employee_pin, String clock_time, String clock_mode) {
        SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
        Cursor c = db.rawQuery("SELECT * FROM EmployeeAttendance_v1 WHERE employee_pin='" + employee_pin + "'", null);
        String out_time = "";
        int id = -1;
        boolean first_time = false;
        if (c.moveToFirst()) {
            out_time = c.getString(c.getColumnIndex("out_time"));
            id = c.getInt(c.getColumnIndex("Id"));

            while (c.moveToNext()) {
                out_time = c.getString(c.getColumnIndex("out_time"));
                id = c.getInt(c.getColumnIndex("Id"));
            }
        } else {
            first_time = true;
        }
        c.close();

        if (clock_mode.equals("In")) {
            if (first_time || !out_time.equals("")) {
                db.execSQL("INSERT INTO EmployeeAttendance_v1 (employee_pin, in_time, out_time) VALUES ('" + employee_pin + "', " +
                        "'" + clock_time + "', '');");
                return true;
            }
        } else if (clock_mode.equals("Out")) {
            if (!first_time && out_time.equals("")) {
                db.execSQL("UPDATE EmployeeAttendance_v1 SET out_time='" + clock_time + "' WHERE Id=" + id);
                return true;
            }
        }

        db.close();
        return false;
    }

    private class TabOptions implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            final String[] tab_options = {"Create Tab", "Tab List", "Close Tab"};
            //////////////////Default Dialog to Select Tab Options//////////////////////
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Select Tab Options")
                    .setItems(tab_options, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Log.d("SELECTED TAB OPTIONS", tab_options[which]);

                            if (tab_options[which].equals("Create Tab")) {
                                //TODO open custom dialog
                                final Dialog create_tab_dialog = new Dialog(context);
                                create_tab_dialog.setContentView(R.layout.create_tab_dialog_layout);

                                final EditText et_tab_customer_name = (EditText) create_tab_dialog.findViewById(R.id.et_tab_customer_name);
                                final EditText et_tab_card_number = (EditText) create_tab_dialog.findViewById(R.id.et_tab_card_number);

                                Button button_tab_confirm = (Button) create_tab_dialog.findViewById(R.id.button_tab_confirm);
                                button_tab_confirm.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        String customer_name = et_tab_customer_name.getText().toString();
                                        String card_number = et_tab_card_number.getText().toString();

                                        if (!customer_name.isEmpty()) {
                                            SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
                                            Long tsLong = System.currentTimeMillis()/1000;
                                            String ts = tsLong.toString();

                                            //Insert Customer Details for CustomerPerTable
                                            db.execSQL("INSERT INTO CustomerPerTable_v1 (table_name, customer, time_stamp, floor_name) VALUES (" +
                                                    "'" + customer_name + "', '1', '" + ts + "', 'Tab');");

                                            //Insert Discount Details for BillSupplementary
                                            db.execSQL("INSERT INTO BillSupplementary_v1 (order_number, discount, discount_type) VALUES (" +
                                                    "'" + ts + "', '0', '');");

                                            //Insert Print Details for PrintOptions
                                            db.execSQL("INSERT INTO PrintOptions_v1 (order_number, print_options, table_number, combination, " +
                                                    "payment_status) VALUES (" +
                                                    "'" + ts + "', 'Print Together', '" + customer_name + "', '1', 'unpaid');");

                                            if (!card_number.isEmpty()) {
                                                //Insert Card Details for TabDetails
                                                db.execSQL("INSERT INTO TabDetails_v1 (order_number, customer_name, card_number, status) VALUES (" +
                                                        "'" + ts + "', '" + customer_name + "', '" + card_number + "', 'open');");
                                            } else {
                                                //Insert Card Details for TabDetails
                                                db.execSQL("INSERT INTO TabDetails_v1 (order_number, customer_name, card_number, status) VALUES (" +
                                                        "'" + ts + "', '" + customer_name + "', '', 'open');");
                                            }

                                            db.close();

                                            Toast.makeText(getApplicationContext(), "Tab created for " + customer_name, Toast.LENGTH_SHORT).show();

                                            create_tab_dialog.cancel();
                                        } else {
                                            Toast.makeText(getApplicationContext(), "Please insert a valid name", Toast.LENGTH_SHORT).show();
                                        }

                                    }
                                });

                                create_tab_dialog.show();
                            } else if (tab_options[which].equals("Tab List")) {
                                //TODO open custom dialog
                                final SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
                                Cursor c = db.rawQuery("SELECT * FROM TabDetails_v1 WHERE status='open'", null);

                                if (c.moveToFirst()) {
                                    final String[] open_tabs = new String[c.getCount()];
                                    final String[] open_orders = new String[c.getCount()];
                                    int i = 0;
                                    open_tabs[i] = c.getString(c.getColumnIndex("customer_name"));
                                    open_orders[i] = c.getString(c.getColumnIndex("order_number"));
                                    i++;

                                    while (c.moveToNext()) {
                                        open_tabs[i] = c.getString(c.getColumnIndex("customer_name"));
                                        open_orders[i] = c.getString(c.getColumnIndex("order_number"));
                                        i++;
                                    }

                                    AlertDialog.Builder tab_list_builder = new AlertDialog.Builder(context);
                                    tab_list_builder.setTitle("Open Tabs")
                                            .setItems(open_tabs, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    //TODO open order page
                                                    //////////////////////////Select Details of Table Ordered From///////////////////////////
                                                    SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);

                                                    String table_name, customer = "", time_stamp = "";
                                                    int order_id = 0;
                                                    Cursor c;

                                                    table_name = open_tabs[which];

                                                    c = db.rawQuery("SELECT * FROM CustomerPerTable_v1 WHERE table_name='" + table_name + "' " +
                                                            "AND time_stamp='" + open_orders[which] + "'", null);

                                                    if (c.moveToFirst()) {
                                                        customer = c.getString(c.getColumnIndex("customer"));
                                                        time_stamp = c.getString(c.getColumnIndex("time_stamp"));
                                                        order_id = c.getInt(c.getColumnIndex("Id"));
                                                        while (c.moveToNext()) {
                                                            customer = c.getString(c.getColumnIndex("customer"));
                                                            time_stamp = c.getString(c.getColumnIndex("time_stamp"));
                                                            order_id = c.getInt(c.getColumnIndex("Id"));
                                                        }
                                                    }
                                                    /////////////////////////////////////////////////////////////////////////////////////////
                                                    //Redirect to Order Page
                                                    Intent intent = new Intent(OrderHistoryPage.this, OrderForTablePage.class);
                                                    OrderForTablePage.selected_customer = "1";
                                                    intent.putExtra("table_name", table_name);
                                                    intent.putExtra("customer", customer);
                                                    intent.putExtra("time_stamp", time_stamp);
                                                    intent.putExtra("order_id", String.valueOf(order_id));
                                                    startActivity(intent);
                                                    OrderHistoryPage.this.overridePendingTransition(0, 0);
                                                    OrderHistoryPage.this.finish();

                                                    c.close();
                                                    db.close();
                                                }
                                            });
                                    tab_list_builder.create();
                                    tab_list_builder.show();
                                } else {
                                    Toast.makeText(getApplicationContext(), "No Open Tabs", Toast.LENGTH_SHORT).show();
                                }

                                c.close();
                                db.close();
                            } else if (tab_options[which].equals("Close Tab")) {
                                //TODO open payment page
                            }

                            dialog.cancel();
                        }
                    });
            builder.create();
            builder.show();
            /////////////////////////////////////////////////////////////////////////
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(OrderHistoryPage.this, ViewTables.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        OrderHistoryPage.this.finish();
    }
}
