package com.restaware.mightyegg.restawarepos;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.zip.Inflater;

public class OrderForTablePage extends AppCompatActivity {

    String table_name, customer, time_stamp, order_id;

    public static String selected_customer;

    Context context;

    ////////////Declarations for Expandable Menu List///////////////
    HashMap<String, List<String>>  menu_category;
    HashMap<String, List<String>> menu_price;
    HashMap<String, List<String>> menu_desc;
    List<String> menu_list, price_list, desc_list;
    ExpandableListView exp_list;
    ////////////////////////////////////////////////////////////////

    ///////////Arrays for Existing Orders///////////////
    String[] order_number, customer_number, category, item, unit_price, price, quantity, additional, special_request;
    int orders;
    ////////////////////////////////////////////////////

    //////////Objects for Bill List/////////////
    ListView lv_order_list;
    billListAdapter billAdapter;
    String[] list_order_number, list_customer_number, list_category, list_item, list_unit_price, list_price, list_quantity, list_additional, list_special_request;
    int list_orders;
    ////////////////////////////////////////////

    /////////Variables for Total Bill Calculation///////////
    float subtotal, discount, tax, total, tips, balance;
    ////////////////////////////////////////////////////////

    ////////Variables for Custom Order//////////
    int dialog_quantity;
    String dialog_category, dialog_item, dialog_unit_price, dialog_variation, dialog_addition, dialog_subtraction, dialog_item_to_go, dialog_discount, dialog_discount_type, dialog_special_request;
    String[] type_of_discounts = {"Percentage", "Amount"};
    String[] dialog_modifiers;
    boolean first_time;
    String[] item_modifier_name, item_modifier_price;
    String dialog_modifier_string, dialog_modifier_price;
    boolean[] item_modifier_check;
    ////////////////////////////////////////////

    ///////Variables for Print///////////
    String[] array_of_customers;
    boolean[] array_of_customers_check_list;
    /////////////////////////////////////

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_for_table_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_order_per_table);
        setSupportActionBar(toolbar);

        context = this;

        table_name = getIntent().getStringExtra("table_name");
        customer = getIntent().getStringExtra("customer");
        time_stamp = getIntent().getStringExtra("time_stamp");
        //order_id = getIntent().getStringExtra("order_id");

        Log.d("INHERITED VALUES", table_name + " " + customer + " " + time_stamp);

        //////////////////////////Listener for Toolbar Objects/////////////////////////////////
        TextView tv_table_name = (TextView) toolbar.findViewById(R.id.tv_table_name);
        tv_table_name.setText(table_name);

        TextView tv_customer_number = (TextView) toolbar.findViewById(R.id.tv_customer_number);
        tv_customer_number.setText(selected_customer);

        TextView tv_customer_button = (TextView) findViewById(R.id.tv_customer_button);
        tv_customer_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String[] customerArray = new String[Integer.parseInt(customer)];
                int i;

                for (i = 0; i < Integer.parseInt(customer); i++) {
                    customerArray[i] = String.valueOf(i + 1);
                }

                //////////////////Dialog to Select Customer//////////////////////
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Select Customer")
                        .setItems(customerArray, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String selection = customerArray[which];
                                selected_customer = selection;
                                Intent intent = new Intent(OrderForTablePage.this, OrderForTablePage.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                intent.putExtra("table_name", table_name);
                                intent.putExtra("customer", customer);
                                intent.putExtra("time_stamp", time_stamp);
                                intent.putExtra("order_id", order_id);
                                startActivity(intent);
                                OrderForTablePage.this.overridePendingTransition(0, 0);
                                OrderForTablePage.this.finish();
                            }
                        });
                builder.create();
                builder.show();
                //////////////////////////////////////////////////////////////
            }
        });
        //////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////populate HashMap///////////////////////////////////
        HashMap<String, List<String>> MenuDetails = new HashMap<String, List<String>>();
        HashMap<String, List<String>> MenuPrices = new HashMap<String, List<String>>();
        HashMap<String, List<String>> MenuDescriptions = new HashMap<String, List<String>>();

        SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);

        Cursor cursor;
        String[] menu_categories;
        int[] menu_id;
        int i;

        cursor = db.rawQuery("SELECT * FROM MenuCategories_v1", null);
        menu_categories = new String[cursor.getCount()];
        menu_id = new int[cursor.getCount()];
        i = 0;

        if (cursor.moveToFirst()) {
            menu_categories[i] = cursor.getString(cursor.getColumnIndex("category_name"));
            menu_id[i] = cursor.getInt(cursor.getColumnIndex("Id"));
            i++;

            while (cursor.moveToNext()) {
                menu_categories[i] = cursor.getString(cursor.getColumnIndex("category_name"));
                menu_id[i] = cursor.getInt(cursor.getColumnIndex("Id"));
                i++;
            }
        }

        cursor.close();

        int total_categories = cursor.getCount();
        List<String> menu_items, menu_prices, menu_descs;

        for (i = 0; i < total_categories; i++) {
            cursor = db.rawQuery("SELECT * FROM MenuItems_v1 WHERE item_category='" + menu_categories[i] + "'", null);
            menu_items = new ArrayList<String>();
            menu_prices = new ArrayList<String>();
            menu_descs = new ArrayList<String>();

            if (cursor.moveToFirst()) {
                menu_items.add(cursor.getString(cursor.getColumnIndex("item_name")));
                menu_prices.add(cursor.getString(cursor.getColumnIndex("item_price")));
                menu_descs.add(cursor.getString(cursor.getColumnIndex("item_desc")));

                while (cursor.moveToNext()) {
                    menu_items.add(cursor.getString(cursor.getColumnIndex("item_name")));
                    menu_prices.add(cursor.getString(cursor.getColumnIndex("item_price")));
                    menu_descs.add(cursor.getString(cursor.getColumnIndex("item_desc")));
                }
            }

            MenuDetails.put(menu_categories[i], menu_items);
            MenuPrices.put(menu_categories[i], menu_prices);
            MenuDescriptions.put(menu_categories[i], menu_descs);
        }

        db.close();
        //////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////Fetch Existing Orders//////////////////////////////////////
        orders = 0;
        db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);

        Cursor c = db.rawQuery("SELECT * FROM OrderPerCustomer_v1 WHERE order_number='" + time_stamp + "' AND customer_number='" + selected_customer + "'", null);

        order_number = new String[c.getCount()];
        customer_number = new String[c.getCount()];
        category = new String[c.getCount()];
        item = new String[c.getCount()];
        unit_price = new String[c.getCount()];
        price = new String[c.getCount()];
        quantity = new String[c.getCount()];
        additional = new String[c.getCount()];
        special_request = new String[c.getCount()];

        i = 0;

        if (c.moveToFirst()) {
            order_number[i] = c.getString(c.getColumnIndex("order_number"));
            customer_number[i] = c.getString(c.getColumnIndex("customer_number"));
            category[i] = c.getString(c.getColumnIndex("category"));
            item[i] = c.getString(c.getColumnIndex("item"));
            unit_price[i] = c.getString(c.getColumnIndex("unit_price"));
            price[i] = c.getString(c.getColumnIndex("price"));
            quantity[i] = c.getString(c.getColumnIndex("quantity"));
            additional[i] = c.getString(c.getColumnIndex("additional"));
            special_request[i] = c.getString(c.getColumnIndex("special_request"));
            i++;

            while (c.moveToNext()) {
                order_number[i] = c.getString(c.getColumnIndex("order_number"));
                customer_number[i] = c.getString(c.getColumnIndex("customer_number"));
                category[i] = c.getString(c.getColumnIndex("category"));
                item[i] = c.getString(c.getColumnIndex("item"));
                unit_price[i] = c.getString(c.getColumnIndex("unit_price"));
                price[i] = c.getString(c.getColumnIndex("price"));
                quantity[i] = c.getString(c.getColumnIndex("quantity"));
                additional[i] = c.getString(c.getColumnIndex("additional"));
                special_request[i] = c.getString(c.getColumnIndex("special_request"));
                i++;
            }
        }

        orders = i;

        c.close();
        db.close();
        ///////////////////////////////////////////////////////////////////////////////////////////

        ////////////////////Populate List for Bill////////////////////////
        lv_order_list = (ListView) findViewById(R.id.lv_order_list);
        fetchExistingOrders();
//        billAdapter = new billListAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, R.id.tv_bill_list_name, list_order_number);
//        lv_order_list.setAdapter(billAdapter);
        //////////////////////////////////////////////////////////////////

        ////////////////////Expandable List Populate////////////////////////
        exp_list = (ExpandableListView) findViewById(R.id.lv_menu_category_list);
        menu_category = MenuDetails;
        menu_list = new ArrayList<String>(menu_category.keySet());
        menu_price = MenuPrices;
        price_list = new ArrayList<String>(menu_price.keySet());
        menu_desc = MenuDescriptions;
        desc_list = new ArrayList<String>(menu_desc.keySet());
        ExpandableListAdapter adapter = new MenuAdapter(context, menu_category, menu_list, menu_price, price_list);
        exp_list.setAdapter(adapter);
        ////////////////////////////////////////////////////////////////////

        ////////////////////////Listener for Click on Expandable List/////////////////////////////
        exp_list.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                Log.d("CLICKED ITEM", menu_category.get(menu_list.get(groupPosition)).get(childPosition) + " of Category " + menu_list.get(groupPosition));

                SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);

                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.menu_customization_dialog_layout);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

                dialog_category = String.valueOf(menu_list.get(groupPosition));
                dialog_item = menu_category.get(menu_list.get(groupPosition)).get(childPosition);
                dialog_unit_price = String.valueOf(menu_price.get(price_list.get(groupPosition)).get(childPosition));

                dialog_variation = "";
                dialog_addition = "";
                dialog_subtraction = "";
                dialog_item_to_go = "false";
                dialog_discount = "";
                dialog_discount_type = "";
                dialog_special_request = "";
                dialog_modifier_string = "";
                dialog_modifier_price = "0";

                ImageView iv_opt_dialog_item_image = (ImageView) dialog.findViewById(R.id.iv_opt_dialog_item_image);
                TextView tv_opt_dialog_item_name = (TextView) dialog.findViewById(R.id.tv_opt_dialog_item_name);
                TextView tv_opt_dialog_item_price = (TextView) dialog.findViewById(R.id.tv_opt_dialog_item_price);
                TextView tv_opt_dialog_item_desc = (TextView) dialog.findViewById(R.id.tv_opt_dialog_item_desc);
                ImageButton ib_opt_dialog_quantity_minus = (ImageButton) dialog.findViewById(R.id.ib_opt_dialog_quantity_minus);
                final EditText et_opt_dialog_quantity = (EditText) dialog.findViewById(R.id.et_opt_dialog_quantity);
                ImageButton ib_opt_dialog_quantity_plus = (ImageButton) dialog.findViewById(R.id.ib_opt_dialog_quantity_plus);
                Button button_opt_dialog_modifiers = (Button) dialog.findViewById(R.id.button_opt_dialog_modifiers);
                Button button_opt_dialog_additions = (Button) dialog.findViewById(R.id.button_opt_dialog_additions);
                Button button_opt_dialog_subtraction = (Button) dialog.findViewById(R.id.button_opt_dialog_subtractions);
                Button button_opt_dialog_variations = (Button) dialog.findViewById(R.id.button_opt_dialog_variations);
                Button button_opt_dialog_discount = (Button) dialog.findViewById(R.id.button_opt_dialog_discount);
                Button button_opt_dialog_item_to_go = (Button) dialog.findViewById(R.id.button_opt_dialog_item_to_go);
                ImageButton ib_opt_dialog_confirm = (ImageButton) dialog.findViewById(R.id.ib_opt_dialog_confirm);
                final EditText et_opt_dialog_special_request = (EditText) dialog.findViewById(R.id.et_opt_dialog_special_request);

                tv_opt_dialog_item_name.setText(String.valueOf(menu_category.get(menu_list.get(groupPosition)).get(childPosition)));
                tv_opt_dialog_item_price.setText("Price $ " + String.valueOf(menu_price.get(price_list.get(groupPosition)).get(childPosition)));
                tv_opt_dialog_item_desc.setText(String.valueOf(menu_desc.get(desc_list.get(groupPosition)).get(childPosition)));

                Cursor c = db.rawQuery("SELECT * FROM OrderPerCustomer_v1 WHERE order_number='" + time_stamp + "' AND customer_number='" + selected_customer + "'" +
                        " AND item='" + dialog_item + "'", null);

                if (c.moveToFirst()) {
                    et_opt_dialog_quantity.setText(c.getString(c.getColumnIndex("quantity")));
                    dialog_variation = c.getString(c.getColumnIndex("variations"));
                    dialog_addition = c.getString(c.getColumnIndex("additional"));
                    dialog_subtraction = c.getString(c.getColumnIndex("subtraction"));
                    dialog_item_to_go = c.getString(c.getColumnIndex("to_go"));
                    dialog_discount = c.getString(c.getColumnIndex("item_discount"));
                    dialog_discount_type = c.getString(c.getColumnIndex("item_discount_type"));
                    dialog_special_request = c.getString(c.getColumnIndex("special_request"));
                    dialog_modifier_string = c.getString(c.getColumnIndex("modifier"));
                    dialog_modifier_price = c.getString(c.getColumnIndex("modifier_price"));
                } else {
                    et_opt_dialog_quantity.setText("1");
                }

//                Log.d("DEBUG", "variation: " + dialog_variation + " addition : " + dialog_addition + " subtraction: " + dialog_subtraction + " " +
//                        "item to go: " + dialog_item_to_go + " discount: " + dialog_discount + " discount type: " +
//                        "" + dialog_discount_type + " special request: " + dialog_special_request + " modifier string: " + dialog_modifier_string + " " +
//                        "modifier price: " + dialog_modifier_price);

//                if (dialog_modifier_string == null) {
//                    dialog_modifier_string = "";
//                }
//
//                if (dialog_modifier_price == null) {
//                    dialog_modifier_price = "0";
//                }

                if (!dialog_modifier_string.equals("")) {
                    dialog_modifiers = dialog_modifier_string.split(",");
                }

                et_opt_dialog_special_request.setText(dialog_special_request);

                ib_opt_dialog_quantity_minus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog_quantity = Integer.parseInt(et_opt_dialog_quantity.getText().toString());

                        if (dialog_quantity > 1) {
                            dialog_quantity--;
                            et_opt_dialog_quantity.setText(String.valueOf(dialog_quantity));
                        }
                    }
                });

                ib_opt_dialog_quantity_plus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog_quantity = Integer.parseInt(et_opt_dialog_quantity.getText().toString());

                        if (dialog_quantity < 998) {
                            dialog_quantity++;
                            et_opt_dialog_quantity.setText(String.valueOf(dialog_quantity));
                        }
                    }
                });

                button_opt_dialog_modifiers.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // custom dialog
                        final Dialog dialog = new Dialog(context);
                        dialog.setContentView(R.layout.modifier_dialog_layout);

                        ListView list_modifiers = (ListView) dialog.findViewById(R.id.list_modifiers);

                        list_modifiers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                if (view != null) {
                                    CheckBox cb = (CheckBox) view.findViewById(R.id.cb_modifier);
                                    if (item_modifier_check[position]) {
                                        item_modifier_check[position] = false;
                                        cb.setChecked(false);
                                    } else {
                                        item_modifier_check[position] = true;
                                        cb.setChecked(true);
                                    }
                                    Log.d("CHECK", "Item Clicked : " + position);
                                }
                            }
                        });

                        Button confirmButton = (Button) dialog.findViewById(R.id.button_modifiers_confirm);

                        SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
                        Cursor c = db.rawQuery("SELECT * FROM MenuItems_v1 WHERE item_name='" + dialog_item + "' AND " +
                                "item_category='" + dialog_category + "'", null);

                        boolean modifiers_present = false;

                        if (c.moveToFirst()) {
                            int id, i = 0;
                            id = c.getInt(c.getColumnIndex("Id"));
                            c = db.rawQuery("SELECT * FROM ModifiersForMenuItems_v1 WHERE item_id='" + id + "'", null);

                            if (c.getCount() > 0) {
                                item_modifier_name = new String[c.getCount()];
                                item_modifier_price = new String[c.getCount()];
                                item_modifier_check = new boolean[c.getCount()];

                                for (i = 0; i < c.getCount(); i++) {
                                    item_modifier_check[i] = false;
                                }
                            }

                            i = 0;

                            if (c.moveToFirst()) {
                                modifiers_present = true;
                                item_modifier_name[i] = c.getString(c.getColumnIndex("modification"));
                                item_modifier_price[i] = c.getString(c.getColumnIndex("extra_price"));
                                i++;
                                while (c.moveToNext()) {
                                    item_modifier_name[i] = c.getString(c.getColumnIndex("modification"));
                                    item_modifier_price[i] = c.getString(c.getColumnIndex("extra_price"));
                                    i++;
                                }
                                modifierListAdapter modifierAdapter = new modifierListAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, R.id.tv_modifier_name, item_modifier_name);
                                list_modifiers.setAdapter(modifierAdapter);
                            } else {
                                Toast.makeText(getApplication(), "No modifiers for this item", Toast.LENGTH_SHORT).show();
                            }
                        }

                        confirmButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog_modifier_string = "";
                                dialog_modifier_price = "0";
                                boolean first = true;
                                for (int i = 0; i < item_modifier_check.length; i++) {
                                    if (item_modifier_check[i]) {
                                        if (first) {
                                            first = false;
                                        } else {
                                            dialog_modifier_string += ",";
                                        }
                                        dialog_modifier_string = dialog_modifier_string + item_modifier_name[i];
                                        dialog_modifier_price = String.valueOf(Double.parseDouble(dialog_modifier_price) + Double.parseDouble(item_modifier_price[i]));
                                    }
                                }
                                dialog.cancel();
                            }
                        });

                        if (modifiers_present) {
                            dialog.show();
                        }

                        c.close();
                        db.close();
                    }
                });

                button_opt_dialog_additions.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // custom dialog
                        final Dialog dialog = new Dialog(context);
                        dialog.setContentView(R.layout.addition_subtraction_dialog_layout);

                        final TextView tvDialogTitle = (TextView) dialog.findViewById(R.id.tv_asd_title);
                        final EditText etDialogBody = (EditText) dialog.findViewById(R.id.et_asd_body);

                        tvDialogTitle.setText("Provide Addition");
                        etDialogBody.setText(dialog_addition);

                        Button confirmButton = (Button) dialog.findViewById(R.id.button_asd_confirm);

                        confirmButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog_addition = etDialogBody.getText().toString();
                                dialog.cancel();
                            }
                        });

                        dialog.show();
                    }
                });

                button_opt_dialog_subtraction.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // custom dialog
                        final Dialog dialog = new Dialog(context);
                        dialog.setContentView(R.layout.addition_subtraction_dialog_layout);

                        final TextView tvDialogTitle = (TextView) dialog.findViewById(R.id.tv_asd_title);
                        final EditText etDialogBody = (EditText) dialog.findViewById(R.id.et_asd_body);

                        tvDialogTitle.setText("Provide Subtraction");
                        etDialogBody.setText(dialog_subtraction);

                        Button confirmButton = (Button) dialog.findViewById(R.id.button_asd_confirm);

                        confirmButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog_subtraction = etDialogBody.getText().toString();
                                dialog.cancel();
                            }
                        });

                        dialog.show();
                    }
                });

                button_opt_dialog_variations.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
                        Cursor c = db.rawQuery("SELECT * FROM MenuItems_v1 WHERE item_name='" + dialog_item + "' AND item_category='" + dialog_category + "'", null);
                        int id = 0;
                        final String[] variations;

                        if (c.moveToFirst()) {
                            id = c.getInt(c.getColumnIndex("Id"));

                            c = db.rawQuery("SELECT * FROM VariationsForMenuItems_v1 WHERE item_id='" + id + "'", null);
                            int i = 0;

                            if (c.moveToFirst()) {
                                variations = new String[c.getCount()];
                                variations[i] = c.getString(c.getColumnIndex("variation"));
                                i++;
                                while (c.moveToNext()) {
                                    variations[i] = c.getString(c.getColumnIndex("variation"));
                                    i++;
                                }

                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setTitle("Select type of Variation")
                                        .setItems(variations, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog_variation = variations[which];
                                                Log.d("VARIATION", "Selected variation: " + dialog_variation);
                                                dialog.cancel();
                                            }
                                        });
                                builder.create();
                                builder.show();
                            } else {
                                Toast.makeText(getBaseContext(), "No variations for this item", Toast.LENGTH_SHORT).show();
                            }
                        }

                        c.close();
                        db.close();
                    }
                });

                button_opt_dialog_discount.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // custom dialog
                        final Dialog dialog = new Dialog(context);
                        dialog.setContentView(R.layout.discount_dialog_layout);

                        final TextView tvDialogTitle = (TextView) dialog.findViewById(R.id.tv_discount_title);
                        final TextView tv_discount_type = (TextView) dialog.findViewById(R.id.tv_discount_type);
                        final EditText et_discount_amount = (EditText) dialog.findViewById(R.id.et_discount_amount);

                        tvDialogTitle.setText("Discount");
                        tv_discount_type.setText(dialog_discount_type);
                        et_discount_amount.setText(dialog_discount);

                        tv_discount_type.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //default dialog
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setTitle("Select Discount Type")
                                        .setItems(type_of_discounts, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                tv_discount_type.setText(type_of_discounts[which]);
                                                dialog.cancel();
                                            }
                                        });
                                builder.create();
                                builder.show();
                            }
                        });

                        Button confirmButton = (Button) dialog.findViewById(R.id.button_discount_confirm);

                        confirmButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog_discount = et_discount_amount.getText().toString();
                                dialog_discount_type = tv_discount_type.getText().toString();
                                dialog.cancel();
                            }
                        });

                        dialog.show();
                    }
                });

                button_opt_dialog_item_to_go.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //TODO dialog for item to go
                        AlertDialog.Builder aDialog = new AlertDialog.Builder(OrderForTablePage.this);
                        aDialog.setTitle("Confirm Status");
                        aDialog.setMessage("Choose if you want the item to go.");
                        aDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog_item_to_go = "true";
                                dialog.cancel();
                            }
                        });
                        aDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog_item_to_go = "false";
                                dialog.cancel();
                            }
                        });

                        aDialog.show();
                    }
                });

                ib_opt_dialog_confirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DecimalFormat df = new DecimalFormat("0.00");
                        SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);

                        dialog_quantity = Integer.parseInt(et_opt_dialog_quantity.getText().toString());
                        dialog_special_request = et_opt_dialog_special_request.getText().toString();

                        Log.d("VALUE CHECK", dialog_unit_price + " " + dialog_modifier_price + " " + dialog_quantity);
                        double price = (Double.parseDouble(dialog_unit_price) + Double.parseDouble(dialog_modifier_price)) * (double) dialog_quantity;

                        if (dialog_discount_type.equals("Percentage")) {
                            price = price - (price * Double.parseDouble(dialog_discount)) / 100;
                        } else if (dialog_discount_type.equals("Amount")) {
                            price = price - Double.parseDouble(dialog_discount);
                        }

                        Cursor c = db.rawQuery("SELECT * FROM OrderPerCustomer_v1 WHERE order_number='" + time_stamp + "' AND customer_number='" + selected_customer + "'" +
                                " AND item='" + dialog_item + "'", null);

                        if (c.moveToFirst()) {
                            int id = c.getInt(c.getColumnIndex("Id"));
                            db.execSQL("UPDATE OrderPerCustomer_v1 SET order_number='" + time_stamp + "', customer_number='" + selected_customer + "', " +
                                    "category='" + dialog_category + "', item='" + dialog_item + "', unit_price='" + dialog_unit_price + "', " +
                                    "price='" + df.format(price) + "', quantity='" + dialog_quantity + "', variations='" + dialog_variation + "', " +
                                    "additional='" + dialog_addition + "', subtraction='" + dialog_subtraction + "', to_go='" + dialog_item_to_go + "'" +
                                    ", item_discount='" + dialog_discount + "', item_discount_type='" + dialog_discount_type + "', " +
                                    "special_request='" + dialog_special_request + "', modifier='" + dialog_modifier_string + "', " +
                                    "modifier_price='" + dialog_modifier_price + "'WHERE Id=" + id);

                            //Change payment status to unpaid
                            c = db.rawQuery("SELECT * FROM PrintOptions_v1 WHERE order_number='" + time_stamp + "' AND table_number='" + table_name + "'", null);

                            if (c.moveToFirst()) {
                                String combination = c.getString(c.getColumnIndex("combination"));
                                String[] combination_array = combination.split(",");
                                int i;
                                for (i = 0; i < combination_array.length; i++) {
                                    if (combination_array[i].equals(selected_customer)) {
                                        db.execSQL("UPDATE PrintOptions_v1 SET payment_status='unpaid' WHERE Id=" + c.getInt(c.getColumnIndex("Id")));
                                        break;
                                    }
                                }
                                while (c.moveToNext()) {
                                    combination = c.getString(c.getColumnIndex("combination"));
                                    combination_array = combination.split(",");

                                    for (i = 0; i < combination_array.length; i++) {
                                        if (combination_array[i].equals(selected_customer)) {
                                            db.execSQL("UPDATE PrintOptions_v1 SET payment_status='unpaid' WHERE Id=" + c.getInt(c.getColumnIndex("Id")));
                                            break;
                                        }
                                    }
                                }
                            }
                            c.close();
                        } else {
                            db.execSQL("INSERT INTO OrderPerCustomer_v1 (order_number, customer_number, category, item, unit_price, price, quantity, " +
                                    "variations, additional, subtraction, to_go, item_discount, item_discount_type, special_request, modifier, modifier_price" +
                                    ", table_number) " +
                                    "VALUES ('" + time_stamp + "', '" + selected_customer + "', '" + dialog_category + "', '" + dialog_item + "', '" + dialog_unit_price + "', " +
                                    "'" + df.format(price) + "', '" + dialog_quantity + "', '" + dialog_variation + "', '" + dialog_addition + "', " +
                                    "'" + dialog_subtraction + "', '" + dialog_item_to_go + "', '" + dialog_discount + "', '" + dialog_discount_type + "'," +
                                    " '" + dialog_special_request + "', '" + dialog_modifier_string + "', '" + dialog_modifier_price + "', '" +
                                    "" + table_name + "');");

                            //Change payment status to unpaid
                            c = db.rawQuery("SELECT * FROM PrintOptions_v1 WHERE order_number='" + time_stamp + "' AND table_number='" + table_name + "'", null);

                            if (c.moveToFirst()) {
                                String combination = c.getString(c.getColumnIndex("combination"));
                                String[] combination_array = combination.split(",");
                                int i;
                                for (i = 0; i < combination_array.length; i++) {
                                    if (combination_array[i].equals(selected_customer)) {
                                        db.execSQL("UPDATE PrintOptions_v1 SET payment_status='unpaid' WHERE Id=" + c.getInt(c.getColumnIndex("Id")));
                                        break;
                                    }
                                }
                                while (c.moveToNext()) {
                                    combination = c.getString(c.getColumnIndex("combination"));
                                    combination_array = combination.split(",");

                                    for (i = 0; i < combination_array.length; i++) {
                                        if (combination_array[i].equals(selected_customer)) {
                                            db.execSQL("UPDATE PrintOptions_v1 SET payment_status='unpaid' WHERE Id=" + c.getInt(c.getColumnIndex("Id")));
                                            break;
                                        }
                                    }
                                }
                            }
                            c.close();
                        }

                        c.close();
                        db.close();

                        dialog.cancel();

                        Intent intent = new Intent(OrderForTablePage.this, OrderForTablePage.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        intent.putExtra("table_name", table_name);
                        intent.putExtra("customer", customer);
                        intent.putExtra("time_stamp", time_stamp);
                        intent.putExtra("order_id", order_id);
                        startActivity(intent);
                        OrderForTablePage.this.overridePendingTransition(0, 0);
                        OrderForTablePage.this.finish();
                    }
                });

                dialog.show();

                c.close();
                db.close();

                return false;
            }
        });
        //////////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////Button Functions//////////////////////////////
        ImageButton ib_order_list_discount = (ImageButton) findViewById(R.id.ib_order_list_discount);
        ib_order_list_discount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // custom dialog
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.discount_dialog_layout);

                first_time = true;
                String overall_discount = "", overall_discount_type = "";

                final TextView tvDialogTitle = (TextView) dialog.findViewById(R.id.tv_discount_title);
                final TextView tv_discount_type = (TextView) dialog.findViewById(R.id.tv_discount_type);
                final EditText et_discount_amount = (EditText) dialog.findViewById(R.id.et_discount_amount);

                SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
                Cursor c = db.rawQuery("SELECT * FROM BillSupplementary_v1 WHERE order_number='" + time_stamp + "'", null);

                if (c.moveToFirst()) {
                    first_time = false;
                    overall_discount = c.getString(c.getColumnIndex("discount"));
                    overall_discount_type = c.getString(c.getColumnIndex("discount_type"));
                }

                tvDialogTitle.setText("Discount");
                tv_discount_type.setText(overall_discount_type);
                et_discount_amount.setText(overall_discount);

                tv_discount_type.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //default dialog
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Select Discount Type")
                                .setItems(type_of_discounts, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        tv_discount_type.setText(type_of_discounts[which]);
                                        dialog.cancel();
                                    }
                                });
                        builder.create();
                        builder.show();
                    }
                });

                Button confirmButton = (Button) dialog.findViewById(R.id.button_discount_confirm);

                confirmButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String overall_discount_modified, overall_discount_type_modified;
                        overall_discount_modified = et_discount_amount.getText().toString();
                        overall_discount_type_modified = tv_discount_type.getText().toString();

                        SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);

                        if (!first_time) {
                            db.execSQL("UPDATE BillSupplementary_v1 SET discount='" + overall_discount_modified + "', discount_type='" + overall_discount_type_modified +
                                    "' WHERE order_number='" + time_stamp + "'");
                        } else {
                            db.execSQL("INSERT INTO BillSupplementary_v1 (order_number, discount, discount_type) VALUES " +
                                    "('" + time_stamp + "', '" + overall_discount_modified + "', '" + overall_discount_type_modified + "');");
                        }

                        db.close();

                        refreshBillSegment();

                        dialog.cancel();
                    }
                });

                c.close();
                dialog.show();
            }
        });

        ImageButton ib_order_list_pay = (ImageButton) findViewById(R.id.ib_order_list_pay);
        ib_order_list_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshBillSegment();

                SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
                Cursor c = db.rawQuery("SELECT * FROM PrintOptions_v1 WHERE order_number='" + time_stamp + "' AND table_number='" + table_name + "'", null);
                boolean[] customer_check = new boolean[Integer.parseInt(customer)];
                int i, j;
                String combination = "";
                String[] parsed_combination;
                boolean print_combination = false;

                for (i = 0; i < Integer.parseInt(customer); i++) {
                    customer_check[i] = false;
                }

                if (c.moveToFirst()) {
                    if (c.getString(c.getColumnIndex("print_options")).equals("Combination")) {
                        print_combination = true;
                        combination = c.getString(c.getColumnIndex("combination"));
                        Log.d("FETCHED COMBINATION", combination);
                        parsed_combination = combination.split(",");

                        for (i = 0; i < parsed_combination.length; i++) {
                            Log.d("CUSTOMER IN COMBINATION", String.valueOf(parsed_combination[i]));
                            for (j = 0; j < Integer.parseInt(customer); j++) {
                                if (Integer.parseInt(parsed_combination[i]) == (j + 1)) {
                                    customer_check[j] = true;
                                    Log.d("NUMBER CHECKED", String.valueOf(j + 1));
                                    break;
                                }
                            }
                        }
                    }
                    while (c.moveToNext()) {
                        if (c.getString(c.getColumnIndex("print_options")).equals("Combination")) {
                            combination = c.getString(c.getColumnIndex("combination"));
                            parsed_combination = combination.split(",");

                            for (i = 0; i < parsed_combination.length; i++) {
                                Log.d("CUSTOMER IN COMBINATION", String.valueOf(parsed_combination[i]));
                                for (j = 0; j < Integer.parseInt(customer); j++) {
                                    if (Integer.parseInt(parsed_combination[i]) == (j + 1)) {
                                        customer_check[j] = true;
                                        Log.d("NUMBER CHECKED", String.valueOf(j + 1));
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    if (print_combination) {
                        for (i = 0; i < Integer.parseInt(customer); i++) {
                            if (!customer_check[i]) {
                                db.execSQL("INSERT INTO PrintOptions_v1 (order_number, print_options, combination, payment_status, table_number) " +
                                        "VALUES ('" + time_stamp + "', " +
                                        "'Combination', '" + String.valueOf(i + 1) + "', 'unpaid', '" + table_name + "');");
                            }
                        }
                    }

                    //open page with number pad
                    //TODO shift the following block to page with number pad
                    Intent intent = new Intent(OrderForTablePage.this, PaymentForTablePage.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    intent.putExtra("table_name", table_name);
                    intent.putExtra("time_stamp", time_stamp);
                    intent.putExtra("customer", customer);
                    intent.putExtra("combination", "0");
                    intent.putExtra("redirect_from", "Order");

                    OrderForTablePage.this.finish();
                    OrderForTablePage.this.overridePendingTransition(0, 0);

                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "Please Select Print Options First", Toast.LENGTH_SHORT).show();
                }

                c.close();
                db.close();

//                SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
//                db.execSQL("INSERT INTO OrderLog_v1 (order_number, table_number, customers, subtotal, discount, tax, total) VALUES ('" + time_stamp +
//                        "', '" + table_name + "', '" + customer + "', '" + subtotal + "', '" + discount + "', '" + tax + "', '" + total + "');");
//                db.execSQL("UPDATE TableArrangement_v1 SET status='vacant' WHERE order_number='" + time_stamp + "'");
//                db.close();
            }
        });

        ImageButton ib_order_list_hold = (ImageButton) findViewById(R.id.ib_order_list_hold);
        ib_order_list_hold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderForTablePage.this, ViewTables.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                OrderForTablePage.this.finish();

            }
        });

        ImageButton ib_order_list_cancel = (ImageButton) findViewById(R.id.ib_order_list_cancel);
        ib_order_list_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder aDialog = new AlertDialog.Builder(OrderForTablePage.this);
                aDialog.setTitle("Confirm Cancel");
                aDialog.setMessage("Are you sure you want to delete entire order list?");
                aDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Delete all entries from current order
                        SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);

                        db.execSQL("DELETE FROM OrderPerCustomer_v1 WHERE order_number='" + time_stamp + "'");

                        db.close();

                        Intent intent = new Intent(OrderForTablePage.this, ViewTables.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent);
                        OrderForTablePage.this.finish();
                    }
                });
                aDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                aDialog.show();
            }
        });

        ImageButton ib_order_list_print = (ImageButton) findViewById(R.id.ib_order_list_print);
        ib_order_list_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String[] printOptions = {"Print Separate", "Print Together"};
                final String[] printSeparateOptions = {"All Separate", "Combination"};
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Select Print Option")
                        .setItems(printOptions, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
                                Cursor c;
                                if (printOptions[which].equals("Print Together")) {
                                    c = db.rawQuery("SELECT * FROM PrintOptions_v1 WHERE order_number='" + time_stamp + "' AND table_number='" + table_name + "'", null);
                                    int id;
                                    if (c.moveToFirst()) {
                                        id = c.getInt(c.getColumnIndex("Id"));
                                        db.execSQL("DELETE FROM PrintOptions_v1 WHERE Id=" + id);

                                        while (c.moveToNext()) {
                                            id = c.getInt(c.getColumnIndex("Id"));
                                            db.execSQL("DELETE FROM PrintOptions_v1 WHERE Id=" + id);
                                        }
                                    }

                                    String combination = "";
                                    boolean first = true;
                                    int i;

                                    for (i = 0; i < Integer.parseInt(customer); i++) {
                                        if (first) {
                                            first = false;
                                        } else {
                                            combination += ",";
                                        }
                                        combination += String.valueOf(i + 1);
                                    }

                                    db.execSQL("INSERT INTO PrintOptions_v1 (order_number, print_options, payment_status, combination, table_number) " +
                                            "VALUES ('" + time_stamp + "', 'Print Together', 'unpaid', '" + combination + "', '" + table_name + "');");
                                    c.close();
                                } else if (printOptions[which].equals("Print Separate")) {
                                    AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                                    builder1.setTitle("Select Print Separate Options")
                                            .setItems(printSeparateOptions, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(final DialogInterface dialog, int which) {
                                                    SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
                                                    Cursor c;

                                                    if (printSeparateOptions[which].equals("All Separate")) {
                                                        c = db.rawQuery("SELECT * FROM PrintOptions_v1 WHERE order_number='" + time_stamp + "' AND table_number='" + table_name + "'", null);
                                                        int id;
                                                        if (c.moveToFirst()) {
                                                            id = c.getInt(c.getColumnIndex("Id"));
                                                            db.execSQL("DELETE FROM PrintOptions_v1 WHERE Id=" + id);

                                                            while (c.moveToNext()) {
                                                                id = c.getInt(c.getColumnIndex("Id"));
                                                                db.execSQL("DELETE FROM PrintOptions_v1 WHERE Id=" + id);
                                                            }
                                                        }

                                                        for (int i = 0; i < Integer.parseInt(customer); i++) {
                                                            db.execSQL("INSERT INTO PrintOptions_v1 (order_number, print_options, combination, " +
                                                                    "payment_status, table_number)" +
                                                                    " VALUES ('" + time_stamp + "', 'All Separate', '" + String.valueOf(i+1) + "', " +
                                                                    "'unpaid', '" + table_name + "');");
                                                        }

                                                        c.close();
                                                    } else if (printSeparateOptions[which].equals("Combination")) {
                                                        //TODO add dialog to select combination
                                                        final Dialog d = new Dialog(context);
                                                        d.setContentView(R.layout.separate_bill_dialog_layout);
                                                        ListView list_separate_bills = (ListView) d.findViewById(R.id.list_separate_bills);

                                                        Button confirmButton = (Button) d.findViewById(R.id.button_separate_bills_confirm);

                                                        array_of_customers = new String[Integer.parseInt(customer)];
                                                        array_of_customers_check_list = new boolean[Integer.parseInt(customer)];

                                                        int i;

                                                        for (i = 0; i < Integer.parseInt(customer); i++) {
                                                            array_of_customers[i] = String.valueOf(i + 1);
                                                            array_of_customers_check_list[i] = false;
                                                        }

                                                        separateBillsListAdapter separateBillsListAdapter = new separateBillsListAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, R.id.tv_separate_bills_customer_number, array_of_customers);
                                                        list_separate_bills.setAdapter(separateBillsListAdapter);

                                                        list_separate_bills.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                            @Override
                                                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                                if (view != null) {
                                                                    CheckBox cb = (CheckBox) view.findViewById(R.id.cb_separate_bills);
                                                                    if (array_of_customers_check_list[position]) {
                                                                        cb.setChecked(false);
                                                                        array_of_customers_check_list[position] = false;
                                                                    } else {
                                                                        cb.setChecked(true);
                                                                        array_of_customers_check_list[position] = true;
                                                                    }
                                                                }
                                                            }
                                                        });

                                                        confirmButton.setOnClickListener(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View v) {
                                                                String customer_sequence = "";
                                                                boolean first = true;
                                                                for (int i = 0; i < Integer.parseInt(customer); i++) {
                                                                    if (array_of_customers_check_list[i]) {
                                                                        if (first) {
                                                                            first = false;
                                                                        } else {
                                                                            customer_sequence += ",";
                                                                        }
                                                                        customer_sequence += String.valueOf(i + 1);
                                                                    }
                                                                }

                                                                if (!customer_sequence.equals("")) {
                                                                    SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
                                                                    db.execSQL("DELETE FROM PrintOptions_v1 WHERE order_number='" + time_stamp + "' " +
                                                                            "AND print_options='Print Together'");
                                                                    db.execSQL("DELETE FROM PrintOptions_v1 WHERE order_number='" + time_stamp + "' " +
                                                                            "AND print_options='All Separate'");
                                                                    db.execSQL("INSERT INTO PrintOptions_v1 (order_number, print_options, combination, " +
                                                                            "payment_status, table_number)" +
                                                                            " VALUES ('" + time_stamp + "', 'Combination', '" + customer_sequence + "', " +
                                                                            "'unpaid', '" + table_name + "');");
                                                                    db.close();
                                                                }
                                                                d.cancel();
                                                            }
                                                        });

                                                        d.show();
                                                    }

                                                    db.close();
                                                    dialog.cancel();
                                                }
                                            });
                                    builder1.show();
                                }

                                db.close();
                                dialog.cancel();
                            }
                        });
                builder.create();
                builder.show();
            }
        });
        //////////////////////////////////////////////////////////////////////
    }

    public class MenuAdapter extends BaseExpandableListAdapter {

        public Context ctx;
        public HashMap<String, List<String>>  menu_category, menu_price;
        public List<String> menu_list, price_list;

        public MenuAdapter (Context ctx, HashMap<String, List<String>> menu_category, List<String> menu_list, HashMap<String, List<String>> menu_price, List<String> price_list) {
            this.ctx = ctx;
            this.menu_category = menu_category;
            this.menu_list = menu_list;
            this.menu_price = menu_price;
            this.price_list = price_list;
        }

        @Override
        public int getGroupCount() {
            return menu_list.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return menu_category.get(menu_list.get(groupPosition)).size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return menu_list.get(groupPosition);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return menu_category.get(menu_list.get(groupPosition)).get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            String group_title = (String) getGroup(groupPosition);
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.order_per_table_menu_list_item, parent, false);
            }
            TextView tv_menu_category_list_item = (TextView) convertView.findViewById(R.id.tv_menu_category_list_item);
            tv_menu_category_list_item.setText(group_title);
            return convertView;
        }

        @Override
        public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            String child_title = (String) getChild(groupPosition, childPosition);
            String child_price = menu_price.get(price_list.get(groupPosition)).get(childPosition);

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.order_per_table_menu_sub_category_list_item, parent, false);
            }

            TextView tv_menu_sub_category = (TextView) convertView.findViewById(R.id.tv_menu_sub_category);
            tv_menu_sub_category.setText(child_title);

            TextView tv_menu_sub_price = (TextView) convertView.findViewById(R.id.tv_menu_sub_price);
            tv_menu_sub_price.setText("$" + child_price);

            final CheckBox cb = (CheckBox) convertView.findViewById(R.id.cb_menu_sub_category);
            final Pair<Integer, Integer> tag = new Pair<Integer, Integer>(groupPosition, childPosition);
            cb.setTag(tag);
            String child_category = menu_list.get(groupPosition);
            cb.setChecked(false);
            for (int i = 0; i < orders; i ++) {
                if (item[i].equals(child_title) && category[i].equals(child_category)) {
                    cb.setChecked(true);
                    break;
                }
            }

            cb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final CheckBox cb = (CheckBox) v;

                    SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
                    String category = (String) getGroup(groupPosition);
                    String item = (String) getChild(groupPosition, childPosition);
                    String unit_price = menu_price.get(price_list.get(groupPosition)).get(childPosition);

                    if (cb.isChecked()) {
                        db.execSQL("INSERT INTO OrderPerCustomer_v1 (order_number, customer_number, category, item, unit_price, price, quantity, " +
                                "variations, additional, subtraction, to_go, item_discount, item_discount_type, special_request, " +
                                "modifier, modifier_price, table_number) " +
                                "VALUES ('" + time_stamp + "', '" + selected_customer + "', '" + category + "', '" + item + "', '" + unit_price + "', " +
                                "'" + unit_price + "', '1', '', '', '', 'false', '', '', '', '', '0', '" + table_name + "');");

                        //Change payment status to unpaid
                        Cursor c = db.rawQuery("SELECT * FROM PrintOptions_v1 WHERE order_number='" + time_stamp + "' AND table_number='" + table_name + "'", null);

                        if (c.moveToFirst()) {
                            String combination = c.getString(c.getColumnIndex("combination"));
                            String[] combination_array = combination.split(",");
                            int i;
                            for (i = 0; i < combination_array.length; i++) {
                                if (combination_array[i].equals(selected_customer)) {
                                    db.execSQL("UPDATE PrintOptions_v1 SET payment_status='unpaid' WHERE Id=" + c.getInt(c.getColumnIndex("Id")));
                                    break;
                                }
                            }
                            while (c.moveToNext()) {
                                combination = c.getString(c.getColumnIndex("combination"));
                                combination_array = combination.split(",");

                                for (i = 0; i < combination_array.length; i++) {
                                    if (combination_array[i].equals(selected_customer)) {
                                        db.execSQL("UPDATE PrintOptions_v1 SET payment_status='unpaid' WHERE Id=" + c.getInt(c.getColumnIndex("Id")));
                                        break;
                                    }
                                }
                            }
                        }
                        c.close();
                    } else {
                        Cursor c = db.rawQuery("SELECT * FROM OrderPerCustomer_v1 WHERE order_number='" + time_stamp + "' AND " +
                                "customer_number='" + selected_customer + "' AND item='" + item + "' AND category='" + category + "'", null);
                        int id = 0;
                        boolean flag = false;

                        if (c.moveToFirst()) {
                            id = c.getInt(c.getColumnIndex("Id"));
                            flag = true;
                        }

                        if (flag) {
                            db.execSQL("DELETE FROM OrderPerCustomer_v1 WHERE Id=" + id +";");
                        }

                        c.close();
                    }

                    db.close();

                    fetchExistingOrders();
                }
            });

            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }
    }

    private class billListAdapter extends ArrayAdapter<String> {

        public billListAdapter(Context context, int resource, int textViewResourceId, String[] objects) {
            super(context, resource, textViewResourceId, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.order_per_table_bill_list_item, parent, false);

            TextView tv_bill_list_name = (TextView) row.findViewById(R.id.tv_bill_list_name);
            TextView tv_bill_list_customer = (TextView) row.findViewById(R.id.tv_bill_list_customer);
            TextView tv_bill_list_quantity = (TextView) row.findViewById(R.id.tv_bill_list_quantity);
            TextView tv_bill_list_unit_price = (TextView) row.findViewById(R.id.tv_bill_list_unit_price);
            TextView tv_bill_list_price = (TextView) row.findViewById(R.id.tv_bill_list_price);

            tv_bill_list_name.setText(list_item[position]);
            tv_bill_list_customer.setText(list_customer_number[position]);
            tv_bill_list_quantity.setText(list_quantity[position]);
            tv_bill_list_unit_price.setText(list_unit_price[position]);
            tv_bill_list_price.setText(list_price[position]);

            return row;
        }
    }

    private class modifierListAdapter extends ArrayAdapter<String> {

        public modifierListAdapter(Context context, int resource, int textViewResourceId, String[] objects) {
            super(context, resource, textViewResourceId, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.modifier_list_item, parent, false);

            TextView tv_modifier_name = (TextView) row.findViewById(R.id.tv_modifier_name);
            TextView tv_modifier_price = (TextView) row.findViewById(R.id.tv_modifier_price);
            CheckBox cb_modifier = (CheckBox) row.findViewById(R.id.cb_modifier);

            tv_modifier_name.setText(item_modifier_name[position]);
            tv_modifier_price.setText(item_modifier_price[position]);
            cb_modifier.setChecked(false);

            if (dialog_modifiers != null) {
                for (int i = 0; i < dialog_modifiers.length; i++) {
                    if (item_modifier_name[position].equals(dialog_modifiers[i])) {
                        cb_modifier.setChecked(true);
                        item_modifier_check[position] = true;
                        break;
                    }
                }
            } else {
                cb_modifier.setChecked(false);
            }

            return row;
        }
    }

    private class separateBillsListAdapter extends ArrayAdapter<String> {

        public separateBillsListAdapter(Context context, int resource, int textViewResourceId, String[] objects) {
            super(context, resource, textViewResourceId, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.separate_bill_list_item, parent, false);

            TextView tv_separate_bills_customer_name = (TextView) row.findViewById(R.id.tv_separate_bills_customer_number);
            CheckBox cb_separate_bills = (CheckBox) row.findViewById(R.id.cb_separate_bills);

            tv_separate_bills_customer_name.setText(array_of_customers[position]);
            cb_separate_bills.setChecked(false);

            return row;
        }
    }

    private void refreshBillSegment() {
        //billAdapter.notifyDataSetChanged();

        billAdapter = new billListAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, R.id.tv_bill_list_name, list_order_number);
        lv_order_list.setAdapter(billAdapter);

        subtotal = 0;
        discount = 0;
        tax = 0;
        total = 0;
        tips = 0;
        balance = 0;
        int i;

        for (i = 0; i < list_orders; i++) {
            subtotal += Float.parseFloat(list_price[i]);
        }

        SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
        Cursor c = db.rawQuery("SELECT * FROM BillSupplementary_v1 WHERE order_number='" + time_stamp + "'", null);

        if (c.moveToFirst()) {
            if (c.getString(c.getColumnIndex("discount_type")).equals("Percentage")) {
                discount = (subtotal * Float.parseFloat(c.getString(c.getColumnIndex("discount"))) / 100 );
            } else if (c.getString(c.getColumnIndex("discount_type")).equals("Amount")) {
                discount = Float.parseFloat(c.getString(c.getColumnIndex("discount")));
            }
        }
        c.close();
        db.close();

        tax = (subtotal - discount) * (float)0.15;

        total = subtotal - discount + tax;

        balance = total + tips;

        ///////////////////Value Display//////////////////////
        DecimalFormat df = new DecimalFormat("0.00");
        //df.setRoundingMode(RoundingMode.CEILING);
        TextView tv_order_per_table_subtotal = (TextView) findViewById(R.id.tv_order_per_table_subtotal);
        tv_order_per_table_subtotal.setText("Subtotal $ " + df.format(subtotal));

        TextView tv_order_per_table_discount = (TextView) findViewById(R.id.tv_order_per_table_discount);
        tv_order_per_table_discount.setText("Discount $ " + df.format(discount));

        TextView tv_order_per_table_tax = (TextView) findViewById(R.id.tv_order_per_table_tax);
        tv_order_per_table_tax.setText("Tax $ " + df.format(tax));

        TextView tv_order_per_table_total = (TextView) findViewById(R.id.tv_order_per_table_total);
        tv_order_per_table_total.setText("Total $ " + df.format(total));

        TextView tv_order_per_table_balance_due = (TextView) findViewById(R.id.tv_order_per_table_balance_due);
        tv_order_per_table_balance_due.setText("Balance Due $ " + df.format(balance));
        //////////////////////////////////////////////////////
    }

    private void fetchExistingOrders() {
        SQLiteDatabase db;
        list_orders = 0;
        db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);

        Cursor c = db.rawQuery("SELECT * FROM OrderPerCustomer_v1 WHERE order_number='" + time_stamp + "' AND table_number='" + table_name + "'", null);

        list_order_number = new String[c.getCount()];
        list_customer_number = new String[c.getCount()];
        list_category = new String[c.getCount()];
        list_item = new String[c.getCount()];
        list_unit_price = new String[c.getCount()];
        list_price = new String[c.getCount()];
        list_quantity = new String[c.getCount()];
        list_additional = new String[c.getCount()];
        list_special_request = new String[c.getCount()];

        int i = 0;

        if (c.moveToFirst()) {
            list_order_number[i] = c.getString(c.getColumnIndex("order_number"));
            list_customer_number[i] = c.getString(c.getColumnIndex("customer_number"));
            list_category[i] = c.getString(c.getColumnIndex("category"));
            list_item[i] = c.getString(c.getColumnIndex("item"));
            list_unit_price[i] = c.getString(c.getColumnIndex("unit_price"));
            list_price[i] = c.getString(c.getColumnIndex("price"));
            list_quantity[i] = c.getString(c.getColumnIndex("quantity"));
            list_additional[i] = c.getString(c.getColumnIndex("additional"));
            list_special_request[i] = c.getString(c.getColumnIndex("special_request"));
            i++;

            while (c.moveToNext()) {
                list_order_number[i] = c.getString(c.getColumnIndex("order_number"));
                list_customer_number[i] = c.getString(c.getColumnIndex("customer_number"));
                list_category[i] = c.getString(c.getColumnIndex("category"));
                list_item[i] = c.getString(c.getColumnIndex("item"));
                list_unit_price[i] = c.getString(c.getColumnIndex("unit_price"));
                list_price[i] = c.getString(c.getColumnIndex("price"));
                list_quantity[i] = c.getString(c.getColumnIndex("quantity"));
                list_additional[i] = c.getString(c.getColumnIndex("additional"));
                list_special_request[i] = c.getString(c.getColumnIndex("special_request"));
                i++;
            }
        }

        list_orders = i;

        c.close();
        db.close();

        refreshBillSegment();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Toast.makeText(getApplicationContext(), "Click Hold or Cancel to Go Back", Toast.LENGTH_SHORT).show();
    }
}
