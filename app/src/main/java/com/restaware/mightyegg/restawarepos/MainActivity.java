package com.restaware.mightyegg.restawarepos;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    Context context;
    ProgressDialog progressDialog;

    boolean progressDialogShow;

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    String restaurant_name, restaurant_password, authentication_token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;
        progressDialogShow = false;

        pref = getSharedPreferences("Restaurant_Login.conf", Context.MODE_PRIVATE);
        editor = pref.edit();

        ////////////Common Initialization///////////////
        editor.putString("api", "http://10.0.2.2:3000/api/v1/user_tables/");
        //editor.putString("authentication_token", "");
        editor.apply();
        ////////////////////////////////////////////////

//        ReInitialize();

        //////////Initialize Selected Floor////////////
        TableLayout.selectedFloor = "";
        ///////////////////////////////////////////////

        Thread timer = new Thread() {
            @Override
            public void run() {
                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            restaurant_name = pref.getString("restaurant_name", "");
                            restaurant_password = pref.getString("restaurant_password", "");
                            authentication_token = pref.getString("authentication_token", "");

                            progressDialog = new ProgressDialog(context);
                            progressDialog.setTitle("Checking Credentials");
                            progressDialog.setMessage("Please wait a while");
                            progressDialog.show();
                            progressDialogShow = true;

                            initialServerCheck();
                            DatabaseInitialization();
                        }
                    });
                }
            }
        };

        timer.start();


    }

    private void ReInitialize() {
        ////////////Re-initiate///////////////
        editor.putString("restaurant_name", "");
        editor.putString("restaurant_password", "");
        editor.apply();
        //////////////////////////////////////
    }

    private void initialServerCheck() {
        if(restaurant_name.equals("") || restaurant_password.equals("") || authentication_token.equals("")) {
            Intent intent = new Intent(MainActivity.this, RestawareLoginPage.class);
            startActivity(intent);
            MainActivity.this.finish();

            if (progressDialogShow) {
                progressDialogShow = false;
                progressDialog.dismiss();
            }
        }
        else {
            //Re-check existing credentials
//            if (isNetworkAvailable()) {
//                /////////////send data to server for checking////////////////////
//                SharedPreferences pref = getSharedPreferences("Restaurant_Login.conf", Context.MODE_PRIVATE);
//
//                new ServerCheck().execute(pref.getString("api", "") + "user_authentication.json?email=" + restaurant_name + "&password=" + restaurant_password);
//                /////////////////////////////////////////////////////////////////
//            } else {
//                Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
//            }

            //Re-direct without checking server
            Toast.makeText(getApplicationContext(), "Login Successful", Toast.LENGTH_SHORT).show();

            if (progressDialogShow) {
                progressDialogShow = false;
                progressDialog.dismiss();
            }

            Intent intent = new Intent(MainActivity.this, EmployeeLoginPage.class);
            startActivity(intent);
            MainActivity.this.finish();

            /////////////////////Demo Navigation///////////////////////
//            Intent intent = new Intent(MainActivity.this, EmployeeLoginPage.class);
//            startActivity(intent);
//            MainActivity.this.finish();
//
//            if (progressDialogShow) {
//                progressDialogShow = false;
//                progressDialog.dismiss();
//            }
            /////////////////////////////////////////////////////////////
        }
    }

    private void DatabaseInitialization() {
        ///////////////////////////Local Database////////////////////////////
        SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);

        db.execSQL("CREATE TABLE IF NOT EXISTS FloorTable_v1 (Id INTEGER PRIMARY KEY, name VARCHAR);");

        //////////Re-initiate TableArrangement Table//////////////
//        db.execSQL("DROP TABLE IF EXISTS TableArrangement_v1;"); //Arrangement of tables and individual status
//        db.execSQL("DROP TABLE IF EXISTS CustomerPerTable_v1;"); //Number of customers in each occupied table
//        db.execSQL("DROP TABLE IF EXISTS OrderPerCustomer_v1;"); //Orders against each individual customer
//        db.execSQL("DROP TABLE IF EXISTS OrderLog_v1;"); //Once order has been cleared
//        db.execSQL("DROP TABLE IF EXISTS BillSupplementary_v1;"); //Discount options for whole menu
//        db.execSQL("DROP TABLE IF EXISTS PrintOptions_v1"); //Options for printing bill
//        db.execSQL("DROP TABLE IF EXISTS TabDetails_v1"); //Details for opened tabs
//        db.execSQL("DROP TABLE IF EXISTS EmployeeAttendance_v1"); //Employee Attendance log
//        db.execSQL("DROP TABLE IF EXISTS user_tables"); //User details against employee attendance
        //////////////////////////////////////////////////////////

        db.execSQL("CREATE TABLE IF NOT EXISTS TableArrangement_v1 (Id INTEGER PRIMARY KEY, type VARCHAR, number VARCHAR, " +
                "xCoordinate VARCHAR, yCoordinate VARCHAR, capacity VARCHAR, floor VARCHAR, status VARCHAR, order_number VARCHAR);");

        //////////Tables for Order////////////
        db.execSQL("CREATE TABLE IF NOT EXISTS CustomerPerTable_v1 (Id INTEGER PRIMARY KEY, table_name VARCHAR, customer VARCHAR, " +
                "floor_name VARCHAR, time_stamp VARCHAR);");

        db.execSQL("CREATE TABLE IF NOT EXISTS OrderPerCustomer_v1 (Id INTEGER PRIMARY KEY, order_number VARCHAR, customer_number VARCHAR, category VARCHAR, " +
                "item VARCHAR, unit_price VARCHAR, price VARCHAR, quantity VARCHAR, modifier VARCHAR, additional VARCHAR, subtraction VARCHAR, " +
                "variations VARCHAR, item_discount VARCHAR, item_discount_type VARCHAR, special_request VARCHAR, to_go VARCHAR, modifier_price VARCHAR, " +
                "table_number VARCHAR);");

        db.execSQL("CREATE TABLE IF NOT EXISTS MenuCategories_v1 (Id INTEGER PRIMARY KEY, category_name VARCHAR);");

        db.execSQL("CREATE TABLE IF NOT EXISTS MenuItems_v1 (Id INTEGER PRIMARY KEY, item_name VARCHAR, " +
                "item_price VARCHAR, item_desc VARCHAR, item_category VARCHAR);");

        db.execSQL("CREATE TABLE IF NOT EXISTS OrderLog_v1 (Id INTEGER PRIMARY KEY, order_number VARCHAR, table_number VARCHAR, total_customers VARCHAR, " +
                "subtotal VARCHAR, discount VARCHAR, tax VARCHAR, total VARCHAR, balance VARCHAR, paid VARCHAR, change VARCHAR, customer_numbers VARCHAR, " +
                "payment_type VARCHAR, tips VARCHAR, payment_time VARCHAR);");

        db.execSQL("CREATE TABLE IF NOT EXISTS BillSupplementary_v1 (Id INTEGER PRIMARY KEY, order_number VARCHAR, discount VARCHAR, discount_type VARCHAR);");

        db.execSQL("CREATE TABLE IF NOT EXISTS ModifiersForMenuItems_v1 (Id INTEGER PRIMARY KEY, modification VARCHAR, extra_price VARCHAR, item_id VARCHAR);");

        db.execSQL("CREATE TABLE IF NOT EXISTS VariationsForMenuItems_v1 (Id INTEGER PRIMARY KEY, variation VARCHAR, item_id VARCHAR);");

        db.execSQL("CREATE TABLE IF NOT EXISTS PrintOptions_v1 (Id INTEGER PRIMARY KEY, order_number VARCHAR, print_options VARCHAR, " +
                "combination VARCHAR, payment_status VARCHAR, table_number VARCHAR);");

        db.execSQL("CREATE TABLE IF NOT EXISTS TabDetails_v1 (Id INTEGER PRIMARY KEY, order_number VARCHAR, customer_name VARCHAR, card_number VARCHAR, " +
                "status VARCHAR);");

        db.execSQL("CREATE TABLE IF NOT EXISTS EmployeeAttendance_v1 (Id INTEGER PRIMARY KEY, employee_pin VARCHAR, in_time VARCHAR, " +
                "out_time VARCHAR);");

        db.execSQL("CREATE TABLE IF NOT EXISTS user_tables (Id INTEGER PRIMARY KEY, fullname VARCHAR, email VARCHAR, user_designation VARCHAR, " +
                "employee_pin VARCHAR);");
        //////////////////////////////////////

        //////////////Default Menu//////////////////
//        db.execSQL("INSERT INTO MenuCategories_v1 (category_name) VALUES ('Appetizer');");
//        db.execSQL("INSERT INTO MenuCategories_v1 (category_name) VALUES ('Soups');");
//        db.execSQL("INSERT INTO MenuCategories_v1 (category_name) VALUES ('Fries');");
//        db.execSQL("INSERT INTO MenuCategories_v1 (category_name) VALUES ('Rice');");
//        db.execSQL("INSERT INTO MenuCategories_v1 (category_name) VALUES ('Sides');");
//        db.execSQL("INSERT INTO MenuCategories_v1 (category_name) VALUES ('Main Dishes');");
//        db.execSQL("INSERT INTO MenuCategories_v1 (category_name) VALUES ('Desserts');");
//        db.execSQL("INSERT INTO MenuCategories_v1 (category_name) VALUES ('Drinks');");
//
//        db.execSQL("INSERT INTO MenuItems_v1 (item_name, item_price, item_desc, item_category) VALUES ('Nachos', '3.50', 'Description for Nachos', 'Appetizer');");
//        db.execSQL("INSERT INTO MenuItems_v1 (item_name, item_price, item_desc, item_category) VALUES ('Chicken Pizza Cone', '1.80', 'Chicken item', 'Appetizer');");
//        db.execSQL("INSERT INTO MenuItems_v1 (item_name, item_price, item_desc, item_category) VALUES ('Cheese Chicks', '3.80', 'Chicken with cheese', 'Appetizer');");
//
//        db.execSQL("INSERT INTO MenuItems_v1 (item_name, item_price, item_desc, item_category) VALUES ('French Fries', '1.90', 'Potato Fries', 'Fries');");
//        db.execSQL("INSERT INTO MenuItems_v1 (item_name, item_price, item_desc, item_category) VALUES ('Wedges', '2.20', 'Large potato pieces', 'Fries');");
//
//        db.execSQL("INSERT INTO MenuItems_v1 (item_name, item_price, item_desc, item_category) VALUES ('Sea Food Noodle Soup', '4.20', 'Multiple sea food items', 'Soups');");
//        db.execSQL("INSERT INTO MenuItems_v1 (item_name, item_price, item_desc, item_category) VALUES ('Cream of  Chicken/Mushroom Soup', '3.40', 'Choose between Chicken or Mushroom', 'Soups');");
//
//        db.execSQL("INSERT INTO MenuItems_v1 (item_name, item_price, item_desc, item_category) VALUES ('Thai Vegetable Fried Rice', '5.10', 'Thai item', 'Rice');");
//        db.execSQL("INSERT INTO MenuItems_v1 (item_name, item_price, item_desc, item_category) VALUES ('Thai Mixed Fried Rice', '5.50', 'Mixed non-veg items', 'Rice');");
//        db.execSQL("INSERT INTO MenuItems_v1 (item_name, item_price, item_desc, item_category) VALUES ('Chinese Fried Rice', '5.40', 'Veg item', 'Rice');");
//
//        db.execSQL("INSERT INTO MenuItems_v1 (item_name, item_price, item_desc, item_category) VALUES ('Prawn Papaya Salad', '6.00', 'Prawn item', 'Sides');");
//        db.execSQL("INSERT INTO MenuItems_v1 (item_name, item_price, item_desc, item_category) VALUES ('Lamb Gai Salad', '6.30', 'Mutton item', 'Sides');");
//        db.execSQL("INSERT INTO MenuItems_v1 (item_name, item_price, item_desc, item_category) VALUES ('Seafood Salad', '7.80', 'Shrimp and Squid', 'Sides');");
//        db.execSQL("INSERT INTO MenuItems_v1 (item_name, item_price, item_desc, item_category) VALUES ('Thai Beef Salad', '6.60', 'Beef item', 'Sides');");
//        db.execSQL("INSERT INTO MenuItems_v1 (item_name, item_price, item_desc, item_category) VALUES ('Prawn Cashew Nut Salad', '6.10', 'Prawn and nut item', 'Sides');");
//
//        db.execSQL("INSERT INTO MenuItems_v1 (item_name, item_price, item_desc, item_category) VALUES ('Grilled Chicken', '8.60', '1/4th Chicken', 'Main Dishes');");
//        db.execSQL("INSERT INTO MenuItems_v1 (item_name, item_price, item_desc, item_category) VALUES ('Thai Fried Chicken', '8.60', '4 pieces', 'Main Dishes');");
//        db.execSQL("INSERT INTO MenuItems_v1 (item_name, item_price, item_desc, item_category) VALUES ('Mongolian Beef', '8.50', 'Beef item', 'Main Dishes');");
//        db.execSQL("INSERT INTO MenuItems_v1 (item_name, item_price, item_desc, item_category) VALUES ('Beef Chow Foon', '7.80', 'Beef item', 'Main Dishes');");
//        db.execSQL("INSERT INTO MenuItems_v1 (item_name, item_price, item_desc, item_category) VALUES ('Fried Chili Crab', '8.70', 'Crab item', 'Main Dishes');");
//        db.execSQL("INSERT INTO MenuItems_v1 (item_name, item_price, item_desc, item_category) VALUES ('Flaming Duck', '12.40', 'Duck item', 'Main Dishes');");
//        db.execSQL("INSERT INTO MenuItems_v1 (item_name, item_price, item_desc, item_category) VALUES ('Fish/Prawn Sizzling', '8.30', 'Select between fishes', 'Main Dishes');");
//
//        db.execSQL("INSERT INTO MenuItems_v1 (item_name, item_price, item_desc, item_category) VALUES ('Ice Cream', '1.60', 'Choose between scoops', 'Desserts');");
//        db.execSQL("INSERT INTO MenuItems_v1 (item_name, item_price, item_desc, item_category) VALUES ('Chocolate Mousse Cake', '2.80', 'Cake', 'Desserts');");
//        db.execSQL("INSERT INTO MenuItems_v1 (item_name, item_price, item_desc, item_category) VALUES ('Fruit Cocktail with Ice Cream', '2.50', 'Drinks with ice cream', 'Desserts');");
//
//        db.execSQL("INSERT INTO MenuItems_v1 (item_name, item_price, item_desc, item_category) VALUES ('Tea/Coffee', '0.75', 'Tea or Coffee', 'Drinks');");
//        db.execSQL("INSERT INTO MenuItems_v1 (item_name, item_price, item_desc, item_category) VALUES ('Soft Drinks', '0.50', 'Choose between all', 'Drinks');");
        ////////////////////////////////////////////

        ///////////////Default Employees///////////////
//        db.execSQL("INSERT INTO user_tables (fullname, email, user_designation, employee_pin) VALUES ('Tauseef Mamun', 'tm@mightyegg.net', " +
//                "'Manager', '1111');");
//
//        db.execSQL("INSERT INTO user_tables (fullname, email, user_designation, employee_pin) VALUES ('Saddam Hossain', 'sh@mightyegg.net', " +
//                "'Employee', '1234');");
//
//        db.execSQL("INSERT INTO user_tables (fullname, email, user_designation, employee_pin) VALUES ('Saiful Islam Anik', 'sia@mightyegg.net', " +
//                "'Employee', '7890');");
        ///////////////////////////////////////////////

        Cursor c;
        String item_name = "Beef Chow Foon", item_category = "Main Dishes";
        int id;

        /////////////Create Demo Variations//////////////
//        c = db.rawQuery("SELECT * FROM MenuItems_v1 WHERE item_name='" + item_name + "' AND item_category='" + item_category + "'", null);
//
//        if (c.moveToFirst()) {
//            id = c.getInt(c.getColumnIndex("Id"));
//
//            db.execSQL("INSERT INTO VariationsForMenuItems_v1 (variation, item_id) VALUES ('Extra Spicy', '" + id + "');");
//            db.execSQL("INSERT INTO VariationsForMenuItems_v1 (variation, item_id) VALUES ('Spicy', '" + id + "');");
//            db.execSQL("INSERT INTO VariationsForMenuItems_v1 (variation, item_id) VALUES ('Regular', '" + id + "');");
//        }
//
//        c.close();
        /////////////////////////////////////////////////

        ////////////Create Demo Modifiers/////////////////
//        c = db.rawQuery("SELECT * FROM MenuItems_v1 WHERE item_name='" + item_name + "' AND item_category='" + item_category + "'", null);
//
//        if (c.moveToFirst()) {
//            id = c.getInt(c.getColumnIndex("Id"));
//
//            db.execSQL("INSERT INTO ModifiersForMenuItems_v1 (modification, extra_price, item_id) VALUES ('Extra Cheese', '1.00', '" + id + "');");
//            db.execSQL("INSERT INTO ModifiersForMenuItems_v1 (modification, extra_price, item_id) VALUES ('Extra Pickle', '0.40', '" + id + "');");
//            db.execSQL("INSERT INTO ModifiersForMenuItems_v1 (modification, extra_price, item_id) VALUES ('Extra Salt', '0.10', '" + id + "');");
//        }
//
//        c.close();
        //////////////////////////////////////////////////
        db.close();
        /////////////////////////////////////////////////////////////////////
    }

    class ServerCheck extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String result = "";

            try {
                Log.d("URL", params[0]);
                URL url = new URL(params[0]);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setReadTimeout(20000);
                conn.setConnectTimeout(30000);

                //////////////Additional for POST call////////////////
//                conn.setDoInput(true);
//                conn.setDoOutput(true);

//                Uri.Builder builder = new Uri.Builder().appendQueryParameter("restaurant_name", restaurant_name)
//                        .appendQueryParameter("restaurant_password", restaurant_password);
//
//                String query = builder.build().getEncodedQuery();
//
//                OutputStream os = conn.getOutputStream();
//                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
//                writer.write(query);
//                writer.flush();
//                writer.close();
//                os.close();

//                conn.connect();
                ///////////////////////////////////////////////////////

                int responseCode = conn.getResponseCode();

                Log.d("RESPONSE CODE", String.valueOf(responseCode));

                if(responseCode == HttpsURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while((line = reader.readLine()) != null) {
                        result += line;
                    }
                }
                else {
                    result = "";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d("RESPONSE", s);

            ////////////////JSON Parsing////////////////////
            if (!s.isEmpty()) {
                try {
                    //JSONArray jsonArray = new JSONArray(s);
                    JSONObject jsonObject = new JSONObject(s);

                    boolean success = jsonObject.getBoolean("success");
                    JSONObject result = jsonObject.getJSONObject("result");
                    String authentication_code = result.getString("authentication_token");

                    Log.d("RECEIVED VALUES", success + " " + authentication_code);

                    if (success) {
                        SharedPreferences pref = getSharedPreferences("Restaurant_Login.conf", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("authentication_token", authentication_code);
                        editor.apply();

                        Toast.makeText(getApplicationContext(), "Login Successful", Toast.LENGTH_SHORT).show();

                        if (progressDialogShow) {
                            progressDialogShow = false;
                            progressDialog.dismiss();
                        }

                        Intent intent = new Intent(MainActivity.this, EmployeeLoginPage.class);
                        startActivity(intent);
                        MainActivity.this.finish();
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Login Failed", Toast.LENGTH_SHORT).show();

                        if (progressDialogShow) {
                            progressDialogShow = false;
                            progressDialog.dismiss();
                        }

                        Intent intent = new Intent(MainActivity.this, RestawareLoginPage.class);
                        startActivity(intent);
                        MainActivity.this.finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                if (progressDialogShow) {
                    progressDialogShow = false;
                    progressDialog.dismiss();
                }

                Toast.makeText(getApplicationContext(), "Invalid Credentials", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this, RestawareLoginPage.class);
                startActivity(intent);
                MainActivity.this.finish();
            }
            ////////////////////////////////////////////////
        }
    }

    private boolean isNetworkAvailable(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetInfo != null && activeNetInfo.isConnected();
    }
}
