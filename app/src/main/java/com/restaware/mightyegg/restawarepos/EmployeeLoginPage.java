package com.restaware.mightyegg.restawarepos;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

public class EmployeeLoginPage extends AppCompatActivity implements View.OnTouchListener {

    String redirectTo;
    String PIN;

    ImageButton ibPOS, ibClockIn, ibClockOut, ibButton1, ibButton2, ibButton3, ibButton4, ibButton5, ibButton6, ibButton7, ibButton8, ibButton9, ibButton0, ibClear, ibLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_login_page);

        PIN = "";
        redirectTo = "";

        ibPOS = (ImageButton) findViewById(R.id.ibPOS);
        ibPOS.setOnTouchListener(this);

        ibClockIn = (ImageButton) findViewById(R.id.ibClockIn);
        ibClockIn.setOnTouchListener(this);

        ibClockOut = (ImageButton) findViewById(R.id.ibClockOut);
        ibClockOut.setOnTouchListener(this);

        ibButton1 = (ImageButton) findViewById(R.id.ibButton1);
        ibButton2 = (ImageButton) findViewById(R.id.ibButton2);
        ibButton3 = (ImageButton) findViewById(R.id.ibButton3);
        ibButton4 = (ImageButton) findViewById(R.id.ibButton4);
        ibButton5 = (ImageButton) findViewById(R.id.ibButton5);
        ibButton6 = (ImageButton) findViewById(R.id.ibButton6);
        ibButton7 = (ImageButton) findViewById(R.id.ibButton7);
        ibButton8 = (ImageButton) findViewById(R.id.ibButton8);
        ibButton9 = (ImageButton) findViewById(R.id.ibButton9);
        ibButton0 = (ImageButton) findViewById(R.id.ibButton0);

        ibButton1.setOnTouchListener(this);
        ibButton2.setOnTouchListener(this);
        ibButton3.setOnTouchListener(this);
        ibButton4.setOnTouchListener(this);
        ibButton5.setOnTouchListener(this);
        ibButton6.setOnTouchListener(this);
        ibButton7.setOnTouchListener(this);
        ibButton8.setOnTouchListener(this);
        ibButton9.setOnTouchListener(this);
        ibButton0.setOnTouchListener(this);

        ibClear = (ImageButton) findViewById(R.id.ibClear);
        ibClear.setOnTouchListener(this);

        ibLogin = (ImageButton) findViewById(R.id.ibLogin);
        ibLogin.setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        ImageView ivPIN1 = (ImageView) findViewById(R.id.ivPIN1);
        ImageView ivPIN2 = (ImageView) findViewById(R.id.ivPIN2);
        ImageView ivPIN3 = (ImageView) findViewById(R.id.ivPIN3);
        ImageView ivPIN4 = (ImageView) findViewById(R.id.ivPIN4);

        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();

        if(v.getId() == R.id.ibPOS) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    ibPOS.setImageResource(R.drawable.employee_login_pos_clicked);
                    redirectTo = "POS";
                    break;
                case MotionEvent.ACTION_UP:
                    ibPOS.setImageResource(R.drawable.employee_login_pos_button);
                    break;
            }
        }
        else if(v.getId() == R.id.ibClockIn) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    ibClockIn.setImageResource(R.drawable.employee_login_clock_in_clicked);
                    redirectTo = "Clock In";
                    break;
                case MotionEvent.ACTION_UP:
                    ibClockIn.setImageResource(R.drawable.employee_login_clockin_button);
                    break;
            }
        }
        else if(v.getId() == R.id.ibClockOut) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    ibClockOut.setImageResource(R.drawable.employee_login_clock_out_clicked);
                    redirectTo = "Clock Out";
                    break;
                case MotionEvent.ACTION_UP:
                    ibClockOut.setImageResource(R.drawable.employee_login_clockout_button);
                    break;
            }
        }
        else if(v.getId() == R.id.ibButton1) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    ibButton1.setImageResource(R.drawable.employee_login_1_clicked);
                    if(PIN.length() >= 0 && PIN.length() < 4) {
                        PIN = PIN + "1";
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    ibButton1.setImageResource(R.drawable.employee_login_button_1);
                    break;
            }
        }
        else if(v.getId() == R.id.ibButton2) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    ibButton2.setImageResource(R.drawable.employee_login_2_clicked);
                    if(PIN.length() >= 0 && PIN.length() < 4) {
                        PIN = PIN + "2";
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    ibButton2.setImageResource(R.drawable.employee_login_button_2);
                    break;
            }
        }
        else if(v.getId() == R.id.ibButton3) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    ibButton3.setImageResource(R.drawable.employee_login_3_clicked);
                    if(PIN.length() >= 0 && PIN.length() < 4) {
                        PIN = PIN + "3";
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    ibButton3.setImageResource(R.drawable.employee_login_button_3);
                    break;
            }
        }
        else if(v.getId() == R.id.ibButton4) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    ibButton4.setImageResource(R.drawable.employee_login_4_clicked);
                    if(PIN.length() >= 0 && PIN.length() < 4) {
                        PIN = PIN + "4";
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    ibButton4.setImageResource(R.drawable.employee_login_button_4);
                    break;
            }
        }
        else if(v.getId() == R.id.ibButton5) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    ibButton5.setImageResource(R.drawable.employee_login_5_clicked);
                    if(PIN.length() >= 0 && PIN.length() < 4) {
                        PIN = PIN + "5";
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    ibButton5.setImageResource(R.drawable.employee_login_button_5);
                    break;
            }
        }
        else if(v.getId() == R.id.ibButton6) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    ibButton6.setImageResource(R.drawable.employee_login_6_clicked);
                    if(PIN.length() >= 0 && PIN.length() < 4) {
                        PIN = PIN + "6";
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    ibButton6.setImageResource(R.drawable.employee_login_button_6);
                    break;
            }
        }
        else if(v.getId() == R.id.ibButton7) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    ibButton7.setImageResource(R.drawable.employee_login_7_clicked);
                    if(PIN.length() >= 0 && PIN.length() < 4) {
                        PIN = PIN + "7";
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    ibButton7.setImageResource(R.drawable.employee_login_button_7);
                    break;
            }
        }
        else if(v.getId() == R.id.ibButton8) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    ibButton8.setImageResource(R.drawable.employee_login_8_clicked);
                    if(PIN.length() >= 0 && PIN.length() < 4) {
                        PIN = PIN + "8";
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    ibButton8.setImageResource(R.drawable.employee_login_button_8);
                    break;
            }
        }
        else if(v.getId() == R.id.ibButton9) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    ibButton9.setImageResource(R.drawable.employee_login_9_clicked);
                    if(PIN.length() >= 0 && PIN.length() < 4) {
                        PIN = PIN + "9";
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    ibButton9.setImageResource(R.drawable.employee_login_button_9);
                    break;
            }
        }
        else if(v.getId() == R.id.ibButton0) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    ibButton0.setImageResource(R.drawable.employee_login_0_clicked);
                    if(PIN.length() >= 0 && PIN.length() < 4) {
                        PIN = PIN + "0";
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    ibButton0.setImageResource(R.drawable.employee_login_button_0);
                    break;
            }
        }
        else if(v.getId() == R.id.ibClear) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    ibClear.setImageResource(R.drawable.employee_login_clear_clicked);
                    if(PIN.length() > 0) {
                        PIN = PIN.substring(0, PIN.length() - 1);
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    ibClear.setImageResource(R.drawable.employee_login_clear_button);
                    break;
            }
            Log.d("PIN", PIN);
        }
        else if(v.getId() == R.id.ibLogin) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    boolean return_type;
                    ibLogin.setImageResource(R.drawable.employee_login_login_clicked);
                    if(PIN.length() < 4) {
                        Toast.makeText(getApplicationContext(), "Incomplete PIN", Toast.LENGTH_SHORT).show();
                    }
                    else if(redirectTo.isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Please Select an Option", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
                        Cursor c = db.rawQuery("SELECT * FROM user_tables WHERE employee_pin='" + PIN + "'", null);

                        if (c.moveToFirst()) {
                            //TODO check database for verifying employee pin
                            SharedPreferences pref = getSharedPreferences("Employee_Login", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = pref.edit();

                            editor.putString("employee_designation", c.getString(c.getColumnIndex("user_designation")));
                            editor.putString("employee_id", PIN);
                            editor.apply();

                            if(redirectTo.equals("POS")) {
                                return_type = EmployeeAttendance(PIN, ts, "In");

                                if (return_type)
                                    Toast.makeText(getApplicationContext(), "Successfully Clocked In", Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(getApplicationContext(), "Already Clocked In", Toast.LENGTH_SHORT).show();
                                ////////////////successful login///////////////////
                                Intent intent = new Intent(EmployeeLoginPage.this, ViewTables.class);
                                startActivity(intent);
                                ///////////////////////////////////////////////////
                            } else if (redirectTo.equals("Clock In")) {
                                return_type = EmployeeAttendance(PIN, ts, "In");

                                if (return_type)
                                    Toast.makeText(getApplicationContext(), "Successfully Clocked In", Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(getApplicationContext(), "Failed to Clock In", Toast.LENGTH_SHORT).show();
                            } else if (redirectTo.equals("Clock Out")) {
                                return_type = EmployeeAttendance(PIN, ts, "Out");

                                if (return_type)
                                    Toast.makeText(getApplicationContext(), "Successfully Clocked Out", Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(getApplicationContext(), "Failed to Clock Out", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Invalid PIN", Toast.LENGTH_SHORT).show();
                        }

                        c.close();
                        db.close();
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    ibLogin.setImageResource(R.drawable.employee_login_login_button);
                    break;
            }
            Log.d("PIN", PIN);
        }

        int length = PIN.length();

        switch (length) {
            case 0:
                ivPIN1.setImageResource(R.drawable.employee_login_white_dot);
                ivPIN2.setImageResource(R.drawable.employee_login_white_dot);
                ivPIN3.setImageResource(R.drawable.employee_login_white_dot);
                ivPIN4.setImageResource(R.drawable.employee_login_white_dot);
                break;
            case 1:
                ivPIN1.setImageResource(R.drawable.employee_login_blue_dot);
                ivPIN2.setImageResource(R.drawable.employee_login_white_dot);
                ivPIN3.setImageResource(R.drawable.employee_login_white_dot);
                ivPIN4.setImageResource(R.drawable.employee_login_white_dot);
                break;
            case 2:
                ivPIN1.setImageResource(R.drawable.employee_login_blue_dot);
                ivPIN2.setImageResource(R.drawable.employee_login_blue_dot);
                ivPIN3.setImageResource(R.drawable.employee_login_white_dot);
                ivPIN4.setImageResource(R.drawable.employee_login_white_dot);
                break;
            case 3:
                ivPIN1.setImageResource(R.drawable.employee_login_blue_dot);
                ivPIN2.setImageResource(R.drawable.employee_login_blue_dot);
                ivPIN3.setImageResource(R.drawable.employee_login_blue_dot);
                ivPIN4.setImageResource(R.drawable.employee_login_white_dot);
                break;
            case 4:
                ivPIN1.setImageResource(R.drawable.employee_login_blue_dot);
                ivPIN2.setImageResource(R.drawable.employee_login_blue_dot);
                ivPIN3.setImageResource(R.drawable.employee_login_blue_dot);
                ivPIN4.setImageResource(R.drawable.employee_login_blue_dot);
                break;
        }

        return true;
    }

    private boolean EmployeeAttendance(String employee_pin, String clock_time, String clock_mode) {
        SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
        Cursor c = db.rawQuery("SELECT * FROM EmployeeAttendance_v1 WHERE employee_pin='" + employee_pin + "'", null);
        String out_time = "";
        int id = -1;
        boolean first_time = false;
        if (c.moveToFirst()) {
            out_time = c.getString(c.getColumnIndex("out_time"));
            id = c.getInt(c.getColumnIndex("Id"));

            while (c.moveToNext()) {
                out_time = c.getString(c.getColumnIndex("out_time"));
                id = c.getInt(c.getColumnIndex("Id"));
            }
        } else {
            first_time = true;
        }
        c.close();

        if (clock_mode.equals("In")) {
            if (first_time || !out_time.equals("")) {
                db.execSQL("INSERT INTO EmployeeAttendance_v1 (employee_pin, in_time, out_time) VALUES ('" + employee_pin + "', " +
                        "'" + clock_time + "', '');");
                return true;
            }
        } else if (clock_mode.equals("Out")) {
            if (!first_time && out_time.equals("")) {
                db.execSQL("UPDATE EmployeeAttendance_v1 SET out_time='" + clock_time + "' WHERE Id=" + id);
                return true;
            }
        }

        db.close();
        return false;
    }

}
