package com.restaware.mightyegg.restawarepos;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class RestawareLoginPage extends AppCompatActivity implements View.OnTouchListener {

    String restaurant_name, restaurant_password;
    boolean remember_me;

    EditText etRestaurantName, etRestaurantPassword;
    ImageButton ibRestaurantLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaware_login_page);

        ibRestaurantLogin = (ImageButton) findViewById(R.id.ibRestaurantLoginButton);
        ibRestaurantLogin.setImageResource(R.drawable.restaware_login_submit);
        ibRestaurantLogin.setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(v.getId() == R.id.ibRestaurantLoginButton) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    ibRestaurantLogin.setImageResource(R.drawable.restaurant_login_submit_clicked);

                    etRestaurantName = (EditText) findViewById(R.id.etRestaurantName);
                    restaurant_name = etRestaurantName.getText().toString();

                    etRestaurantPassword = (EditText) findViewById(R.id.etRestaurantPassword);
                    restaurant_password = etRestaurantPassword.getText().toString();

                    Log.d("RESTAURANT CREDENTIALS", "Restaurant Credentials: " + restaurant_password + " " + restaurant_name);

                    if (isNetworkAvailable()) {
                        /////////////send data to server for checking////////////////////
                        SharedPreferences pref = getSharedPreferences("Restaurant_Login.conf", Context.MODE_PRIVATE);

                        new checkRestaurantCredentials().execute(pref.getString("api", "") + "user_authentication.json?email=" + restaurant_name + "&password=" + restaurant_password);
                        /////////////////////////////////////////////////////////////////

                        //////////////////demo user/////////////////////
//                        if(restaurant_name.equals("Tauseef") && restaurant_password.equals("1234")) {
//                            CheckBox cbRememberOnDevice = (CheckBox) findViewById(R.id.cbRememberOnDevice);
//                            remember_me = cbRememberOnDevice.isChecked();
//
//                            SharedPreferences pref = getSharedPreferences("Restaurant_Login.conf", Context.MODE_PRIVATE);
//                            SharedPreferences.Editor editor = pref.edit();
//
//                            if(remember_me) {
//                                editor.putString("restaurant_name", restaurant_name);
//                                editor.putString("restaurant_password", restaurant_password);
//                                editor.apply();
//                            }
//                            else {
//                                editor.putString("restaurant_name", "");
//                                editor.putString("restaurant_password", "");
//                                editor.apply();
//                            }
//
//                            Toast.makeText(getApplicationContext(), "Login Successful", Toast.LENGTH_SHORT).show();
//
//                            Intent intent = new Intent(RestawareLoginPage.this, EmployeeLoginPage.class);
//                            startActivity(intent);
//                            RestawareLoginPage.this.finish();
//                        }
//                        else {
//                            Toast.makeText(getApplicationContext(), "Login Failed", Toast.LENGTH_SHORT).show();
//
//                            Log.d("RESTAURANT CREDENTIALS", "Restaurant Credentials: " + restaurant_password + " " + restaurant_name);
//
//                            etRestaurantName.setText("");
//                            etRestaurantPassword.setText("");
//                        }
                        ////////////////////////////////////////////////
                    } else {
                        Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                    }

                    break;
                case MotionEvent.ACTION_UP:
                    ibRestaurantLogin.setImageResource(R.drawable.restaware_login_submit);
                    break;
            }
        }
        return true;
    }

    class checkRestaurantCredentials extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String result = "";

            try {
                Log.d("URL", params[0]);
                URL url = new URL(params[0]);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                //////////////Additional for POST call////////////////
//                conn.setReadTimeout(20000);
//                conn.setConnectTimeout(30000);

//                conn.setDoInput(true);
//                conn.setDoOutput(true);

//                Uri.Builder builder = new Uri.Builder().appendQueryParameter("restaurant_name", restaurant_name)
//                        .appendQueryParameter("restaurant_password", restaurant_password);
//
//                String query = builder.build().getEncodedQuery();
//
//                OutputStream os = conn.getOutputStream();
//                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
//                writer.write(query);
//                writer.flush();
//                writer.close();
//                os.close();

//                conn.connect();
                ///////////////////////////////////////////////////////

                int responseCode = conn.getResponseCode();

                Log.d("RESPONSE CODE", String.valueOf(responseCode));

                if(responseCode == HttpsURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while((line = reader.readLine()) != null) {
                        result += line;
                    }
                }
                else {
                    result = "";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d("RESPONSE", s);

            ////////////////JSON Parsing////////////////////
            if (!s.isEmpty()) {
                try {
                    //JSONArray jsonArray = new JSONArray(s);
                    JSONObject jsonObject = new JSONObject(s);

                    boolean success = jsonObject.getBoolean("success");
                    JSONObject result = jsonObject.getJSONObject("result");
                    String authentication_code = result.getString("authentication_token");

                    Log.d("RECEIVED VALUES", success + " " + authentication_code);

                    if (success) {
                        CheckBox cbRememberOnDevice = (CheckBox) findViewById(R.id.cbRememberOnDevice);
                        remember_me = cbRememberOnDevice.isChecked();

                        SharedPreferences pref = getSharedPreferences("Restaurant_Login.conf", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();

                        if(remember_me) {
                            editor.putString("restaurant_name", restaurant_name);
                            editor.putString("restaurant_password", restaurant_password);
                            editor.putString("authentication_token", authentication_code);
                            editor.apply();
                        }
                        else {
                            editor.putString("restaurant_name", "");
                            editor.putString("restaurant_password", "");
                            editor.putString("authentication_token", authentication_code);
                            editor.apply();
                        }

                        Toast.makeText(getApplicationContext(), "Login Successful", Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(RestawareLoginPage.this, EmployeeLoginPage.class);
                        startActivity(intent);
                        RestawareLoginPage.this.finish();
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Login Failed", Toast.LENGTH_SHORT).show();

                        Log.d("RESTAURANT CREDENTIALS", "Restaurant Credentials: " + restaurant_password + " " + restaurant_name);

                        etRestaurantName.setText("");
                        etRestaurantPassword.setText("");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Invalid Credentials", Toast.LENGTH_SHORT).show();
            }
            ////////////////////////////////////////////////
        }
    }

    private boolean isNetworkAvailable(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetInfo != null && activeNetInfo.isConnected();
    }
}
