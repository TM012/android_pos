package com.restaware.mightyegg.restawarepos;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class TableLayout extends AppCompatActivity {

    public static String selectedFloor;

    Context context;
    String selectedTableType;

    ViewGroup tableLayout;
    ImageView iv;
    RelativeLayout.LayoutParams layoutParams;
    ImageButton ibAddObject, ibSetObject;
    int[] location;

    String[] tableTypes = {"Rectangle", "Round"};
    String[] floors;

    private int _xDelta, _yDelta;
    private int maxWidth, maxHeight;

    int[] XCoordinates, YCoordinates;
    String[] assignedTableTypes, number;
    int tables;

    String redirectTo;
    String PIN;

    ImageButton ibButton1, ibButton2, ibButton3, ibButton4, ibButton5, ibButton6, ibButton7, ibButton8, ibButton9, ibButton0, ibClear, ibLogin;
    ImageView ivPIN1, ivPIN2, ivPIN3, ivPIN4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_table_layout);
        setSupportActionBar(toolbar);

        TextView tv_floor_name = (TextView) toolbar.findViewById(R.id.tv_floor_name);
        if (selectedFloor.isEmpty()) {
            tv_floor_name.setText("None");
        } else {
            tv_floor_name.setText(selectedFloor);
        }

        ImageButton ib_floor = (ImageButton) toolbar.findViewById(R.id.ib_floor);
        ib_floor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //////////////////Fetch Tables from Database////////////////
                SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
                String querySetUp = "SELECT * FROM FloorTable_v1";
                Cursor c = db.rawQuery(querySetUp, null);
                floors = new String[c.getCount() + 1];
                int i = 0;
                if (c.moveToFirst()) {
                    floors[i] = c.getString(c.getColumnIndex("name"));
                    i++;
                    while (c.moveToNext()) {
                        floors[i] = c.getString(c.getColumnIndex("name"));
                        i++;
                    }
                }
                floors[i] = "Create Floor";
                c.close();
                db.close();

                Log.d("NUMBER OF FLOORS", String.valueOf(c.getCount()));
                ////////////////////////////////////////////////////////////

                //////////////////Dialog to Select Table//////////////////////
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Select Floor")
                        .setItems(floors, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String selection = floors[which];
                                if (!selection.equals("Create Floor")) {
                                    selectedFloor = selection;
                                    Intent intent = new Intent(TableLayout.this, TableLayout.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                    startActivity(intent);
                                    TableLayout.this.overridePendingTransition(0, 0);
                                    TableLayout.this.finish();
                                }
                                else {
                                    // custom dialog
                                    final Dialog createFloorDialog = new Dialog(context);
                                    createFloorDialog.setContentView(R.layout.create_floor_dialog_layout);
                                    createFloorDialog.setTitle("Create Floor");

                                    final EditText etTableNumber = (EditText) createFloorDialog.findViewById(R.id.etDialogFloorName);

                                    Button dialogButton = (Button) createFloorDialog.findViewById(R.id.buttonCreateFloorDialog);

                                    dialogButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            String floor_name;
                                            floor_name = etTableNumber.getText().toString();

                                            if (!floor_name.isEmpty()) {
                                                SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);

                                                db.execSQL("INSERT INTO FloorTable_v1 (name) VALUES ('" + floor_name + "');");

                                                db.close();

                                                selectedFloor = floor_name;

                                                createFloorDialog.cancel();

                                                Intent intent = new Intent(TableLayout.this, TableLayout.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                                startActivity(intent);
                                                TableLayout.this.overridePendingTransition(0, 0);
                                                TableLayout.this.finish();
                                            }
                                            else {
                                                Toast.makeText(getApplicationContext(), "Fields Empty", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });

                                    createFloorDialog.show();
                                }
                            }
                        });
                builder.create();
                builder.show();
                //////////////////////////////////////////////////////////////
            }
        });

        //copy for clock in/out
        ImageButton ib_clock_in_out = (ImageButton) findViewById(R.id.ib_clock_in_out);
        ib_clock_in_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PIN = "";
                redirectTo = "";

                //default dialog for clock in/out
                final String[] clock_options = {"Clock In", "Clock Out"};
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Select Clock Options")
                        .setItems(clock_options, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                redirectTo = clock_options[which];

                                // custom dialog
                                final Dialog clockDialog = new Dialog(context);
                                clockDialog.setContentView(R.layout.clock_in_out_dialog_layout);
                                clockDialog.setTitle("Provide Details");

                                ibButton1 = (ImageButton) clockDialog.findViewById(R.id.ibButton1);
                                ibButton2 = (ImageButton) clockDialog.findViewById(R.id.ibButton2);
                                ibButton3 = (ImageButton) clockDialog.findViewById(R.id.ibButton3);
                                ibButton4 = (ImageButton) clockDialog.findViewById(R.id.ibButton4);
                                ibButton5 = (ImageButton) clockDialog.findViewById(R.id.ibButton5);
                                ibButton6 = (ImageButton) clockDialog.findViewById(R.id.ibButton6);
                                ibButton7 = (ImageButton) clockDialog.findViewById(R.id.ibButton7);
                                ibButton8 = (ImageButton) clockDialog.findViewById(R.id.ibButton8);
                                ibButton9 = (ImageButton) clockDialog.findViewById(R.id.ibButton9);
                                ibButton0 = (ImageButton) clockDialog.findViewById(R.id.ibButton0);

                                ibButton1.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton2.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton3.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton4.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton5.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton6.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton7.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton8.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton9.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton0.setOnTouchListener(new EmployeePINOnTouchListener());

                                ibClear = (ImageButton) clockDialog.findViewById(R.id.ibClear);
                                ibClear.setOnTouchListener(new EmployeePINOnTouchListener());

                                ibLogin = (ImageButton) clockDialog.findViewById(R.id.ibLogin);
                                ibLogin.setOnTouchListener(new EmployeePINOnTouchListener());

                                ivPIN1 = (ImageView) clockDialog.findViewById(R.id.ivPIN1);
                                ivPIN2 = (ImageView) clockDialog.findViewById(R.id.ivPIN2);
                                ivPIN3 = (ImageView) clockDialog.findViewById(R.id.ivPIN3);
                                ivPIN4 = (ImageView) clockDialog.findViewById(R.id.ivPIN4);

                                clockDialog.show();
                            }
                        });
                builder.create();
                builder.show();
            }
        });

        context = this;
        location = new int[2];

        tableLayout = (ViewGroup) findViewById(R.id.layoutTableLayout);
        layoutParams = new RelativeLayout.LayoutParams(200, 200);

        ViewTreeObserver vto = tableLayout.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                tableLayout.getLocationOnScreen(location);
                tableLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                maxWidth = location[0];
                maxHeight = location[1];

                Log.d("MAX BOUNDS", tableLayout.getLeft() + " " + tableLayout.getTop());
                Log.d("LOCATION", maxWidth + " " + maxHeight);
            }
        });

        if (!selectedFloor.isEmpty()) {
            ////////////////////////Fetch Existing Table from Database///////////////////////////////
            SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);

            String querySetUp = "SELECT * FROM TableArrangement_v1 WHERE floor='" + selectedFloor + "'";
            Cursor c = db.rawQuery(querySetUp, null);

            tables = c.getCount();
            XCoordinates = new int[50];
            YCoordinates = new int[50];
            assignedTableTypes = new String[50];
            number = new String[50];

            int i = 0;
            if (c.moveToFirst()) {
                XCoordinates[i] = Integer.parseInt(c.getString(c.getColumnIndex("xCoordinate")));
                YCoordinates[i] = Integer.parseInt(c.getString(c.getColumnIndex("yCoordinate")));
                assignedTableTypes[i] = c.getString(c.getColumnIndex("type"));
                number[i] = c.getString(c.getColumnIndex("number"));
                i++;

                while (c.moveToNext()) {
                    XCoordinates[i] = Integer.parseInt(c.getString(c.getColumnIndex("xCoordinate")));
                    YCoordinates[i] = Integer.parseInt(c.getString(c.getColumnIndex("yCoordinate")));
                    assignedTableTypes[i] = c.getString(c.getColumnIndex("type"));
                    number[i] = c.getString(c.getColumnIndex("number"));
                    i++;
                }
            }

            c.close();
            db.close();
            /////////////////////////////////////////////////////////////////////////////////////////

            ///////////////////////////Populate With Existing Tables/////////////////////////////////
            ImageView iv;
            TextView tv;

            for (i = 0; i < tables; i++) {
                iv = new ImageView(context);
                tv = new TextView(context);

                iv.setBackground(null);
                tv.setText(number[i]);
                tv.setTextColor(Color.WHITE);
                tv.setTextSize(20);

                if (assignedTableTypes[i].equals("Rectangle")) {
                    //layoutParams = new RelativeLayout.LayoutParams(180, 94);
                    iv.setImageResource(R.drawable.table_layout_rectangle_blue);
                }
                else if (assignedTableTypes[i].equals("Round")) {
                    //layoutParams = new RelativeLayout.LayoutParams(180, 171);
                    iv.setImageResource(R.drawable.table_layout_ellipse_blue);
                }
                iv.setLayoutParams(layoutParams);
                iv.setScaleType(ImageView.ScaleType.FIT_XY);

                tableLayout.addView(iv);
                tableLayout.addView(tv);
                iv.setX((float) XCoordinates[i]);
                iv.setY((float) YCoordinates[i]);
                tv.setX((float) XCoordinates[i] + 100);
                tv.setY((float) YCoordinates[i] + 100);
            }
            /////////////////////////////////////////////////////////////////////////////////////////
        }
        else {
            Toast.makeText(getApplicationContext(), "Please Select Floor First", Toast.LENGTH_SHORT).show();
        }

        ibAddObject = (ImageButton) findViewById(R.id.ibAddObject);
        ibAddObject.setOnClickListener(new AddObjectClickListener());

        ibSetObject = (ImageButton) findViewById(R.id.ibSetObject);
        ibSetObject.setOnClickListener(null);
        ibSetObject.setVisibility(View.GONE);

//        ImageButton ibViewTables = (ImageButton) findViewById(R.id.ibViewTables);
//        ibViewTables.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (selectedFloor.isEmpty()) {
//                    Toast.makeText(getApplicationContext(), "Please Select Floor First", Toast.LENGTH_SHORT).show();
//                } else {
//                    Intent intent = new Intent(TableLayout.this, ViewTables.class);
//                    startActivity(intent);
//                    TableLayout.this.finish();
//                }
//            }
//        });
    }

    private class AddObjectClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if (selectedFloor.isEmpty()) {
                Toast.makeText(getApplicationContext(), "Please Select Floor First", Toast.LENGTH_SHORT).show();
            } else {
                selectedTableType = "";

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Select type of table")
                        .setItems(tableTypes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                selectedTableType = tableTypes[which];
                                AddViewToLayout();
                            }
                        });
                builder.create();
                builder.show();
            }
        }
    }

    protected void AddViewToLayout() {
        iv = new ImageView(context);

        if(selectedTableType.equals("Rectangle")) {
            iv.setImageResource(R.drawable.table_layout_rectangle_blue);
        }
        else if (selectedTableType.equals("Round")) {
            iv.setImageResource(R.drawable.table_layout_ellipse_blue);
        }

        tableLayout.addView(iv);
        iv.setX(0);
        iv.setY(0);

        layoutParams = new RelativeLayout.LayoutParams(200, 200);
        iv.setLayoutParams(layoutParams);
        iv.setScaleType(ImageView.ScaleType.FIT_XY);
        iv.setOnTouchListener(new ChoiceTouchListener());

        ibAddObject.setOnClickListener(null);
        ibAddObject.setVisibility(View.GONE);
        ibSetObject.setVisibility(View.VISIBLE);
        ibSetObject.setOnClickListener(new SetObjectClickListener());
    }

    private class SetObjectClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            ibAddObject.setOnClickListener(new AddObjectClickListener());
            iv.setOnTouchListener(null);
            ibSetObject.setVisibility(View.GONE);
            ibAddObject.setVisibility(View.VISIBLE);

            //Log.d("POSITION", iv.getLeft() + " " + iv.getTop() );
            XCoordinates[tables] = iv.getLeft();
            YCoordinates[tables] = iv.getTop();
            assignedTableTypes[tables] = selectedTableType;
            Log.d("TABLE DETAILS", XCoordinates[tables] + " " + YCoordinates[tables] + " " + assignedTableTypes[tables]);

            // custom dialog
            final Dialog dialog = new Dialog(context);
            dialog.setContentView(R.layout.table_details_dialog_layout);
            dialog.setTitle("Provide Details");

            final EditText etTableNumber = (EditText) dialog.findViewById(R.id.etDialogTableNumber);
            final EditText etTableCapacity = (EditText) dialog.findViewById(R.id.etDialogTableCapacity);

            Button dialogButton = (Button) dialog.findViewById(R.id.buttonDialog);

            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String table_number, table_capacity;
                    table_number = etTableNumber.getText().toString();
                    table_capacity = etTableCapacity.getText().toString();

                    if (!table_number.isEmpty() && !table_capacity.isEmpty()) {
                        SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);

                        db.execSQL("INSERT INTO TableArrangement_v1 " +
                                "(type, number, xCoordinate, yCoordinate, capacity, floor, status) VALUES ('"
                                + assignedTableTypes[tables] + "', '" + table_number + "', '" + XCoordinates[tables] + "', '"
                                + YCoordinates[tables] + "', '" + table_capacity + "', '" + selectedFloor + "', 'vacant');");

                        db.close();

                        ////////////////Add Table Number to View///////////////////
                        TextView tv = new TextView(context);
                        tv.setText(table_number);
                        tv.setTextColor(Color.WHITE);
                        tv.setTextSize(20);
                        tableLayout.addView(tv);
                        tv.setX((float) XCoordinates[tables] + 100);
                        tv.setY((float) YCoordinates[tables] + 100);
                        ///////////////////////////////////////////////////////////

                        tables++;

                        dialog.cancel();
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Fields Empty", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            dialog.show();
        }
    }

    private class ChoiceTouchListener implements View.OnTouchListener {

        @Override
        public boolean onTouch(View view, MotionEvent event) {
            int X = (int) event.getRawX();
            int Y = (int) event.getRawY();

            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    RelativeLayout.LayoutParams lpForActionDown = (RelativeLayout.LayoutParams) view.getLayoutParams();
                    _xDelta = X - lpForActionDown.leftMargin;
                    _yDelta = Y - lpForActionDown.topMargin;
                    break;
                case MotionEvent.ACTION_UP:
                    //Log.d("POSITION", _xDelta + " " + _yDelta );
                    break;
                case MotionEvent.ACTION_POINTER_DOWN:
                    break;
                case MotionEvent.ACTION_POINTER_UP:
                    break;
                case MotionEvent.ACTION_MOVE:
                    RelativeLayout.LayoutParams lpForActionMove = (RelativeLayout.LayoutParams) view.getLayoutParams();
                    lpForActionMove.leftMargin = X - _xDelta;
                    lpForActionMove.topMargin = Y - _yDelta;
                    lpForActionMove.rightMargin = -250;
                    lpForActionMove.bottomMargin = -250;
                    view.setLayoutParams(lpForActionMove);
                    break;
            }
            tableLayout.invalidate();
            return true;
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(TableLayout.this, ViewTables.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        TableLayout.this.finish();
    }

    private class EmployeePINOnTouchListener implements View.OnTouchListener {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            Long tsLong = System.currentTimeMillis()/1000;
            String ts = tsLong.toString();

            if(v.getId() == R.id.ibButton1) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton1.setImageResource(R.drawable.employee_login_1_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "1";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton1.setImageResource(R.drawable.employee_login_button_1);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton2) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton2.setImageResource(R.drawable.employee_login_2_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "2";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton2.setImageResource(R.drawable.employee_login_button_2);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton3) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton3.setImageResource(R.drawable.employee_login_3_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "3";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton3.setImageResource(R.drawable.employee_login_button_3);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton4) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton4.setImageResource(R.drawable.employee_login_4_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "4";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton4.setImageResource(R.drawable.employee_login_button_4);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton5) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton5.setImageResource(R.drawable.employee_login_5_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "5";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton5.setImageResource(R.drawable.employee_login_button_5);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton6) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton6.setImageResource(R.drawable.employee_login_6_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "6";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton6.setImageResource(R.drawable.employee_login_button_6);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton7) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton7.setImageResource(R.drawable.employee_login_7_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "7";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton7.setImageResource(R.drawable.employee_login_button_7);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton8) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton8.setImageResource(R.drawable.employee_login_8_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "8";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton8.setImageResource(R.drawable.employee_login_button_8);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton9) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton9.setImageResource(R.drawable.employee_login_9_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "9";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton9.setImageResource(R.drawable.employee_login_button_9);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton0) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton0.setImageResource(R.drawable.employee_login_0_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "0";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton0.setImageResource(R.drawable.employee_login_button_0);
                        break;
                }
            }
            else if(v.getId() == R.id.ibClear) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibClear.setImageResource(R.drawable.employee_login_clear_clicked);
                        if(PIN.length() > 0) {
                            PIN = PIN.substring(0, PIN.length() - 1);
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibClear.setImageResource(R.drawable.employee_login_clear_button);
                        break;
                }
                Log.d("PIN", PIN);
            }
            else if(v.getId() == R.id.ibLogin) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        boolean return_type;
                        ibLogin.setImageResource(R.drawable.employee_login_login_clicked);
                        if(PIN.length() < 4) {
                            Toast.makeText(getApplicationContext(), "Incomplete PIN", Toast.LENGTH_SHORT).show();
                        }
                        else if(redirectTo.isEmpty()) {
                            Toast.makeText(getApplicationContext(), "Please Select an Option", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
                            Cursor c = db.rawQuery("SELECT * FROM user_tables WHERE employee_pin='" + PIN + "'", null);

                            if (c.moveToFirst()) {
                                if (redirectTo.equals("Clock In")) {
                                    return_type = EmployeeAttendance(PIN, ts, "In");

                                    if (return_type)
                                        Toast.makeText(getApplicationContext(), "Successfully Clocked In", Toast.LENGTH_SHORT).show();
                                    else
                                        Toast.makeText(getApplicationContext(), "Failed to Clock In", Toast.LENGTH_SHORT).show();
                                } else if (redirectTo.equals("Clock Out")) {
                                    return_type = EmployeeAttendance(PIN, ts, "Out");

                                    if (return_type)
                                        Toast.makeText(getApplicationContext(), "Successfully Clocked Out", Toast.LENGTH_SHORT).show();
                                    else
                                        Toast.makeText(getApplicationContext(), "Failed to Clock Out", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), "Invalid PIN", Toast.LENGTH_SHORT).show();
                            }

                            c.close();
                            db.close();
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibLogin.setImageResource(R.drawable.employee_login_login_button);
                        break;
                }
                Log.d("PIN", PIN);
            }

            int length = PIN.length();

            switch (length) {
                case 0:
                    ivPIN1.setImageResource(R.drawable.employee_login_white_dot);
                    ivPIN2.setImageResource(R.drawable.employee_login_white_dot);
                    ivPIN3.setImageResource(R.drawable.employee_login_white_dot);
                    ivPIN4.setImageResource(R.drawable.employee_login_white_dot);
                    break;
                case 1:
                    ivPIN1.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN2.setImageResource(R.drawable.employee_login_white_dot);
                    ivPIN3.setImageResource(R.drawable.employee_login_white_dot);
                    ivPIN4.setImageResource(R.drawable.employee_login_white_dot);
                    break;
                case 2:
                    ivPIN1.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN2.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN3.setImageResource(R.drawable.employee_login_white_dot);
                    ivPIN4.setImageResource(R.drawable.employee_login_white_dot);
                    break;
                case 3:
                    ivPIN1.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN2.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN3.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN4.setImageResource(R.drawable.employee_login_white_dot);
                    break;
                case 4:
                    ivPIN1.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN2.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN3.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN4.setImageResource(R.drawable.employee_login_blue_dot);
                    break;
            }

            return true;
        }
    }

    private boolean EmployeeAttendance(String employee_pin, String clock_time, String clock_mode) {
        SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
        Cursor c = db.rawQuery("SELECT * FROM EmployeeAttendance_v1 WHERE employee_pin='" + employee_pin + "'", null);
        String out_time = "";
        int id = -1;
        boolean first_time = false;
        if (c.moveToFirst()) {
            out_time = c.getString(c.getColumnIndex("out_time"));
            id = c.getInt(c.getColumnIndex("Id"));

            while (c.moveToNext()) {
                out_time = c.getString(c.getColumnIndex("out_time"));
                id = c.getInt(c.getColumnIndex("Id"));
            }
        } else {
            first_time = true;
        }
        c.close();

        if (clock_mode.equals("In")) {
            if (first_time || !out_time.equals("")) {
                db.execSQL("INSERT INTO EmployeeAttendance_v1 (employee_pin, in_time, out_time) VALUES ('" + employee_pin + "', " +
                        "'" + clock_time + "', '');");
                return true;
            }
        } else if (clock_mode.equals("Out")) {
            if (!first_time && out_time.equals("")) {
                db.execSQL("UPDATE EmployeeAttendance_v1 SET out_time='" + clock_time + "' WHERE Id=" + id);
                return true;
            }
        }

        db.close();
        return false;
    }
}
