package com.restaware.mightyegg.restawarepos;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

public class MenuListPage extends AppCompatActivity {

    Context context;

    String[] category_name, item_name, item_price, item_desc, item_category;
    String[] modification, extra_price;
    String[] variation;

    SQLiteDatabase db;
    Cursor c;

    String redirectTo;
    String PIN;

    ImageButton ibButton1, ibButton2, ibButton3, ibButton4, ibButton5, ibButton6, ibButton7, ibButton8, ibButton9, ibButton0, ibClear, ibLogin;
    ImageView ivPIN1, ivPIN2, ivPIN3, ivPIN4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_list_page);

        context = this;

        InitializeToolbar();
        PopulateMenuList();
        PopulateList();
    }

    private void PopulateList() {
        ListView lv_menu_list = (ListView) findViewById(R.id.lv_menu_list);

        if (item_name != null && item_name.length > 0) {
            MenuListAdapter menuListAdapter = new MenuListAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, R.id.tv_menu_item, item_name);
            lv_menu_list.setAdapter(menuListAdapter);

            lv_menu_list.setOnItemClickListener(new MenuListItemClickListener());
        }
    }

    private class MenuListItemClickListener implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Log.d("CLICKED ITEM", item_name[position] + " " + item_category[position]);
            db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
            c = db.rawQuery("SELECT Id FROM MenuItems_v1 WHERE item_name='" + item_name[position] + "' AND " +
                    "item_category='" + item_category[position] + "'", null);

            int fetched_id = -1;
            if (c.moveToFirst()) {
                fetched_id = c.getInt(c.getColumnIndex("Id"));
            }

            if (fetched_id != -1) {
                //TODO open dialog
                PopulateModifierVariation(fetched_id);

                // custom dialog
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.menu_customization_options_dialog_layout);
                dialog.setTitle("Create Floor");

                ListView modifier_list = (ListView) dialog.findViewById(R.id.lv_modifier_list);
                ListView variation_list = (ListView) dialog.findViewById(R.id.lv_variation_list);

                if (modification != null && modification.length > 0) {
                    ModifierListAdapter modifierListAdapter = new ModifierListAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, R.id.tv_menu_item, modification);
                    modifier_list.setAdapter(modifierListAdapter);
                }

                if (variation != null && variation.length > 0) {
                    VariationListAdapter variationListAdapter = new VariationListAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, R.id.tv_menu_item, variation);
                    variation_list.setAdapter(variationListAdapter);
                }

                dialog.show();
            }

            db.close();
        }
    }

    private void PopulateModifierVariation(int id) {
        db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
        c = db.rawQuery("SELECT * FROM ModifiersForMenuItems_v1 WHERE item_id='" + id + "'", null);
        int i;

        if (c.moveToFirst()) {
            modification = new String[c.getCount()];
            extra_price = new String[c.getCount()];
            i = 0;

            modification[i] = c.getString(c.getColumnIndex("modification"));
            extra_price[i] = c.getString(c.getColumnIndex("extra_price"));
            i++;

            while (c.moveToNext()) {
                modification[i] = c.getString(c.getColumnIndex("modification"));
                extra_price[i] = c.getString(c.getColumnIndex("extra_price"));
                i++;
            }
        } else {
            modification = null;
            extra_price = null;
        }

        c = db.rawQuery("SELECT * FROM VariationsForMenuItems_v1 WHERE item_id='" + id + "'", null);

        if (c.moveToFirst()) {
            variation = new String[c.getCount()];
            i = 0;

            variation[i] = c.getString(c.getColumnIndex("variation"));
            i++;

            while (c.moveToNext()) {
                variation[i] = c.getString(c.getColumnIndex("variation"));
                i++;
            }
        } else {
            variation = null;
        }
    }

    private class ModifierListAdapter extends ArrayAdapter<String> {

        public ModifierListAdapter(Context context, int resource, int textViewResourceId, String[] objects) {
            super(context, resource, textViewResourceId, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.modifier_variation_list_item, parent, false);

            RelativeLayout layout_menu_category = (RelativeLayout) row.findViewById(R.id.layout_menu_category);
            TextView tv_menu_category = (TextView) row.findViewById(R.id.tv_menu_category);
            TextView tv_menu_item = (TextView) row.findViewById(R.id.tv_menu_item);
            TextView tv_menu_price = (TextView) row.findViewById(R.id.tv_menu_price);

            DecimalFormat df = new DecimalFormat("0.00");

            layout_menu_category.setVisibility(View.GONE);
            tv_menu_item.setText(modification[position]);
            tv_menu_price.setText("$ " + df.format(Double.parseDouble(extra_price[position])));

            return row;
        }
    }

    private class VariationListAdapter extends ArrayAdapter<String> {

        public VariationListAdapter(Context context, int resource, int textViewResourceId, String[] objects) {
            super(context, resource, textViewResourceId, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.modifier_variation_list_item, parent, false);

            RelativeLayout layout_menu_category = (RelativeLayout) row.findViewById(R.id.layout_menu_category);
            TextView tv_menu_category = (TextView) row.findViewById(R.id.tv_menu_category);
            TextView tv_menu_item = (TextView) row.findViewById(R.id.tv_menu_item);
            TextView tv_menu_price = (TextView) row.findViewById(R.id.tv_menu_price);

            layout_menu_category.setVisibility(View.GONE);
            tv_menu_item.setText(variation[position]);
            tv_menu_price.setVisibility(View.GONE);

            return row;
        }
    }

    private class MenuListAdapter extends ArrayAdapter<String> {

        public MenuListAdapter(Context context, int resource, int textViewResourceId, String[] objects) {
            super(context, resource, textViewResourceId, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.menu_list_item, parent, false);

            RelativeLayout layout_menu_category = (RelativeLayout) row.findViewById(R.id.layout_menu_category);
            TextView tv_menu_category = (TextView) row.findViewById(R.id.tv_menu_category);
            TextView tv_menu_item = (TextView) row.findViewById(R.id.tv_menu_item);
            TextView tv_menu_price = (TextView) row.findViewById(R.id.tv_menu_price);

            DecimalFormat df = new DecimalFormat("0.00");

            if (position > 0) {
                if (item_category[position].equals(item_category[position - 1])) {
                    layout_menu_category.setVisibility(View.GONE);
                } else {
                    tv_menu_category.setText(item_category[position]);
                }

                tv_menu_item.setText(item_name[position]);
                tv_menu_price.setText("$ " + df.format(Double.parseDouble(item_price[position])));
            } else if (position == 0) {
                tv_menu_category.setText(item_category[position]);
                tv_menu_item.setText(item_name[position]);
                tv_menu_price.setText("$ " + df.format(Double.parseDouble(item_price[position])));
            }

            return row;
        }
    }

    private void PopulateMenuList() {
        int i;

        db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
        c = db.rawQuery("SELECT * FROM MenuCategories_v1", null);

        if (c.moveToFirst()) {
            category_name = new String[c.getCount()];
            i = 0;

            category_name[i] = c.getString(c.getColumnIndex("category_name"));
            i++;

            while (c.moveToNext()) {
                category_name[i] = c.getString(c.getColumnIndex("category_name"));
                i++;
            }
        }

        c = db.rawQuery("SELECT * FROM MenuItems_v1 ORDER BY item_category", null);

        if (c.moveToFirst()) {
            item_name = new String[c.getCount()];
            item_price = new String[c.getCount()];
            item_desc = new String[c.getCount()];
            item_category = new String[c.getCount()];
            i = 0;

            item_name[i] = c.getString(c.getColumnIndex("item_name"));
            item_price[i] = c.getString(c.getColumnIndex("item_price"));
            item_desc[i] = c.getString(c.getColumnIndex("item_desc"));
            item_category[i] = c.getString(c.getColumnIndex("item_category"));
            i++;

            while (c.moveToNext()) {
                item_name[i] = c.getString(c.getColumnIndex("item_name"));
                item_price[i] = c.getString(c.getColumnIndex("item_price"));
                item_desc[i] = c.getString(c.getColumnIndex("item_desc"));
                item_category[i] = c.getString(c.getColumnIndex("item_category"));
                i++;
            }
        }

        c.close();
        db.close();
    }

    private void InitializeToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_tab_history);
        setSupportActionBar(toolbar);

        ImageButton ib_tab = (ImageButton) findViewById(R.id.ib_tab);
        ib_tab.setOnClickListener(new TabOptions());

        //copy for clock in/out
        ImageButton ib_clock_in_out = (ImageButton) findViewById(R.id.ib_clock_in_out);
        ib_clock_in_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PIN = "";
                redirectTo = "";

                //default dialog for clock in/out
                final String[] clock_options = {"Clock In", "Clock Out"};
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Select Clock Options")
                        .setItems(clock_options, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                redirectTo = clock_options[which];

                                // custom dialog
                                final Dialog clockDialog = new Dialog(context);
                                clockDialog.setContentView(R.layout.clock_in_out_dialog_layout);
                                clockDialog.setTitle("Provide Details");

                                ibButton1 = (ImageButton) clockDialog.findViewById(R.id.ibButton1);
                                ibButton2 = (ImageButton) clockDialog.findViewById(R.id.ibButton2);
                                ibButton3 = (ImageButton) clockDialog.findViewById(R.id.ibButton3);
                                ibButton4 = (ImageButton) clockDialog.findViewById(R.id.ibButton4);
                                ibButton5 = (ImageButton) clockDialog.findViewById(R.id.ibButton5);
                                ibButton6 = (ImageButton) clockDialog.findViewById(R.id.ibButton6);
                                ibButton7 = (ImageButton) clockDialog.findViewById(R.id.ibButton7);
                                ibButton8 = (ImageButton) clockDialog.findViewById(R.id.ibButton8);
                                ibButton9 = (ImageButton) clockDialog.findViewById(R.id.ibButton9);
                                ibButton0 = (ImageButton) clockDialog.findViewById(R.id.ibButton0);

                                ibButton1.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton2.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton3.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton4.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton5.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton6.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton7.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton8.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton9.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton0.setOnTouchListener(new EmployeePINOnTouchListener());

                                ibClear = (ImageButton) clockDialog.findViewById(R.id.ibClear);
                                ibClear.setOnTouchListener(new EmployeePINOnTouchListener());

                                ibLogin = (ImageButton) clockDialog.findViewById(R.id.ibLogin);
                                ibLogin.setOnTouchListener(new EmployeePINOnTouchListener());

                                ivPIN1 = (ImageView) clockDialog.findViewById(R.id.ivPIN1);
                                ivPIN2 = (ImageView) clockDialog.findViewById(R.id.ivPIN2);
                                ivPIN3 = (ImageView) clockDialog.findViewById(R.id.ivPIN3);
                                ivPIN4 = (ImageView) clockDialog.findViewById(R.id.ivPIN4);

                                clockDialog.show();
                            }
                        });
                builder.create();
                builder.show();
            }
        });
    }

    private class EmployeePINOnTouchListener implements View.OnTouchListener {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            Long tsLong = System.currentTimeMillis()/1000;
            String ts = tsLong.toString();

            if(v.getId() == R.id.ibButton1) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton1.setImageResource(R.drawable.employee_login_1_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "1";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton1.setImageResource(R.drawable.employee_login_button_1);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton2) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton2.setImageResource(R.drawable.employee_login_2_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "2";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton2.setImageResource(R.drawable.employee_login_button_2);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton3) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton3.setImageResource(R.drawable.employee_login_3_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "3";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton3.setImageResource(R.drawable.employee_login_button_3);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton4) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton4.setImageResource(R.drawable.employee_login_4_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "4";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton4.setImageResource(R.drawable.employee_login_button_4);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton5) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton5.setImageResource(R.drawable.employee_login_5_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "5";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton5.setImageResource(R.drawable.employee_login_button_5);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton6) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton6.setImageResource(R.drawable.employee_login_6_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "6";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton6.setImageResource(R.drawable.employee_login_button_6);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton7) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton7.setImageResource(R.drawable.employee_login_7_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "7";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton7.setImageResource(R.drawable.employee_login_button_7);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton8) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton8.setImageResource(R.drawable.employee_login_8_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "8";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton8.setImageResource(R.drawable.employee_login_button_8);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton9) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton9.setImageResource(R.drawable.employee_login_9_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "9";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton9.setImageResource(R.drawable.employee_login_button_9);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton0) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton0.setImageResource(R.drawable.employee_login_0_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "0";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton0.setImageResource(R.drawable.employee_login_button_0);
                        break;
                }
            }
            else if(v.getId() == R.id.ibClear) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibClear.setImageResource(R.drawable.employee_login_clear_clicked);
                        if(PIN.length() > 0) {
                            PIN = PIN.substring(0, PIN.length() - 1);
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibClear.setImageResource(R.drawable.employee_login_clear_button);
                        break;
                }
                Log.d("PIN", PIN);
            }
            else if(v.getId() == R.id.ibLogin) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        boolean return_type;
                        ibLogin.setImageResource(R.drawable.employee_login_login_clicked);
                        if(PIN.length() < 4) {
                            Toast.makeText(getApplicationContext(), "Incomplete PIN", Toast.LENGTH_SHORT).show();
                        }
                        else if(redirectTo.isEmpty()) {
                            Toast.makeText(getApplicationContext(), "Please Select an Option", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
                            Cursor c = db.rawQuery("SELECT * FROM user_tables WHERE employee_pin='" + PIN + "'", null);

                            if (c.moveToFirst()) {
                                if (redirectTo.equals("Clock In")) {
                                    return_type = EmployeeAttendance(PIN, ts, "In");

                                    if (return_type)
                                        Toast.makeText(getApplicationContext(), "Successfully Clocked In", Toast.LENGTH_SHORT).show();
                                    else
                                        Toast.makeText(getApplicationContext(), "Failed to Clock In", Toast.LENGTH_SHORT).show();
                                } else if (redirectTo.equals("Clock Out")) {
                                    return_type = EmployeeAttendance(PIN, ts, "Out");

                                    if (return_type)
                                        Toast.makeText(getApplicationContext(), "Successfully Clocked Out", Toast.LENGTH_SHORT).show();
                                    else
                                        Toast.makeText(getApplicationContext(), "Failed to Clock Out", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), "Invalid PIN", Toast.LENGTH_SHORT).show();
                            }

                            c.close();
                            db.close();
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibLogin.setImageResource(R.drawable.employee_login_login_button);
                        break;
                }
                Log.d("PIN", PIN);
            }

            int length = PIN.length();

            switch (length) {
                case 0:
                    ivPIN1.setImageResource(R.drawable.employee_login_white_dot);
                    ivPIN2.setImageResource(R.drawable.employee_login_white_dot);
                    ivPIN3.setImageResource(R.drawable.employee_login_white_dot);
                    ivPIN4.setImageResource(R.drawable.employee_login_white_dot);
                    break;
                case 1:
                    ivPIN1.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN2.setImageResource(R.drawable.employee_login_white_dot);
                    ivPIN3.setImageResource(R.drawable.employee_login_white_dot);
                    ivPIN4.setImageResource(R.drawable.employee_login_white_dot);
                    break;
                case 2:
                    ivPIN1.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN2.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN3.setImageResource(R.drawable.employee_login_white_dot);
                    ivPIN4.setImageResource(R.drawable.employee_login_white_dot);
                    break;
                case 3:
                    ivPIN1.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN2.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN3.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN4.setImageResource(R.drawable.employee_login_white_dot);
                    break;
                case 4:
                    ivPIN1.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN2.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN3.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN4.setImageResource(R.drawable.employee_login_blue_dot);
                    break;
            }

            return true;
        }
    }

    private boolean EmployeeAttendance(String employee_pin, String clock_time, String clock_mode) {
        SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
        Cursor c = db.rawQuery("SELECT * FROM EmployeeAttendance_v1 WHERE employee_pin='" + employee_pin + "'", null);
        String out_time = "";
        int id = -1;
        boolean first_time = false;
        if (c.moveToFirst()) {
            out_time = c.getString(c.getColumnIndex("out_time"));
            id = c.getInt(c.getColumnIndex("Id"));

            while (c.moveToNext()) {
                out_time = c.getString(c.getColumnIndex("out_time"));
                id = c.getInt(c.getColumnIndex("Id"));
            }
        } else {
            first_time = true;
        }
        c.close();

        if (clock_mode.equals("In")) {
            if (first_time || !out_time.equals("")) {
                db.execSQL("INSERT INTO EmployeeAttendance_v1 (employee_pin, in_time, out_time) VALUES ('" + employee_pin + "', " +
                        "'" + clock_time + "', '');");
                return true;
            }
        } else if (clock_mode.equals("Out")) {
            if (!first_time && out_time.equals("")) {
                db.execSQL("UPDATE EmployeeAttendance_v1 SET out_time='" + clock_time + "' WHERE Id=" + id);
                return true;
            }
        }

        db.close();
        return false;
    }

    private class TabOptions implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            final String[] tab_options = {"Create Tab", "Tab List", "Close Tab"};
            //////////////////Default Dialog to Select Tab Options//////////////////////
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Select Tab Options")
                    .setItems(tab_options, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Log.d("SELECTED TAB OPTIONS", tab_options[which]);

                            if (tab_options[which].equals("Create Tab")) {
                                //TODO open custom dialog
                                final Dialog create_tab_dialog = new Dialog(context);
                                create_tab_dialog.setContentView(R.layout.create_tab_dialog_layout);

                                final EditText et_tab_customer_name = (EditText) create_tab_dialog.findViewById(R.id.et_tab_customer_name);
                                final EditText et_tab_card_number = (EditText) create_tab_dialog.findViewById(R.id.et_tab_card_number);

                                Button button_tab_confirm = (Button) create_tab_dialog.findViewById(R.id.button_tab_confirm);
                                button_tab_confirm.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        String customer_name = et_tab_customer_name.getText().toString();
                                        String card_number = et_tab_card_number.getText().toString();

                                        if (!customer_name.isEmpty()) {
                                            db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
                                            Long tsLong = System.currentTimeMillis()/1000;
                                            String ts = tsLong.toString();

                                            //Insert Customer Details for CustomerPerTable
                                            db.execSQL("INSERT INTO CustomerPerTable_v1 (table_name, customer, time_stamp, floor_name) VALUES (" +
                                                    "'" + customer_name + "', '1', '" + ts + "', 'Tab');");

                                            //Insert Discount Details for BillSupplementary
                                            db.execSQL("INSERT INTO BillSupplementary_v1 (order_number, discount, discount_type) VALUES (" +
                                                    "'" + ts + "', '0', '');");

                                            //Insert Print Details for PrintOptions
                                            db.execSQL("INSERT INTO PrintOptions_v1 (order_number, print_options, table_number, combination, " +
                                                    "payment_status) VALUES (" +
                                                    "'" + ts + "', 'Print Together', '" + customer_name + "', '1', 'unpaid');");

                                            if (!card_number.isEmpty()) {
                                                //Insert Card Details for TabDetails
                                                db.execSQL("INSERT INTO TabDetails_v1 (order_number, customer_name, card_number, status) VALUES (" +
                                                        "'" + ts + "', '" + customer_name + "', '" + card_number + "', 'open');");
                                            } else {
                                                //Insert Card Details for TabDetails
                                                db.execSQL("INSERT INTO TabDetails_v1 (order_number, customer_name, card_number, status) VALUES (" +
                                                        "'" + ts + "', '" + customer_name + "', '', 'open');");
                                            }

                                            db.close();

                                            Toast.makeText(getApplicationContext(), "Tab created for " + customer_name, Toast.LENGTH_SHORT).show();

                                            create_tab_dialog.cancel();
                                        } else {
                                            Toast.makeText(getApplicationContext(), "Please insert a valid name", Toast.LENGTH_SHORT).show();
                                        }

                                    }
                                });

                                create_tab_dialog.show();
                            } else if (tab_options[which].equals("Tab List")) {
                                //TODO open custom dialog
                                final SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
                                Cursor c = db.rawQuery("SELECT * FROM TabDetails_v1 WHERE status='open'", null);

                                if (c.moveToFirst()) {
                                    final String[] open_tabs = new String[c.getCount()];
                                    final String[] open_orders = new String[c.getCount()];
                                    int i = 0;
                                    open_tabs[i] = c.getString(c.getColumnIndex("customer_name"));
                                    open_orders[i] = c.getString(c.getColumnIndex("order_number"));
                                    i++;

                                    while (c.moveToNext()) {
                                        open_tabs[i] = c.getString(c.getColumnIndex("customer_name"));
                                        open_orders[i] = c.getString(c.getColumnIndex("order_number"));
                                        i++;
                                    }

                                    AlertDialog.Builder tab_list_builder = new AlertDialog.Builder(context);
                                    tab_list_builder.setTitle("Open Tabs")
                                            .setItems(open_tabs, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    //TODO open order page
                                                    //////////////////////////Select Details of Table Ordered From///////////////////////////
                                                    SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);

                                                    String table_name, customer = "", time_stamp = "";
                                                    int order_id = 0;
                                                    Cursor c;

                                                    table_name = open_tabs[which];

                                                    c = db.rawQuery("SELECT * FROM CustomerPerTable_v1 WHERE table_name='" + table_name + "' " +
                                                            "AND time_stamp='" + open_orders[which] + "'", null);

                                                    if (c.moveToFirst()) {
                                                        customer = c.getString(c.getColumnIndex("customer"));
                                                        time_stamp = c.getString(c.getColumnIndex("time_stamp"));
                                                        order_id = c.getInt(c.getColumnIndex("Id"));
                                                        while (c.moveToNext()) {
                                                            customer = c.getString(c.getColumnIndex("customer"));
                                                            time_stamp = c.getString(c.getColumnIndex("time_stamp"));
                                                            order_id = c.getInt(c.getColumnIndex("Id"));
                                                        }
                                                    }
                                                    /////////////////////////////////////////////////////////////////////////////////////////
                                                    //Redirect to Order Page
                                                    Intent intent = new Intent(MenuListPage.this, OrderForTablePage.class);
                                                    OrderForTablePage.selected_customer = "1";
                                                    intent.putExtra("table_name", table_name);
                                                    intent.putExtra("customer", customer);
                                                    intent.putExtra("time_stamp", time_stamp);
                                                    intent.putExtra("order_id", String.valueOf(order_id));
                                                    startActivity(intent);
                                                    MenuListPage.this.overridePendingTransition(0, 0);
                                                    MenuListPage.this.finish();

                                                    c.close();
                                                    db.close();
                                                }
                                            });
                                    tab_list_builder.create();
                                    tab_list_builder.show();
                                } else {
                                    Toast.makeText(getApplicationContext(), "No Open Tabs", Toast.LENGTH_SHORT).show();
                                }

                                c.close();
                                db.close();
                            } else if (tab_options[which].equals("Close Tab")) {
                                //TODO open payment page
                            }

                            dialog.cancel();
                        }
                    });
            builder.create();
            builder.show();
            /////////////////////////////////////////////////////////////////////////
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(MenuListPage.this, ViewTables.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        MenuListPage.this.finish();
    }

}
