package com.restaware.mightyegg.restawarepos;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ClockHistoryPage extends AppCompatActivity {

    Context context;

    String[] employee_pin, in_time, out_time, full_name, log_pin, log_name, open_log_name, open_in_time;

    SQLiteDatabase db;
    Cursor c;
    int i, j;

    ListView lv_clock_history_list;

    String redirectTo;
    String PIN;
    ImageButton ibButton1, ibButton2, ibButton3, ibButton4, ibButton5, ibButton6, ibButton7, ibButton8, ibButton9, ibButton0, ibClear, ibLogin;
    ImageView ivPIN1, ivPIN2, ivPIN3, ivPIN4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clock_history_page);

        context = this;

        db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);

        InitializeToolbar();
        PopulateClockHistoryArrays();
        PopulateClockHistoryList();
        InitializeButtons();

        db.close();
    }

    private void InitializeButtons() {
        Button button_show_all_clock = (Button) findViewById(R.id.button_show_all_clock);
        Button button_show_open_clock = (Button) findViewById(R.id.button_show_open_clock);

        button_show_open_clock.setOnClickListener(new ClockButtonClickListeners());
        button_show_all_clock.setOnClickListener(new ClockButtonClickListeners());
    }

    public class ClockButtonClickListeners implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.button_show_all_clock) {
                //Log.d("CLICKED BUTTON", String.valueOf(log_name.length));
                if (log_name != null && log_name.length > 0 && lv_clock_history_list != null) {
                    ClockHistoryAdapter adapter = new ClockHistoryAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, R.id.tv_clock_history_employee_name, log_name);
                    lv_clock_history_list.setAdapter(adapter);
                }
            } else if (v.getId() == R.id.button_show_open_clock) {
                if (open_log_name != null && open_log_name.length > 0 && lv_clock_history_list != null) {
                    OpenClockAdapter openAdapter = new OpenClockAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, R.id.tv_clock_history_employee_name, open_log_name);
                    lv_clock_history_list.setAdapter(openAdapter);
                }
            }
        }
    }

    private void PopulateClockHistoryList() {
        lv_clock_history_list = (ListView) findViewById(R.id.lv_clock_history);

        if (log_name != null && log_name.length > 0 && lv_clock_history_list != null) {
            ClockHistoryAdapter adapter = new ClockHistoryAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, R.id.tv_clock_history_employee_name, log_name);
            lv_clock_history_list.setAdapter(adapter);
        }
    }

    public class OpenClockAdapter extends ArrayAdapter<String> {

        public OpenClockAdapter(Context context, int resource, int textViewResourceId, String[] objects) {
            super(context, resource, textViewResourceId, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.clock_history_list_item, parent, false);

            TextView tv_clock_history_employee_name = (TextView) row.findViewById(R.id.tv_clock_history_employee_name);
            TextView tv_clock_history_in_time = (TextView) row.findViewById(R.id.tv_clock_history_in_time);
            TextView tv_clock_history_out_time = (TextView) row.findViewById(R.id.tv_clock_history_out_time);
            TextView tv_clock_history_shift_duration = (TextView) row.findViewById(R.id.tv_clock_history_shift_duration);

            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy HH:mm");
            Date in_date;

            tv_clock_history_employee_name.setText(open_log_name[position]);

            in_date = new Date(Long.parseLong(open_in_time[position]) * 1000);
            tv_clock_history_in_time.setText(sdf.format(in_date));

            tv_clock_history_out_time.setText("");
            tv_clock_history_shift_duration.setText("");

            return row;
        }
    }

    public class ClockHistoryAdapter extends ArrayAdapter<String> {

        public ClockHistoryAdapter(Context context, int resource, int textViewResourceId, String[] objects) {
            super(context, resource, textViewResourceId, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.clock_history_list_item, parent, false);

            TextView tv_clock_history_employee_name = (TextView) row.findViewById(R.id.tv_clock_history_employee_name);
            TextView tv_clock_history_in_time = (TextView) row.findViewById(R.id.tv_clock_history_in_time);
            TextView tv_clock_history_out_time = (TextView) row.findViewById(R.id.tv_clock_history_out_time);
            TextView tv_clock_history_shift_duration = (TextView) row.findViewById(R.id.tv_clock_history_shift_duration);

            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy HH:mm");
            Date in_date, out_date;

            tv_clock_history_employee_name.setText(log_name[position]);

            in_date = new Date(Long.parseLong(in_time[position]) * 1000);
            tv_clock_history_in_time.setText(sdf.format(in_date));

            if (!out_time[position].equals("")) {
                out_date = new Date(Long.parseLong(out_time[position]) * 1000);
                tv_clock_history_out_time.setText(sdf.format(out_date));

                long difference = out_date.getTime() - in_date.getTime();

                tv_clock_history_shift_duration.setText((difference / (60 * 60 * 1000)) + " hours " + ((difference % (60 * 60 * 1000)) / (60 * 1000)) + " minutes");
            } else {
                tv_clock_history_out_time.setText("");
                tv_clock_history_shift_duration.setText("");
            }

            return row;
        }
    }

    private void PopulateClockHistoryArrays() {
        c = db.rawQuery("SELECT * FROM user_tables", null);

        if (c.moveToFirst()) {
            full_name = new String[c.getCount()];
            employee_pin = new String[c.getCount()];
            i = 0;

            full_name[i] = c.getString(c.getColumnIndex("fullname"));
            employee_pin[i] = c.getString(c.getColumnIndex("employee_pin"));
            i++;

            while (c.moveToNext()) {
                full_name[i] = c.getString(c.getColumnIndex("fullname"));
                employee_pin[i] = c.getString(c.getColumnIndex("employee_pin"));
                i++;
            }

            c = db.rawQuery("SELECT * FROM EmployeeAttendance_v1 ORDER BY in_time ASC", null);

            if (c.moveToFirst()) {
                log_pin = new String[c.getCount()];
                log_name = new String[c.getCount()];
                in_time = new String[c.getCount()];
                out_time = new String[c.getCount()];
                i = 0;

                log_pin[i] = c.getString(c.getColumnIndex("employee_pin"));
                in_time[i] = c.getString(c.getColumnIndex("in_time"));
                out_time[i] = c.getString(c.getColumnIndex("out_time"));
                i++;

                while (c.moveToNext()) {
                    log_pin[i] = c.getString(c.getColumnIndex("employee_pin"));
                    in_time[i] = c.getString(c.getColumnIndex("in_time"));
                    out_time[i] = c.getString(c.getColumnIndex("out_time"));
                    i++;
                }

                for (i = 0; i < employee_pin.length; i++) {
                    for (j = 0; j < log_pin.length; j++) {
                        if (employee_pin[i].equals(log_pin[j])) {
                            log_name[j] = full_name[i];
                        }
                    }
                }

                int openCount = 0;

                for (i = 0; i < log_name.length; i++) {
                    if (out_time[i].equals("")) {
                        openCount++;
                    }
                }

                if (openCount > 0) {
                    open_log_name = new String[openCount];
                    open_in_time = new String[openCount];

                    openCount = 0;

                    for (i = 0; i < log_name.length; i++) {
                        if (out_time[i].equals("")) {
                            open_log_name[openCount] = log_name[i];
                            open_in_time[openCount] = in_time[i];
                            openCount++;
                        }
                    }
                }
            }
        }
    }

    private void InitializeToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_tab_history);
        setSupportActionBar(toolbar);

        ImageButton ib_tab = (ImageButton) findViewById(R.id.ib_tab);
        ib_tab.setOnClickListener(new TabOptions());

        //copy for clock in/out
        ImageButton ib_clock_in_out = (ImageButton) findViewById(R.id.ib_clock_in_out);
        ib_clock_in_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PIN = "";
                redirectTo = "";

                //default dialog for clock in/out
                final String[] clock_options = {"Clock In", "Clock Out"};
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Select Clock Options")
                        .setItems(clock_options, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                redirectTo = clock_options[which];

                                // custom dialog
                                final Dialog clockDialog = new Dialog(context);
                                clockDialog.setContentView(R.layout.clock_in_out_dialog_layout);
                                clockDialog.setTitle("Provide Details");

                                ibButton1 = (ImageButton) clockDialog.findViewById(R.id.ibButton1);
                                ibButton2 = (ImageButton) clockDialog.findViewById(R.id.ibButton2);
                                ibButton3 = (ImageButton) clockDialog.findViewById(R.id.ibButton3);
                                ibButton4 = (ImageButton) clockDialog.findViewById(R.id.ibButton4);
                                ibButton5 = (ImageButton) clockDialog.findViewById(R.id.ibButton5);
                                ibButton6 = (ImageButton) clockDialog.findViewById(R.id.ibButton6);
                                ibButton7 = (ImageButton) clockDialog.findViewById(R.id.ibButton7);
                                ibButton8 = (ImageButton) clockDialog.findViewById(R.id.ibButton8);
                                ibButton9 = (ImageButton) clockDialog.findViewById(R.id.ibButton9);
                                ibButton0 = (ImageButton) clockDialog.findViewById(R.id.ibButton0);

                                ibButton1.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton2.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton3.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton4.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton5.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton6.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton7.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton8.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton9.setOnTouchListener(new EmployeePINOnTouchListener());
                                ibButton0.setOnTouchListener(new EmployeePINOnTouchListener());

                                ibClear = (ImageButton) clockDialog.findViewById(R.id.ibClear);
                                ibClear.setOnTouchListener(new EmployeePINOnTouchListener());

                                ibLogin = (ImageButton) clockDialog.findViewById(R.id.ibLogin);
                                ibLogin.setOnTouchListener(new EmployeePINOnTouchListener());

                                ivPIN1 = (ImageView) clockDialog.findViewById(R.id.ivPIN1);
                                ivPIN2 = (ImageView) clockDialog.findViewById(R.id.ivPIN2);
                                ivPIN3 = (ImageView) clockDialog.findViewById(R.id.ivPIN3);
                                ivPIN4 = (ImageView) clockDialog.findViewById(R.id.ivPIN4);

                                clockDialog.show();
                            }
                        });
                builder.create();
                builder.show();
            }
        });
    }

    private class EmployeePINOnTouchListener implements View.OnTouchListener {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            Long tsLong = System.currentTimeMillis()/1000;
            String ts = tsLong.toString();

            if(v.getId() == R.id.ibButton1) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton1.setImageResource(R.drawable.employee_login_1_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "1";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton1.setImageResource(R.drawable.employee_login_button_1);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton2) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton2.setImageResource(R.drawable.employee_login_2_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "2";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton2.setImageResource(R.drawable.employee_login_button_2);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton3) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton3.setImageResource(R.drawable.employee_login_3_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "3";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton3.setImageResource(R.drawable.employee_login_button_3);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton4) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton4.setImageResource(R.drawable.employee_login_4_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "4";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton4.setImageResource(R.drawable.employee_login_button_4);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton5) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton5.setImageResource(R.drawable.employee_login_5_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "5";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton5.setImageResource(R.drawable.employee_login_button_5);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton6) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton6.setImageResource(R.drawable.employee_login_6_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "6";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton6.setImageResource(R.drawable.employee_login_button_6);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton7) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton7.setImageResource(R.drawable.employee_login_7_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "7";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton7.setImageResource(R.drawable.employee_login_button_7);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton8) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton8.setImageResource(R.drawable.employee_login_8_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "8";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton8.setImageResource(R.drawable.employee_login_button_8);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton9) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton9.setImageResource(R.drawable.employee_login_9_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "9";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton9.setImageResource(R.drawable.employee_login_button_9);
                        break;
                }
            }
            else if(v.getId() == R.id.ibButton0) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibButton0.setImageResource(R.drawable.employee_login_0_clicked);
                        if(PIN.length() >= 0 && PIN.length() < 4) {
                            PIN = PIN + "0";
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibButton0.setImageResource(R.drawable.employee_login_button_0);
                        break;
                }
            }
            else if(v.getId() == R.id.ibClear) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ibClear.setImageResource(R.drawable.employee_login_clear_clicked);
                        if(PIN.length() > 0) {
                            PIN = PIN.substring(0, PIN.length() - 1);
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibClear.setImageResource(R.drawable.employee_login_clear_button);
                        break;
                }
                Log.d("PIN", PIN);
            }
            else if(v.getId() == R.id.ibLogin) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        boolean return_type;
                        ibLogin.setImageResource(R.drawable.employee_login_login_clicked);
                        if(PIN.length() < 4) {
                            Toast.makeText(getApplicationContext(), "Incomplete PIN", Toast.LENGTH_SHORT).show();
                        }
                        else if(redirectTo.isEmpty()) {
                            Toast.makeText(getApplicationContext(), "Please Select an Option", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
                            Cursor c = db.rawQuery("SELECT * FROM user_tables WHERE employee_pin='" + PIN + "'", null);

                            if (c.moveToFirst()) {
                                if (redirectTo.equals("Clock In")) {
                                    return_type = EmployeeAttendance(PIN, ts, "In");

                                    if (return_type)
                                        Toast.makeText(getApplicationContext(), "Successfully Clocked In", Toast.LENGTH_SHORT).show();
                                    else
                                        Toast.makeText(getApplicationContext(), "Failed to Clock In", Toast.LENGTH_SHORT).show();
                                } else if (redirectTo.equals("Clock Out")) {
                                    return_type = EmployeeAttendance(PIN, ts, "Out");

                                    if (return_type)
                                        Toast.makeText(getApplicationContext(), "Successfully Clocked Out", Toast.LENGTH_SHORT).show();
                                    else
                                        Toast.makeText(getApplicationContext(), "Failed to Clock Out", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), "Invalid PIN", Toast.LENGTH_SHORT).show();
                            }

                            c.close();
                            db.close();
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        ibLogin.setImageResource(R.drawable.employee_login_login_button);
                        break;
                }
                Log.d("PIN", PIN);
            }

            int length = PIN.length();

            switch (length) {
                case 0:
                    ivPIN1.setImageResource(R.drawable.employee_login_white_dot);
                    ivPIN2.setImageResource(R.drawable.employee_login_white_dot);
                    ivPIN3.setImageResource(R.drawable.employee_login_white_dot);
                    ivPIN4.setImageResource(R.drawable.employee_login_white_dot);
                    break;
                case 1:
                    ivPIN1.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN2.setImageResource(R.drawable.employee_login_white_dot);
                    ivPIN3.setImageResource(R.drawable.employee_login_white_dot);
                    ivPIN4.setImageResource(R.drawable.employee_login_white_dot);
                    break;
                case 2:
                    ivPIN1.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN2.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN3.setImageResource(R.drawable.employee_login_white_dot);
                    ivPIN4.setImageResource(R.drawable.employee_login_white_dot);
                    break;
                case 3:
                    ivPIN1.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN2.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN3.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN4.setImageResource(R.drawable.employee_login_white_dot);
                    break;
                case 4:
                    ivPIN1.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN2.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN3.setImageResource(R.drawable.employee_login_blue_dot);
                    ivPIN4.setImageResource(R.drawable.employee_login_blue_dot);
                    break;
            }

            return true;
        }
    }

    private boolean EmployeeAttendance(String employee_pin, String clock_time, String clock_mode) {
        SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
        Cursor c = db.rawQuery("SELECT * FROM EmployeeAttendance_v1 WHERE employee_pin='" + employee_pin + "'", null);
        String out_time = "";
        int id = -1;
        boolean first_time = false;
        if (c.moveToFirst()) {
            out_time = c.getString(c.getColumnIndex("out_time"));
            id = c.getInt(c.getColumnIndex("Id"));

            while (c.moveToNext()) {
                out_time = c.getString(c.getColumnIndex("out_time"));
                id = c.getInt(c.getColumnIndex("Id"));
            }
        } else {
            first_time = true;
        }
        c.close();

        if (clock_mode.equals("In")) {
            if (first_time || !out_time.equals("")) {
                db.execSQL("INSERT INTO EmployeeAttendance_v1 (employee_pin, in_time, out_time) VALUES ('" + employee_pin + "', " +
                        "'" + clock_time + "', '');");
                return true;
            }
        } else if (clock_mode.equals("Out")) {
            if (!first_time && out_time.equals("")) {
                db.execSQL("UPDATE EmployeeAttendance_v1 SET out_time='" + clock_time + "' WHERE Id=" + id);
                return true;
            }
        }

        db.close();
        return false;
    }

    private class TabOptions implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            final String[] tab_options = {"Create Tab", "Tab List", "Close Tab"};
            //////////////////Default Dialog to Select Tab Options//////////////////////
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Select Tab Options")
                    .setItems(tab_options, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Log.d("SELECTED TAB OPTIONS", tab_options[which]);

                            if (tab_options[which].equals("Create Tab")) {
                                //TODO open custom dialog
                                final Dialog create_tab_dialog = new Dialog(context);
                                create_tab_dialog.setContentView(R.layout.create_tab_dialog_layout);

                                final EditText et_tab_customer_name = (EditText) create_tab_dialog.findViewById(R.id.et_tab_customer_name);
                                final EditText et_tab_card_number = (EditText) create_tab_dialog.findViewById(R.id.et_tab_card_number);

                                Button button_tab_confirm = (Button) create_tab_dialog.findViewById(R.id.button_tab_confirm);
                                button_tab_confirm.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        String customer_name = et_tab_customer_name.getText().toString();
                                        String card_number = et_tab_card_number.getText().toString();

                                        if (!customer_name.isEmpty()) {
                                            SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
                                            Long tsLong = System.currentTimeMillis()/1000;
                                            String ts = tsLong.toString();

                                            //Insert Customer Details for CustomerPerTable
                                            db.execSQL("INSERT INTO CustomerPerTable_v1 (table_name, customer, time_stamp, floor_name) VALUES (" +
                                                    "'" + customer_name + "', '1', '" + ts + "', 'Tab');");

                                            //Insert Discount Details for BillSupplementary
                                            db.execSQL("INSERT INTO BillSupplementary_v1 (order_number, discount, discount_type) VALUES (" +
                                                    "'" + ts + "', '0', '');");

                                            //Insert Print Details for PrintOptions
                                            db.execSQL("INSERT INTO PrintOptions_v1 (order_number, print_options, table_number, combination, " +
                                                    "payment_status) VALUES (" +
                                                    "'" + ts + "', 'Print Together', '" + customer_name + "', '1', 'unpaid');");

                                            if (!card_number.isEmpty()) {
                                                //Insert Card Details for TabDetails
                                                db.execSQL("INSERT INTO TabDetails_v1 (order_number, customer_name, card_number, status) VALUES (" +
                                                        "'" + ts + "', '" + customer_name + "', '" + card_number + "', 'open');");
                                            } else {
                                                //Insert Card Details for TabDetails
                                                db.execSQL("INSERT INTO TabDetails_v1 (order_number, customer_name, card_number, status) VALUES (" +
                                                        "'" + ts + "', '" + customer_name + "', '', 'open');");
                                            }

                                            db.close();

                                            Toast.makeText(getApplicationContext(), "Tab created for " + customer_name, Toast.LENGTH_SHORT).show();

                                            create_tab_dialog.cancel();
                                        } else {
                                            Toast.makeText(getApplicationContext(), "Please insert a valid name", Toast.LENGTH_SHORT).show();
                                        }

                                    }
                                });

                                create_tab_dialog.show();
                            } else if (tab_options[which].equals("Tab List")) {
                                //TODO open custom dialog
                                final SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);
                                Cursor c = db.rawQuery("SELECT * FROM TabDetails_v1 WHERE status='open'", null);

                                if (c.moveToFirst()) {
                                    final String[] open_tabs = new String[c.getCount()];
                                    final String[] open_orders = new String[c.getCount()];
                                    int i = 0;
                                    open_tabs[i] = c.getString(c.getColumnIndex("customer_name"));
                                    open_orders[i] = c.getString(c.getColumnIndex("order_number"));
                                    i++;

                                    while (c.moveToNext()) {
                                        open_tabs[i] = c.getString(c.getColumnIndex("customer_name"));
                                        open_orders[i] = c.getString(c.getColumnIndex("order_number"));
                                        i++;
                                    }

                                    AlertDialog.Builder tab_list_builder = new AlertDialog.Builder(context);
                                    tab_list_builder.setTitle("Open Tabs")
                                            .setItems(open_tabs, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    //TODO open order page
                                                    //////////////////////////Select Details of Table Ordered From///////////////////////////
                                                    SQLiteDatabase db = openOrCreateDatabase("RestawarePOS", MODE_PRIVATE, null);

                                                    String table_name, customer = "", time_stamp = "";
                                                    int order_id = 0;
                                                    Cursor c;

                                                    table_name = open_tabs[which];

                                                    c = db.rawQuery("SELECT * FROM CustomerPerTable_v1 WHERE table_name='" + table_name + "' " +
                                                            "AND time_stamp='" + open_orders[which] + "'", null);

                                                    if (c.moveToFirst()) {
                                                        customer = c.getString(c.getColumnIndex("customer"));
                                                        time_stamp = c.getString(c.getColumnIndex("time_stamp"));
                                                        order_id = c.getInt(c.getColumnIndex("Id"));
                                                        while (c.moveToNext()) {
                                                            customer = c.getString(c.getColumnIndex("customer"));
                                                            time_stamp = c.getString(c.getColumnIndex("time_stamp"));
                                                            order_id = c.getInt(c.getColumnIndex("Id"));
                                                        }
                                                    }
                                                    /////////////////////////////////////////////////////////////////////////////////////////
                                                    //Redirect to Order Page
                                                    Intent intent = new Intent(ClockHistoryPage.this, OrderForTablePage.class);
                                                    OrderForTablePage.selected_customer = "1";
                                                    intent.putExtra("table_name", table_name);
                                                    intent.putExtra("customer", customer);
                                                    intent.putExtra("time_stamp", time_stamp);
                                                    intent.putExtra("order_id", String.valueOf(order_id));
                                                    startActivity(intent);
                                                    ClockHistoryPage.this.overridePendingTransition(0, 0);
                                                    ClockHistoryPage.this.finish();

                                                    c.close();
                                                    db.close();
                                                }
                                            });
                                    tab_list_builder.create();
                                    tab_list_builder.show();
                                } else {
                                    Toast.makeText(getApplicationContext(), "No Open Tabs", Toast.LENGTH_SHORT).show();
                                }

                                c.close();
                                db.close();
                            } else if (tab_options[which].equals("Close Tab")) {
                                //TODO open payment page
                            }

                            dialog.cancel();
                        }
                    });
            builder.create();
            builder.show();
            /////////////////////////////////////////////////////////////////////////
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(ClockHistoryPage.this, ViewTables.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        ClockHistoryPage.this.finish();
    }
}
